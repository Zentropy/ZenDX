﻿#include "ZModelImporter.h"
#include "assimp/Importer.hpp"      // C++ importer interface
#include "assimp/scene.h"           // Output data structure
#include "assimp/postprocess.h"     // Post processing flags
#include "Utilities/ZLog.h"
#include <iostream>
#include "Engine/Engine.h"
#include <Engine/GameObject.h>
#include <queue>
#include "Renderer/Tests/ZMesh.h"


namespace Zen {
	ZModelImporter::ZModelImporter() {
		importer = std::make_unique<Assimp::Importer>();
	}

	//std::shared_ptr<const aiScene> ZModelImporter::ImportFBX(const std::string& filePath)
	std::shared_ptr<GameObject> ZModelImporter::ImportFBX(const std::string& inFilePath)
	{
		std::string filePath = DirectoryUtil::GetModelDirectory() + inFilePath;
		
		// And have it read the given file with some example postprocessing
		// Usually - if speed is not the most important aspect for you - you'll 
		// propably to request more postprocessing than we do in this example.
		// #TODO: aiProcess_RemoveComponent aiProcess_SplitLargeMeshes https://github.com/assimp/assimp/blob/4919d3da2f772bfe6bff487e8345fcb2215ddbdb/include/assimp/postprocess.h
		//importer->SetPropertyFloat("AI_CONFIG_GLOBAL_SCALE_FACTOR_KEY", 100.f);
		scene = importer->ReadFile( filePath,
			aiProcess_CalcTangentSpace         | // calculate tangents and bitangents if possible
			aiProcess_JoinIdenticalVertices    | // join identical vertices/ optimize indexing
												 //aiProcess_ValidateDataStructure  | // perform a full validation of the loader's output
			aiProcess_Triangulate              | // Ensure all verticies are triangulated (each 3 vertices are triangle)
			aiProcess_ConvertToLeftHanded      | // convert everything to D3D left handed space (by default right-handed, for OpenGL)
			aiProcess_SortByPType              | // ?
			aiProcess_ImproveCacheLocality     | // improve the cache locality of the output vertices
			aiProcess_RemoveRedundantMaterials | // remove redundant materials
			aiProcess_FindDegenerates          | // remove degenerated polygons from the import
			aiProcess_FindInvalidData          | // detect invalid model data, such as invalid normal vectors
			aiProcess_GenUVCoords              | // convert spherical, cylindrical, box and planar mapping to proper UVs
			aiProcess_TransformUVCoords        | // preprocess UV transformations (scaling, translation ...)
			aiProcess_FindInstances            | // search for instanced meshes and remove them by references to one master
			aiProcess_LimitBoneWeights         | // limit bone weights to 4 per vertex
			aiProcess_OptimizeMeshes           | // join small meshes, if possible;
			aiProcess_GenNormals			   |
			aiProcess_SplitByBoneCount          // split meshes with too many bones.
		);			

		// If the import failed, report it
		if( !scene)
		{
			//DoTheErrorLogging( importer.GetErrorString());
			LOG << "Failed to load FBX: " << filePath;
			return nullptr;
		}
		LOG << "Loaded scene: " << scene->mNumMeshes;
		// Now we can access the file's contents. 
		return ConvertToNodeGraph(scene, filePath);
		
	}	

	std::shared_ptr<GameObject> ZModelImporter::ConvertToNodeGraph(const aiScene* inScene, const std::string& filePath) {
		//mDevice = inDevice;
		//mMaterial = std::make_unique<ZMaterial>(L"C:/Workspace/CPP/ZenDX/Data/Models/TestBox/cube_diffuse.jpg", ShaderTypes::Diffuse, mDevice, ENGINE->GetHwnd(), L"texture.vs", L"texture.ps");

		//texture = std::make_unique<ZTexture>(L"C:/Workspace/CPP/ZenDX/Data/Textures/awesomeface.png", device);
		//auto imp = ENGINE->GetAssetImporter();
		////change this to GetExePath and  symlink my data over to build exe folder, then remove NewBox.fbx and concat aiString path onto the end of it to load texture
		//std::string modelpath = DirectoryUtil::GetExeDirectory() + "Data/Models/" + filePath;
		////std::string str(modelpath.begin(), modelpath.end());
		//imp->ImportFBX(modelpath);
		//imp->ImportFBX("C:/Workspace/CPP/ZenDX/Data/Models/nanosuit/nanosuit.obj");
		auto& aiSceneData = inScene;
		auto gameScene = ENGINE->GetCurrentScene();
		LOG << "Imported fbx and texture";

		//one node per game object for now
		aiNode* currentAiNode = aiSceneData->mRootNode;
		std::queue<std::pair<GameObjectPtr, aiNode*>> nodeStack;
		auto rootGameObject = gameScene->CreateGameObject();
		GameObjectPtr currentGameObject = rootGameObject;

		//for (int i = 0; i < NumMeshes; ++i)
		while (nullptr != currentAiNode)
		{
			//get node's children and put em onto the stack
			for (unsigned int i = 0; i < currentAiNode->mNumChildren; ++i)
			{
				nodeStack.push(std::make_pair(currentGameObject, currentAiNode->mChildren[i]));
			}

			//load each mesh from this node, if no meshes LOG but keep going for now
			for (unsigned int i = 0; i < currentAiNode->mNumMeshes; ++i)
			{
				//currentnode->mMeshes holds an index pointing to the scene mesh
				aiMesh* mesh = aiSceneData->mMeshes[currentAiNode->mMeshes[i]];

				//Create game obj to hold them
				//#TODO: IM HERE CONVERTING PROPERLY NOW
				static int GenericGONamePostfix = 1;
				auto meshObj = gameScene->CreateGameObject(currentGameObject);
				aiString emptyStr("");
				std::string newName;
				if (mesh->mName == emptyStr) {
					newName = "Node_" + std::to_string(GenericGONamePostfix);
					GenericGONamePostfix++;
					meshObj->SetName(newName);
				}
				else {
					meshObj->SetName(mesh->mName.C_Str());
				}
				

				aiMatrix4x4t tform = currentAiNode->mTransformation;
				aiVector3t<float> scaling;
				aiVector3t<float> position;
				aiVector3t<float> rotationAxis;
				float rotationDegree;
				tform.Decompose(scaling, rotationAxis, rotationDegree, position);
				//float scaleTest = 0.0100f; //3ds at least exports the positions 100x too big
				std::shared_ptr<ZMesh> newMesh = std::make_shared<ZMesh>(meshObj, mesh);

				float scaleTest = 1.0f;
				meshObj->SetMesh(newMesh);
				newMesh->transform->Initialize(position.x/scaleTest, position.y/scaleTest, position.z/scaleTest, ZQuaternion::Identity());
				if (rotationDegree != 0.0f) {
					newMesh->transform->RotateAroundAxisDegrees(vec3(rotationAxis.x, rotationAxis.y, rotationAxis.z), rotationDegree);
					newMesh->transform->_originalRotation = newMesh->transform->_rotation; // kludge workaround because Initialize doesn't capture quat
				}
				//newMesh->transform.SetScale(vec3(scaling.x, scaling.y, scaling.z));
				//Meshes.emplace_back(newMesh);
				gameScene->AddMesh(newMesh);
				LOG << "Loaded missile mesh";

				/////////////////////////////////////////////////////////////////////////////////////////
				/////////// TEXTURES /////////////////////////////////////////

				int texIndex = 0;
				aiString path; // filename
							   //path after this line runs ends up being "textures\\sponza_thorn_diff.png"
				aiReturn texFound = aiSceneData->mMaterials[mesh->mMaterialIndex]->GetTexture(
					aiTextureType_DIFFUSE, texIndex, &path);

				aiString name;
				aiSceneData->mMaterials[mesh->mMaterialIndex]->Get(AI_MATKEY_NAME, name);

				std::string texpath;

				if(texFound == aiReturn_FAILURE)
				{
					LOG << "Failed loading texture";
				} else
				{
					
					//fix the path by checking to see if it has C: at the beginning and use it directly if so, otherwise do the rest of the junk
					if (path.C_Str()[1] == ':') {//it's a directory
						LOG << "Found directory in path";
						texpath = path.C_Str();
					} else {
						texpath = filePath.substr(0, filePath.find_last_of("\\/"));
						texpath += "/";
						texpath += path.C_Str();
					}

					//the rest of the junk
					
				}

				//while (texFound == AI_SUCCESS) {
				//    //fill map with textures, OpenGL image ids set to 0
				//   // textureIdMap[path.data] = 0; 
				//    // more textures?
				//    texIndex++;
				//    texFound = data->mMaterials[0]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
				//}
				// Tex path is: C:/Workspace/CPP/ZenDX/Build/ZenApplication/x64/Debug/Data/Models/NewBoxtextures\awesomeface.png

				

				std::replace(texpath.begin(), texpath.end(), '\\', '/');
				//trade out all TGAs for PNGs forcibly b/c tga loading doesn't fucking work 
				StrUtil::ReplaceStringInPlace(texpath, "tga", "png");
				std::wstring texpathwide(texpath.begin(), texpath.end());
				//std::replace(texpathwide.begin(), texpathwide.end(), 'tga', 'png');
				LOG << "Tex path is: " << texpathwide;

				newMesh->mMaterial = std::make_shared<ZMaterial>(EShaderTypes::DIFFUSE, ENGINE->GetDevice(),
					ENGINE->GetHwnd(), L"texture.vs", L"texture.ps");
				//newMesh->mMaterial = std::make_shared<ZMaterial>(EShaderTypes::LIT, ENGINE->GetDevice(),
				//	ENGINE->GetHwnd(), L"lit.vs",
				//	L"lit.ps");
				newMesh->mMaterial->SetNewTexture(texpathwide );
								
			}

			//move to next node from stack
			if (!nodeStack.empty())
			{
				//CurrentObject is the thing to parent the new objects to, whereas node is the aiScene node to process
				auto front = nodeStack.front();
				currentGameObject = front.first;
				currentAiNode = front.second;
				nodeStack.pop();
			}
			else
			{
				//stop traversing node structure
				currentAiNode = nullptr;
			}
		}
		LOG << "Done processing nodes";
		return rootGameObject;
	}

	ZModelImporter::~ZModelImporter()
	{
		int i = 5;
		std::cout << "JFC";
		//importer.FreeScene();
	}

}
