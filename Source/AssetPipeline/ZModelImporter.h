#pragma once
#include <string>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>

namespace Zen {
	class GameObject;

class ZModelImporter {
public:
	ZModelImporter();	
	//std::shared_ptr<const aiScene> ImportFBX(const std::string& filePath);
	std::shared_ptr<GameObject> ImportFBX(const std::string& filePath);
	std::shared_ptr<GameObject> ConvertToNodeGraph(const aiScene* inScene, const std::string& filePath);
	virtual ~ZModelImporter();
	
//private:
	std::unique_ptr<Assimp::Importer> importer;
	//std::shared_ptr<const aiScene> scene;
	const aiScene* scene;
	
};
}
