#include "D3DManager.h"
//#include <Utilities/ZLog.h>
#include "d3dUtil.h"
#include "Utilities/Settings/ZenSettings.h"
#include <Unknwn.h>
#include "GUI/imgui/imgui.h"
#include "GUI/imgui/imgui_impl_dx11.h"
#include "Engine/Engine.h"

namespace Zen {

	using namespace DirectX;
	   
	D3DManager::D3DManager()
		: mSwapChain(nullptr),
		  mDevice(nullptr),
		mDeviceContext(nullptr)
	{}

	D3DManager::~D3DManager()
	{
		Shutdown();
		
	}

	bool D3DManager::Initialize(HWND hwnd,  DXGI_FORMAT FrameBufferFormat)
	{
		mHwnd = hwnd;
		HRESULT result;
		IDXGIFactory* pFactory;
		IDXGIAdapter* pAdapter;
		IDXGIOutput* pAdapterOutput;
		unsigned numModes;

		int width = ZSettings->windowWidth;
		int height = ZSettings->windowHeight;

		// Store the vsync setting.
		//mVsyncEnabled = VSYNC;

		// Get System Information
		//----------------------------------------------------------------------------------

		// Create a DirectX graphics interface factory.
		result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pFactory);
		if (FAILED(result))
		{
			return false;
		}

		// Use the factory to create an adapter for the primary graphics interface (video card).
		result = pFactory->EnumAdapters(0, &pAdapter);
		if (FAILED(result))
		{
			return false;
		}

		// Enumerate the primary adapter output (monitor).
		result = pAdapter->EnumOutputs(0, &pAdapterOutput);
		if (FAILED(result))
		{
			return false;
		}

		// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
		result = pAdapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, nullptr);
		if (FAILED(result))
		{
			return false;
		}

		// Create a list to hold all the possible display modes for this monitor/video card combination.
		DXGI_MODE_DESC* displayModeList;
		displayModeList = new DXGI_MODE_DESC[numModes];
		if (!displayModeList)
		{
			return false;
		}

		// Now fill the display mode list structures.
		result = pAdapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
		if (FAILED(result))
		{
			return false;
		}

		// Now go through all the display modes and find the one that matches the screen width and height.
		// When a match is found store the numerator and denominator of the refresh rate for that monitor.
		unsigned numerator = 0;
		unsigned denominator = 0;
		for (unsigned i = 0; i<numModes; i++)
		{
			if (displayModeList[i].Width == (unsigned int)width)
			{
				if (displayModeList[i].Height == (unsigned int)height)
				{
					numerator = displayModeList[i].RefreshRate.Numerator;
					denominator = displayModeList[i].RefreshRate.Denominator;
				}
			}
		}

		if (numerator == 0 && denominator == 0)
		{
			wchar_t info[127];
			numerator	= displayModeList[numModes / 2].RefreshRate.Numerator;
			denominator = displayModeList[numModes / 2].RefreshRate.Denominator;
			swprintf_s(info, L"Specified resolution (%ux%u) not found: Using (%ux%u) instead\n",
				width, height,
				displayModeList[numModes / 2].Width, displayModeList[numModes / 2].Height);
			width = displayModeList[numModes / 2].Width;
			height = displayModeList[numModes / 2].Height;
			OutputDebugString(info);

			// also resize window
			SetWindowPos(hwnd, 0, 10, 10, width, height, SWP_NOZORDER);
		}

		// Get the adapter (video card) description.
		DXGI_ADAPTER_DESC adapterDesc;
		result = pAdapter->GetDesc(&adapterDesc);
		if (FAILED(result))
		{
			return false;
		}

		// Store the dedicated video card memory in megabytes.
		mVRAM = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

		// Convert the name of the video card to a character array and store it.
		size_t stringLength;
		int error;
		error = wcstombs_s(&stringLength, mGPUDescription, 128, adapterDesc.Description, 128);
		if (error != 0)
		{
			return false;
		}

		// Release memory
		delete[] displayModeList;		displayModeList = nullptr;
		pAdapterOutput->Release();		pAdapterOutput = nullptr;
		pAdapter->Release();			pAdapter = nullptr;
		pFactory->Release();			pFactory = nullptr;

		if (!InitSwapChain(hwnd, ZSettings->bUseFullscreen, width, height, numerator, denominator, DXGI_FORMAT_B8G8R8A8_UNORM))
		{
			return false;		
		}

		
		InitDepthStencil();
		return true;
	}

	bool D3DManager::InitDepthStencil() {
		// create render target view	
		mSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer));
		mDevice->CreateRenderTargetView(backBuffer, 0, &mRenderTargetView);
		ReleaseCOM(backBuffer);

		// Create depth/stencil buffer and view
		D3D11_TEXTURE2D_DESC depthStencilDesc;
		//fucking hell i should've been using client bounds here instead of window size
		RECT clientRect = ENGINE->GetClientRect();
		//GetClientRect(mHwnd, &clientRect);

		//depthStencilDesc.Width = ZSettings->windowWidth;
		depthStencilDesc.Width = clientRect.right;
		//depthStencilDesc.Height = ZSettings->windowHeight;
		depthStencilDesc.Height = clientRect.bottom;

		depthStencilDesc.MipLevels = 1; //always 1 for depth/stencil
		depthStencilDesc.ArraySize = 1; //same as above
		depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;

		//need msaa here from splitting up device and swap chain creation
		depthStencilDesc.SampleDesc.Count = 1;
		depthStencilDesc.SampleDesc.Quality = 0;

		depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
		depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		depthStencilDesc.CPUAccessFlags = 0;
		depthStencilDesc.MiscFlags = 0;

		mDevice->CreateTexture2D(&depthStencilDesc, // description of texture to create
			0, //pointer to initial texture data  
			&mDepthStencilBuffer);

		mDevice->CreateDepthStencilView(mDepthStencilBuffer, 
			0, //null because we specified the type of the buffer, see pg 120 
			&mDepthStencilView);

		//Bind views to output merger stage
		//number of render targets = 1,  pointer to array of render target view pointers, depth stencil view to bind
		mDeviceContext->OMSetRenderTargets(1, &mRenderTargetView, mDepthStencilView);

		//Create viewport to draw to		
		vp.TopLeftX = 0.0f;
		vp.TopLeftY = 0.f;
		//vp.Width = static_cast<float>(ZSettings->windowWidth);
		vp.Width = static_cast<float>(clientRect.right);
		vp.Height = static_cast<float>(clientRect.bottom);
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;

		mDeviceContext->RSSetViewports(1, &vp);

		float fieldOfView = 3.141592654f / 4.0f;
		float screenAspect = (float)clientRect.right / clientRect.bottom;
		float screenNear = 1.f;
		float screenDepth = 1000; //#TODO: extract this upward
		mProjectionMatrix = XMMatrixPerspectiveFovLH(fieldOfView, screenAspect, screenNear, screenDepth );

		mOrthoMatrix = XMMatrixOrthographicLH((float)clientRect.right, (float)clientRect.bottom, screenNear, screenDepth);

		mWorldMatrix = XMMatrixIdentity();

		return true;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	bool D3DManager::InitSwapChain(HWND hwnd, bool fullscreen, int scrWidth, int scrHeight, unsigned numerator, unsigned denominator, DXGI_FORMAT FrameBufferFormat)
	{
		HRESULT result;
		D3D_FEATURE_LEVEL featureLevel;
		const D3D_FEATURE_LEVEL featureLevelArray[2] = { D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_0, };
		
		//DXGI_SWAP_CHAIN_DESC sd;
		//ZeroMemory(&sd, sizeof(sd));
		//sd.BufferCount = 2;
		//sd.BufferDesc.Width = 0;
		//sd.BufferDesc.Height = 0;
		//sd.BufferDesc.Format = FrameBufferFormat;
		//sd.BufferDesc.RefreshRate.Numerator = 60;
		//sd.BufferDesc.RefreshRate.Denominator = 1;
		//sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
		//sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT;
		//sd.OutputWindow = hwnd;
		//sd.SampleDesc.Count = 1;
		//sd.SampleDesc.Quality = 0;
		//sd.Windowed = TRUE;
		//sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		//
		//UINT createDeviceFlags = 0;
		////createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
		//
		//
		//if (D3D11CreateDeviceAndSwapChain(
		//	nullptr, 
		//	D3D_DRIVER_TYPE_HARDWARE, 
		//	nullptr, 
		//	createDeviceFlags, 
		//	featureLevelArray, 
		//	2, 
		//	D3D11_SDK_VERSION, 
		//	&sd, 
		//	&mSwapChain, 
		//	&mDevice, 
		//	&featureLevel, 
		//	&mDeviceContext) != S_OK)
		//	return E_FAIL;
		//
		////CreateRenderTarget();
		//ImGui_ImplDX11_Init(mDevice, mDeviceContext);
		//return true;


		//below not stolen from imgui


#ifndef HIGHER_FEATURE_LEVEL
		DXGI_SWAP_CHAIN_DESC swapChainDesc;
#else
		DXGI_SWAP_CHAIN_DESC1 swapChainDesc;
#endif	

		// Initialize the swap chain description.
		ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

		// Set to a single back buffer.
		swapChainDesc.BufferCount = 2;


#ifndef HIGHER_FEATURE_LEVEL
		// DEPRECATED
		//
		//swapChainDesc.BufferDesc.Width = scrWidth;
		swapChainDesc.BufferDesc.Width = 0;
//		swapChainDesc.BufferDesc.Height = scrHeight;
		swapChainDesc.BufferDesc.Height = 0;
		swapChainDesc.BufferDesc.Format = FrameBufferFormat;	// https://msdn.microsoft.com/en-us/library/windows/desktop/bb173064(v=vs.85).aspx
		if (ZSettings->bUseFullscreen)
		{	// Set the refresh rate of the back buffer.
			swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
			swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
		}
		else
		{
			swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
			swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
		}		
		swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT;
		swapChainDesc.OutputWindow = hwnd;	// Set the handle for the window to render to.
		//swapChainDesc.Windowed = !fullscreen;
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.SampleDesc.Quality = 0;
		swapChainDesc.Windowed = TRUE;
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;	
		//swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;	// Set the scan line ordering and scaling to unspecified.
		
		//swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_STRETCHED;
#else
		swapChainDesc.Width = scrWidth;
		swapChainDesc.Height = scrHeight;
		swapChainDesc.Format = FrameBufferFormat;	// https://msdn.microsoft.com/en-us/library/windows/desktop/bb173064(v=vs.85).aspx
		swapChainDesc.Scaling = DXGI_SCALING_STRETCH;
#endif
		
		
		//swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;	
		
		
		featureLevel = D3D_FEATURE_LEVEL_11_0;  //idk if 11_0 or 11_1 is right

#if defined( _DEBUG )
		UINT flags = D3D11_CREATE_DEVICE_DEBUG;

#else
		UINT flags = 0;
#endif

#ifndef HIGHER_FEATURE_LEVEL
	
		// Create the swap chain, Direct3D device, and Direct3D device context.
		result = D3D11CreateDeviceAndSwapChain(
			nullptr, //use primary video card
			D3D_DRIVER_TYPE_HARDWARE, 
			nullptr, 
			flags, 
			featureLevelArray, 
			2,
			D3D11_SDK_VERSION, 
			&swapChainDesc, 
			&mSwapChain, 
			&mDevice, 
			&featureLevel, 
			&mDeviceContext);
#else
		IDXGIDevice2 * pDXGIDevice;
		HRESULT hr = mDevice->QueryInterface(__uuidof(IDXGIDevice2), (void **)&pDXGIDevice);

		IDXGIAdapter * pDXGIAdapter;
		hr = pDXGIDevice->GetParent(__uuidof(IDXGIAdapter), (void **)&pDXGIAdapter);

		IDXGIFactory2 * pIDXGIFactory;
		pDXGIAdapter->GetParent(__uuidof(IDXGIFactory2), (void **)&pIDXGIFactory);
		result = pIDXGIFactory->CreateSwapChainForHwnd(
			mDevice,
			hwnd,
			&swapChainDesc,
			nullptr,
			nullptr,
			&mSwapChain
		);
#endif

		if (FAILED(result))
		{
			//LOG->error("D3DManager: Cannot create swap chain");
			LOG << "D3DManager: Cannot create swap chain";
			return false;
		}

#ifdef _DEBUG
		// Direct3D SDK Debug Layer
		//------------------------------------------------------------------------------------------
		// src1: https://blogs.msdn.microsoft.com/chuckw/2012/11/30/direct3d-sdk-debug-layer-tricks/
		// src2: http://seanmiddleditch.com/direct3d-11-debug-api-tricks/
		//------------------------------------------------------------------------------------------
		if ( SUCCEEDED(mDevice->QueryInterface(__uuidof(ID3D11Debug), (void**)&mDebug)) )
		{
			ID3D11InfoQueue* d3dInfoQueue = nullptr;
			if (SUCCEEDED(mDebug->QueryInterface(__uuidof(ID3D11InfoQueue), (void**)&d3dInfoQueue)))
			{
				d3dInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_CORRUPTION, true);
				d3dInfoQueue->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_ERROR, true);
				D3D11_MESSAGE_ID hide[] =
				{
					D3D11_MESSAGE_ID_DEVICE_DRAW_RENDERTARGETVIEW_NOT_SET,
					// Add more message IDs here as needed
				};
		
				D3D11_INFO_QUEUE_FILTER filter;
				memset(&filter, 0, sizeof(filter));
				filter.DenyList.NumIDs = _countof(hide);
				filter.DenyList.pIDList = hide;
				d3dInfoQueue->AddStorageFilterEntries(&filter);
				d3dInfoQueue->Release();
			}
		}

		if ( FAILED(mDeviceContext->QueryInterface(__uuidof(ID3DUserDefinedAnnotation), (void**)&mAnnotation)) )
		{
			LOG << "Can't Query(ID3DUserDefinedAnnotation)";
			return false;
		}
#endif
		//CHECK MSAA
		UINT m4xMsaaQuality;
		mDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, & m4xMsaaQuality);
		assert(m4xMsaaQuality > 0 );
		//LOG->info("MSAA is: {}", m4xMsaaQuality);

		//INITIALIZE DearIMGUI  http://stewmcc.com/post/dear-imgui-setup/
		ImGui_ImplDX11_Init(mDevice, mDeviceContext);
		//init engine as early as possible to avoid races
		ENGINE->SetDevice(mDevice);

		return true;

		
	}



	void D3DManager::Shutdown()
	{
		ImGui_ImplDX11_Shutdown();
		ReleaseCOM(mRenderTargetView);
		ReleaseCOM(mDepthStencilView);
		ReleaseCOM(mSwapChain);
		ReleaseCOM(mDepthStencilBuffer);
		if (mDeviceContext) {
			mDeviceContext->ClearState();
		}

		ReleaseCOM(mDeviceContext);

		//Check directx memory leaks, dumps to console
#ifdef _DEBUGFalse

		ID3D11Debug* DebugDevice = nullptr;
		HRESULT Result = mDevice->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast <void**>(&DebugDevice));
		Result = DebugDevice->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);

#endif

		ReleaseCOM(mDevice);
	}

	void D3DManager::PutProjectionMatrix(XMMATRIX& projectionMatrix)
	{
		projectionMatrix = mProjectionMatrix;
		return;
	}


	void D3DManager::PutWorldMatrix(XMMATRIX& worldMatrix)
	{
		worldMatrix = mWorldMatrix;
		return;
	}


	void D3DManager::PutOrthoMatrix(XMMATRIX& orthoMatrix)
	{
		orthoMatrix = mOrthoMatrix;
		return;
	}

	void D3DManager::BeginScene(float red, float green, float blue, float alpha)
	{
		float color[4];


		// Setup the color to clear the buffer to.
		color[0] = red;
		color[1] = green;
		color[2] = blue;
		color[3] = alpha;

		// Clear the back buffer.
		mDeviceContext->ClearRenderTargetView(mRenderTargetView, color);

		// Clear the depth buffer.
		mDeviceContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

		return;
	}

	void D3DManager::EndScene()
	{
		// Present the back buffer to the screen since rendering is complete.
		//if(m_vsync_enabled)
		//#TODO: This might be a bug
		if(true)
		{
			// Lock to screen refresh rate.
			mSwapChain->Present(1, 0);
		}
		else
		{
			// Present as fast as possible.
			mSwapChain->Present(0, 0);
		}

		return;
	}

	}
