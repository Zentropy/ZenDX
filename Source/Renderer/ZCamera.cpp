#include "ZCamera.h"
#include "Input/EngineInput.h"
#include "Utilities/Settings/ZenSettings.h"
#include "Utilities/Timer.h"
#include "Engine/Engine.h"

namespace Zen {

	using namespace DirectX;

ZCamera::ZCamera()
{
	
}

ZCamera::ZCamera(const ZCamera&)
{

}

ZCamera::~ZCamera()
{

}

void ZCamera::Initialize() {
	CameraSpeed = ZSettings->cameraSpeed;
	MouseLookSpeed = ZSettings->mouseLookSpeed;
	mbInvertMouseVertical = ZSettings->invertMouseVertical;
		

	mLookAt.x = -2.20f;
	mLookAt.y = -0.10f;
	mLookAt.z = 50.0f;

	
}

void ZCamera::SetPosition(float inX, float inY, float inZ)
{
	transform.SetPosition(inX, inY, inZ);
}

void ZCamera::SetRotation(float inX, float inY, float inZ)
{
	transform.SetRotationFromEuler(inX, inY, inZ);
}

void ZCamera::SetLookAt(float inX, float inY, float inZ)
{
	mLookAt.x = inX;
	mLookAt.y = inY;
	mLookAt.z = inZ;
}

DirectX::XMFLOAT3 ZCamera::GetPosition()
{
	return transform._position;
	//return DirectX::XMFLOAT3(mPositionX, mPositionY, mPositionZ);
}

DirectX::XMFLOAT3 ZCamera::GetRotation()
{
	return transform._rotation.ToEulerDeg(transform._rotation);
	//return DirectX::XMFLOAT3(mRotationX, mRotationY, mRotationZ);
}

vec3 ZCamera::GetForward()
{
	return (XMVECTOR)transform._rotation.quat * vec3::Forward;
}

void ZCamera::Update()
{
	float xDelta = 0, yDelta = 0, zDelta = 0;
	float dt = TIMER->DeltaTime();

	XMVECTOR translation = XMVectorSet(0,0,0,0);
	
	if (ENGINE->Input->IsKeyDown(KEY_D)) {
		xDelta += dt * CameraSpeed;
		translation += transform.GetRight() * dt * CameraSpeed;
	}
	else if (ENGINE->Input->IsKeyDown(KEY_A)) {
		xDelta -= dt * CameraSpeed;
		translation += transform.GetLeft() * dt * CameraSpeed;
	}
	if (ENGINE->Input->IsKeyDown(KEY_W)) {
		yDelta += dt * CameraSpeed;
		translation += transform.GetUp() * dt * CameraSpeed;
	}
	else if (ENGINE->Input->IsKeyDown(KEY_S)) {
		yDelta -= dt * CameraSpeed;
		translation += transform.GetDown() * dt * CameraSpeed;
	}

	if (ENGINE->Input->IsKeyDown(KEY_E)) {
		zDelta += dt * CameraSpeed;
		translation += transform.GetForward() * dt * CameraSpeed;
	}
	else if (ENGINE->Input->IsKeyDown(KEY_Q)) {
		zDelta -= dt * CameraSpeed;
		translation += transform.GetBack() * dt * CameraSpeed;
	}

	//auto pos = vec3(transform.xPos()+xDelta, transform.yPos()+yDelta, transform.zPos() + zDelta);
	//auto fwd = GetForward();
	//vec3 rotatedpos = pos * fwd;
	auto rotatedpos = (vec3)transform._position + (vec3)translation;
	transform.SetPosition(rotatedpos);
	//SetPosition(rotatedpos);
	//float mag = pos.Magnitude();
	//vec3 rotatedpos = pos * vec3(mLookAt);
	//rotatedpos.normalize();
	//rotatedpos = rotatedpos * mag;
	//LOG << "Camera pos is: " << rotatedpos.x() << " " << rotatedpos.y() << " " << rotatedpos.z();
	
	


	SetPosition(rotatedpos.x(), rotatedpos.y(), rotatedpos.z());


	xDelta = ENGINE->Input->MouseDeltaX() * dt * MouseLookSpeed;
	if (mbInvertMouseVertical) {
		xDelta *= -1.0f;
	}
	yDelta = ENGINE->Input->MouseDeltaY() * dt * MouseLookSpeed;
	zDelta = 0;
	if (ENGINE->Input->IsKeyDown(KEY_C)) {
		zDelta -= dt * CameraSpeed;
		
	}
	else if (ENGINE->Input->IsKeyDown(KEY_Z)) {
		zDelta += dt * CameraSpeed;
		
	}

	//transform.SetRotation(transform._rotation * ZQuaternion::FromEulerDeg(yDelta * 0.1f, xDelta * 0.1f, 0.0f));
	transform.AddXRotationDeg(yDelta );
	transform.AddYRotationDeg(xDelta );
	if (zDelta != 0.0f)
	{
		transform.AddZRotationDeg(zDelta);
	}
	
	
	//SetRotation(transform.xRot()+yDelta, transform.yRot()+xDelta, transform.zRot());

	//LOG << "New position is: " << mPositionX << ", " << mPositionY << ", " << mPositionZ;
	//LOG << "Mouse Delta is: " << ENGINE->Input->MouseDeltaX();
}

void ZCamera::Render()
{
	XMFLOAT3 up, position;
	XMVECTOR upVector, positionVector, lookAtVector;
	//float yaw, pitch, roll;
	XMMATRIX rotationMatrix;


	// Setup the vector that points upwards.
	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;

	// Load it into a XMVECTOR structure.
	upVector = XMLoadFloat3(&up);

	// Setup the position of the camera in the world.
	position.x = transform.xPos();
	position.y = transform.yPos();
	position.z = transform.zPos();

	// Load it into a XMVECTOR structure.
	positionVector = XMLoadFloat3(&position);

	//asdf

	// Load it into a XMVECTOR structure.
	lookAtVector = XMLoadFloat3(&mLookAt);

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	rotationMatrix = transform._rotation.Matrix();
	//pitch = transform.xRot() * 0.0174532925f * DEG2RAD;
	//yaw   = transform.yRot() * 0.0174532925f * DEG2RAD;
	//roll  = transform.zRot() * 0.0174532925f * DEG2RAD;

	// Create the rotation matrix from the yaw, pitch, and roll values.
	//rotationMatrix = XMMatrixRotationRollPitchYaw(pitch, yaw, roll);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	lookAtVector = XMVector3TransformCoord(lookAtVector, rotationMatrix);
	upVector = XMVector3TransformCoord(upVector, rotationMatrix);

	// Translate the rotated camera position to the location of the viewer.
	lookAtVector = XMVectorAdd(positionVector, lookAtVector);

	// Finally create the view matrix from the three updated vectors.
	mViewMatrix = XMMatrixLookAtLH(positionVector, lookAtVector, upVector);

	return;
}

}
