﻿#pragma once
#include <memory>
#include <string>
#include "Utilities/utils.h"
#include <d3d11.h>
#include "ZTexture.h"
#include "ZShader.h"



namespace DirectX {
struct XMMATRIX;
}

namespace Zen {



//TextureID	diffuseMap;
//	TextureID	normalMap;
//	//-----------------------------------
//	TextureID	heightMap;
//	TextureID	specularMap;
//	TextureID	roughnessMap;
//TextureID mask;

class ZMaterial {

public:
	ZMaterial(){};
	~ZMaterial();

	ZMaterial(EShaderTypes shaderType, ID3D11Device* inDevice, HWND inHwnd, const WCHAR* vsFileName, const WCHAR* psFileName);	
	
	virtual void SetNewTexture(const std::wstring& texturePath, ETextureTypes textureType = ETextureTypes::DIFFUSE);
	void Render(ID3D11DeviceContext* deviceContext, int indexCount, const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& viewMatrix, const DirectX::XMMATRIX& projectionMatrix);
	
	std::shared_ptr<Zen::ZTexture> diffuseTex;
	std::unique_ptr<ZShader> shader;

	friend class Renderer;

};

class ZMaterialLit : public ZMaterial {
	std::shared_ptr<Zen::ZTexture> normalTex;

	ZMaterialLit(EShaderTypes shaderType, ID3D11Device* inDevice, HWND inHwnd, const WCHAR* vsFileName, const WCHAR* psFileName);

	virtual void SetNewTexture(const std::wstring& texturePath, ETextureTypes textureType = ETextureTypes::DIFFUSE);
};

}
