﻿//	VQEngine | DirectX11 Renderer
//	Copyright(C) 2018  - Volkan Ilbeyli
//
//	This program is free software : you can redistribute it and / or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program.If not, see <http://www.gnu.org/licenses/>.
//
//	Contact: volkanilbeyli@gmail.com

#include "ZTexture.h"
#include "Renderer.h"
#include "Utilities/ZLog.h"

#include <d3d11.h>

#include "WICTextureLoader.h"
#include <d3d11.h>
#include <wrl/client.h>
#include <boost/algorithm/string/predicate.hpp>
#include <DirectXTex.h>
#include <string>
#include <pplawait.h>
#include <thread>

namespace Zen
{

	std::atomic<HRESULT> didSucceed = -1;

	void CreateTextureAsync(ID3D11Device* inDevice, const std::wstring& inFilePath, ID3D11Resource** inTexture, ID3D11ShaderResourceView** inSRV, size_t inMaxSize) 
	{
		LOG << "Creating async";
		auto res =  DirectX::CreateWICTextureFromFile(inDevice,
		                                             inFilePath.c_str(),
		                                             inTexture,
		                                             inSRV,
		                                             inMaxSize);
		didSucceed.store(res);
	}

ZTexture::ZTexture(const std::wstring& filePath, ID3D11Device* inDevice)
	:
	_srv(nullptr), // assigned and deleted by renderer
	_tex2D(nullptr),
	_width(0),
	_height(0),
	_depth(0),

	_name(""),
	_id(-1)
{
	//Microsoft::WRL::ComPtr<ID3D11Resource> texture = nullptr;
	ID3D11Resource* texture = nullptr;
	long maxSize = 1410065407L;
	
	Microsoft::WRL::ComPtr<ID3D11Texture2D> tex;
	//don't use this if it's a .tga
	if (boost::algorithm::ends_with(filePath, "tga"))
	{
		LOG << "FOUND TGA";
		auto image = std::make_unique<ScratchImage>();
		auto res = LoadFromTGAFile(filePath.c_str(), nullptr, *image);
		const Image* img = image->GetImage(0,0,0);
		//ID3D11Resource* texptr = texture.Get();
		//CreateTexture(inDevice, img, 1, image->GetMetadata(), &texptr);
		CreateTexture(inDevice, img, 1, image->GetMetadata(), &texture);
		//texture = texptr;
		LOG << "Loaded TGA";
		_name = StrUtil::WideToString(DirectoryUtil::GetFileNameWithoutExtension(filePath));
	}
	else
	{

		std::thread t(CreateTextureAsync,
			inDevice,
		                                             filePath.c_str(),
		                                             &texture,
		                                             &_srv,
		                                             maxSize);
		//ID3D11Resource* texRes = texture.Get()
		//this is slow as balls
		//CreateTextureAsync(inDevice,
		//                                             filePath.c_str(),
		//                                             &texture,
		//                                             &_srv,
		//                                             maxSize);

// 		if (FAILED(res)) {
// 			LOG << "Failed creating tex async";
// 		}

		t.join();

		_name = StrUtil::WideToString(DirectoryUtil::GetFileNameWithoutExtension(filePath));
	}
	
	if (texture == nullptr) {//load default texture
			
		//C:\Workspace\CPP\ZenDX\Data\Textures\DebugTextures
		std::wstring defaultFilePath = DirectoryUtil::GetDataDirectoryW() + L"Textures/DebugTextures/defaultTexture.png";
		auto res = DirectX::CreateWICTextureFromFile(inDevice,
		                                             defaultFilePath.c_str(),
		                                             &texture,
		                                             &_srv,
		                                             maxSize);
		_name = StrUtil::WideToString(DirectoryUtil::GetFileNameWithoutExtension(defaultFilePath));
	}
	
	//Microsoft::WRL::ComPtr<ID3D11Resource> textureptr;
	//textureptr.Attach(texture);
	texture->QueryInterface(&_tex2D);

	if (texture != nullptr) {
		//textureptr.As(&tex);
		//_tex2D = tex.Get();
		auto hash = StrUtil::GetHash(filePath);
		LOG << "Loaded texture: " << filePath << " With hash: " << hash << " and int: " << (int)hash;
		return;
	}

	else {
		LOG << "Tex load failed";
		return;
	}

	
}

ZTexture::~ZTexture()
{
}

//bool ZTexture::InitializeTexture2D(const D3D11_TEXTURE2D_DESC& descriptor, Renderer* pRenderer, bool initializeSRV)
//{
//	HRESULT hr = pRenderer->m_device->CreateTexture2D(&descriptor, nullptr, &this->_tex2D);
//	if (!SUCCEEDED(hr))
//	{
//		Log::Error("Texture::InitializeTexture2D(): Cannot create texture2D");
//		return false;
//	}
//	this->_width = descriptor.Width;
//	this->_height = descriptor.Height;
//
//	if (initializeSRV)
//	{
//		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
//		srvDesc.Format = descriptor.Format;
//		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;	// array maybe? check descriptor.
//		srvDesc.Texture2D.MipLevels = descriptor.MipLevels;
//		pRenderer->m_device->CreateShaderResourceView(this->_tex2D, &srvDesc, &this->_srv);
//	}
//	return true;
//}

void ZTexture::Release()
{
	if (_srvArray.size() > 0)
	{
		for (size_t i = 0; i < _srvArray.size(); ++i)
		{
			if (_srvArray[i])
			{
				_srvArray[i]->Release();
				_srvArray[i] = nullptr;
			}
		}
	}
	else
	{
		if (_srv)
		{
			_srv->Release();
			_srv = nullptr;
		}
	}

	if (_tex2D)
	{
		_tex2D->Release();
		_tex2D = nullptr;
	}
}
}
