#include "Renderer.h"
#include "D3DManager.h"
#include "d3dUtil.h"
#include <Utilities/Timer.h>
#include <Utilities/ZLog.h>
#include "GUI/ZimGUI.h"
#include "Engine/Engine.h"
#include "ZTexture.h"
#include "Tests/ZMesh.h"
#include <Engine/Model.h>
#include "Input/EngineInput.h"

//#include "DirectXTex/DirectXTex.h"


namespace Zen
{
using namespace DirectX;


Renderer::Renderer(D3DManager& inD3DManager)
	:mD3DManager(inD3DManager)
{
	mScene = ENGINE->GetCurrentScene();
}

Renderer::~Renderer()
{
}

void Renderer::SetRenderTest(int inTest)
{
	renderTest = inTest;
}

bool Renderer::Initialize(HWND inHwnd)
{
	hwnd = inHwnd;
	//mD3DManager = std::make_unique<D3DManager>();
	//mD3DManager = inD3DManager;

	const bool result = mD3DManager.Initialize(inHwnd, DXGI_FORMAT_R16G16B16A16_FLOAT);
	// swapchain should be bgra unorm 32bit);

	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize Direct3D", L"Error", MB_OK);
		return false;
	}

	//grab all the necessary rendering stuff from the d3d manager so we can basically forget about it now
	mDevice = mD3DManager.mDevice;
	
	mDeviceContext = mD3DManager.mDeviceContext;
	mRenderTargetView = mD3DManager.mRenderTargetView;
	backBuffer = mD3DManager.backBuffer;

	mDepthStencilBuffer = mD3DManager.mDepthStencilBuffer;
	mDepthStencilView = mD3DManager.mDepthStencilView;
	mSwapChain = mD3DManager.mSwapChain;

	mCamera = std::make_unique<ZCamera>();
	mCamera->Initialize();
	mCamera->SetPosition(0, 0.f, -15.f);

	
	auto GO1 = mScene->CreateMesh("pbrstone/Stone.dae", "Stone");
	GO1->SetScaleUniform(0.25f);
	//auto GO1 = mScene->CreateMesh("nanosuit/nanosuit.obj", "NanoSuit");
	//auto GO1 = mScene->CreateMesh("NewBox/OneBoxMiddle.fbx", "OneBox");
	//auto GO2 = mScene->CreateMesh("NewBox/NewBoxTwo.fbx", "NewBoxTwo2");
	//mScene->AttachGameObjectToParent(GO2, GO1);
	//GO2->Translate(1, 1, 1);
	//mScene->CreateMesh("sponzaNew/sponza.fbx");
	
	return true;
}

void Renderer::CalculateFrameStats()
{
	// Code computes the average frames per second, and also the 
	// average time it takes to render one frame.  These stats 
	// are appended to the window caption bar.

	static int frameCnt = 0;
	static float timeElapsed = 0.0f;

	frameCnt++;

	// Compute averages over one second period.
	if ((TIMER->TotalTime() - timeElapsed) >= 1.0f)
	{
		float fps = (float)frameCnt; // fps = frameCnt / 1
		float mspf = 1000.0f / fps;

		std::wostringstream outs;
		outs.precision(6);
		outs << "ZenEngine" << "    "
		<< "FPS: " << fps << "    "
		<< "Frame Time: " << mspf << " (ms)";
		auto first = GetLastError();
		SetWindowText(hwnd, outs.str().c_str());
		auto second = GetLastError();
		// Reset for next average.
		frameCnt = 0;
		timeElapsed += 1.0f;
	}
}

bool Renderer::ClearRenderTargetView(ColorAlias clearColor)
{
	mDeviceContext->ClearRenderTargetView(mRenderTargetView, reinterpret_cast<const float*>(&clearColor));
	return true;
}

bool Renderer::ClearDepthStencilView(UINT inClearFlags, float inDepth, UINT8 inStencil)
{
	mDeviceContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0, 0);
	return true;
}

int Renderer::Tick()
{
	CalculateFrameStats();
	////LOG->info("Ticking");
	//mScene->UpdateScene(TIMER->DeltaTime());
	mCamera->Update();
	//mModel->Tick();
	DrawScene();

	/////////////////////// TEST INPUT
	if (ENGINE->Input->IsKeyDown(KEY_F))
	{
		LOG << "TRANSLATING";
		const auto& scene = ENGINE->GetCurrentScene();
		const auto go = scene->FindGameObjectByName("pCube1");
		go->Translate(0.1f, 0.1f, 0.1f);
	}
	if (ENGINE->Input->IsKeyDown(KEY_G))
	{
		LOG << "TRANSLATING";
		const auto& scene = ENGINE->GetCurrentScene();
		const auto& go = scene->FindGameObjectByName("Cube");
		go->Translate(0.1f, 0.1f, 0.1f);
	}
	if (ENGINE->Input->IsKeyDown(KEY_H))
	{
		LOG << "TRANSLATING";
		const auto& scene = ENGINE->GetCurrentScene();
		const auto& go = scene->FindGameObjectByName("");
		go->Translate(0.1f, 0.1f, 0.1f);
	}
// 	if (ENGINE->Input->IsKeyDown(KEY_T))
// 	{
// 		LOG << "Switching Texture";
// 		const auto& scene = ENGINE->GetCurrentScene();
// 		const auto& go = scene->FindGameObjectByName("Cube");
// 		go->mMesh->mMaterial->SetNewTexture(L"C:/Workspace/CPP/ZenDX/Build/ZenApplication/x64/Debug/Data/Models/NewBox/textures/awesomefaceblue.png");
// 	}

	


	return 0;
}

float mPhi = 0.25f * 3.1415926f;
float mTheta = 1.5f * 3.1415926f;

float Renderer::GetAspectRatio()
{
	auto client = ENGINE->GetClientRect();

	return static_cast<float>(client.right) / client.bottom;
}

void Renderer::DrawScene()
{
	assert (mDeviceContext);
	assert(mDevice);

	////LOG->info("Drawing scene");
	//mScene->DrawScene();


	//rastertek.com
	
	//bool result;


	// Clear the buffers to begin the scene.
	mD3DManager.BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

	// Generate the view matrix based on the camera's position.
	float mRadius = 35.0f;
	mPhi += 0.01f;
	mTheta += 0.01f;
	float x = mRadius * sinf(mPhi) * cosf(mTheta);
	float z = mRadius * sinf(mPhi) * sinf(mTheta);
	float y = mRadius * cosf(mPhi);
	//mCamera->SetPosition(x, y, z);
	////dbg
	//mCamera->SetPosition(0, 0.f, -25.f);
	mCamera->SetLookAt(0.f, 0.f, 1.f);
	mCamera->Render();

	// Get the world, view, and projection matrices from the camera and d3d objects.
	mD3DManager.PutWorldMatrix(worldMatrix);
	//projectionMatrix = XMMatrixPerspectiveFovLH(0.25f * 3.1415926f, 800.0f / 600.0f, 1.0f, 1000.0f);
	mD3DManager.PutProjectionMatrix(projectionMatrix);
	mCamera->PutViewMatrix(viewMatrix);

	// Convert Spherical to Cartesian coordinates.

	// Build the view matrix.
	//XMVECTOR pos = XMVectorSet(x, y, z, 1.0f);
	//XMVECTOR target = XMVectorZero();
	//XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	//viewMatrix = XMMatrixLookAtLH(pos, target, up);


	// Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.
	//mModel->Render(mDeviceContext, worldMatrix, viewMatrix, projectionMatrix);
	//TODO: Render Model.h here
	//mModelClass->Render(mDeviceContext, worldMatrix, viewMatrix, projectionMatrix);
	
	mScene->Render();

	// Render the model using the color shader.
	//result = mShader->Render(mDeviceContext, mModel->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix);
	//mMaterial->Render(mDeviceContext, mModel->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix);
	//if (!result)
	//{
	//	return;
	//}

	if (ENGINE->isEditorOpen) {
		ZimGUI::CreateRenderData();
		ZimGUI::Draw();	
	}
	
	mDeviceContext->OMSetRenderTargets(1, &(mD3DManager.mRenderTargetView), mD3DManager.mDepthStencilView);
	// Present the rendered scene to the screen.
	mD3DManager.EndScene();


	//flip back buffer to screen
	//HR(mSwapChain->Present(0, 0));
}
}
