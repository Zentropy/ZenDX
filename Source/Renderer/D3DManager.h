#pragma once
#include <d3d11_1.h>
#include <string>
#include <DirectXMath.h>

namespace Zen {

	class D3DManager {
		friend class Renderer;

	public:
		D3DManager();
		~D3DManager();

		bool Initialize(HWND hwnd, DXGI_FORMAT FrameBufferFormat);
		bool InitDepthStencil();
		void Shutdown();

		void PutProjectionMatrix(DirectX::XMMATRIX& projectionMatrix);
		void PutWorldMatrix(DirectX::XMMATRIX& worldMatrix);
		void PutOrthoMatrix(DirectX::XMMATRIX& orthoMatrix);
		void BeginScene(float red, float green, float blue, float alpha);
		void EndScene();

	private:

		bool InitSwapChain(HWND hwnd, bool fullscreen, int scrWidth, int scrHeight, unsigned numerator, unsigned denominator, DXGI_FORMAT FrameBufferFormat);


		//bool						mVsyncEnabled;
		int							mVRAM;
		char						mGPUDescription[128];
		HWND						mHwnd;
		D3D11_VIEWPORT				vp;
		//bool mbEnableMSAA4x = true;

//#define HIGHER_FEATURE_LEVEL 1

#ifndef HIGHER_FEATURE_LEVEL
		IDXGISwapChain*			mSwapChain;
#else
		IDXGISwapChain1*			mSwapChain;
		IDXGIFactory2*				mDXGIFactory;
#endif

		ID3D11Device*				mDevice;			// shared ptr
		ID3D11DeviceContext*		mDeviceContext;

		ID3D11RenderTargetView* mRenderTargetView;
		ID3D11Texture2D* backBuffer;

		ID3D11Texture2D* mDepthStencilBuffer;
		ID3D11DepthStencilView* mDepthStencilView;

		//unsigned					mWndWidth, mWndHeight;

		DirectX::XMMATRIX mProjectionMatrix;
		DirectX::XMMATRIX mWorldMatrix;
		DirectX::XMMATRIX mOrthoMatrix;


#if _DEBUG
		ID3D11Debug*				mDebug;
		ID3DUserDefinedAnnotation*	mAnnotation;
#endif
	};

}