﻿#pragma once
#include <DirectXMath.h>
#include <d3d11.h>
#include "Renderer/ZShader.h"
#include "Renderer/ZTexture.h"
#include <unordered_map>
#include <array>

namespace Zen {

	class DiffuseShader : public ZShader {
		
	public:
		DiffuseShader();
		virtual ~DiffuseShader();

		bool InitializeShader(ID3D11Device* inDevice, HWND inHwnd, const WCHAR* vsFileName, const WCHAR* psFileName) override;
		//void OutputShaderErrorMessage(ID3D10Blob* errorMessage, HWND hwnd, WCHAR* shaderFilename);
		//
		//void Shutdown();
		bool Render(ID3D11DeviceContext* deviceContext, int indexCount, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix) override; 

	private:	

		bool SetShaderParameters(ID3D11DeviceContext* deviceContext, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix) override;
		void RenderShader(ID3D11DeviceContext* deviceContext, int indexCount) override;
		ID3D11SamplerState* m_sampleState;
		ID3D11Device* mDevice;
		//std::vector<ID3D11ShaderResourceView*> textures;
		//std::unordered_map<ETextureTypes, ID3D11ShaderResourceView*> textureMap;
		std::array<ID3D11ShaderResourceView*, 1> textureArray;
	public:
		virtual bool SetTexture(ETextureTypes& textureType, ZTexture* texture);

	};

}
