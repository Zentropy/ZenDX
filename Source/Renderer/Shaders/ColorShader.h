#pragma once
#include <DirectXMath.h>
#include <d3d11.h>
#include "Renderer/ZShader.h"

namespace Zen {

	class ColorShader : public ZShader {
		

	


	public:
		ColorShader();
		virtual ~ColorShader();

		bool InitializeShader(ID3D11Device* inDevice, HWND inHwnd, const WCHAR* vsFileName, const WCHAR* psFileName) override;
		//void OutputShaderErrorMessage(ID3D10Blob* errorMessage, HWND hwnd, WCHAR* shaderFilename);
		//
		//void Shutdown();
		bool Render(ID3D11DeviceContext* deviceContext, int indexCount, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix) override; 

	private:	

		//bool SetShaderParameters(ID3D11DeviceContext* deviceContext, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix);
		//void RenderShader(ID3D11DeviceContext* deviceContext, int indexCount);
	};

}
