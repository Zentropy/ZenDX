﻿#include "LitShader.h"
#include <string>
#include "Utilities/utils.h"

#include <d3dcompiler.h>
#include "Utilities/ZLog.h"
#include <DirectXMath.h>
#include "../d3dUtil.h"

namespace Zen {

	LitShader::LitShader()
	{
		//textureMap.clear();
		mLightBuffer = nullptr;
	}

	LitShader::~LitShader()
	{
		Shutdown();
	}

	bool LitShader::InitializeShader(ID3D11Device* inDevice, HWND inHwnd, const WCHAR* vsFileName, const WCHAR* psFileName)
	{
		std::wstring strres = DirectoryUtil::GetExeDirectoryW();
		mDevice = inDevice;
		strres += L"Shaders/";
		std::wstring vsFilePath = strres + vsFileName;
		std::wstring psFilePath = strres + psFileName;
		//std::wstring thisissoshitty = std::wstring(strres.begin(), strres.end());


		//HR(D3DX11CreateEffectFromFile(thisissoshitty.c_str(), shaderFlags, inDevice, &mFX));


		HRESULT result;
		ID3D10Blob* errorMessage;
		ID3D10Blob* vertexShaderBuffer;
		ID3D10Blob* pixelShaderBuffer;
		D3D11_INPUT_ELEMENT_DESC polygonLayout[3];
		unsigned int numElements;
		D3D11_BUFFER_DESC matrixBufferDesc;
		D3D11_BUFFER_DESC lightBufferDesc;


		// Initialize the pointers this function will use to null.
		errorMessage = 0;
		vertexShaderBuffer = 0;
		pixelShaderBuffer = 0;

		// Compile the vertex shader code.
		result = D3DCompileFromFile(vsFilePath.c_str(), nullptr, nullptr, "PixelShaderMain", "vs_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, &vertexShaderBuffer, &errorMessage);
		if(FAILED(result))
		{
			// If the shader failed to compile it should have writen something to the error message.
			if(errorMessage)
			{
				OutputShaderErrorMessage(errorMessage, inHwnd, vsFileName);
			}
			// If there was  nothing in the error message then it simply could not find the shader file itself.
			else
			{
				MessageBox(inHwnd, vsFileName, L"Missing Shader File", MB_OK);
			}

			return false;
		}

		// Compile the pixel shader code.
		result = D3DCompileFromFile(psFilePath.c_str(), nullptr, nullptr, "PixelShaderMain", "ps_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, &pixelShaderBuffer, &errorMessage);
		if(FAILED(result))
		{
			// If the shader failed to compile it should have writen something to the error message.
			if(errorMessage)
			{
				OutputShaderErrorMessage(errorMessage, inHwnd, psFileName);
			}
			// If there was nothing in the error message then it simply could not find the file itself.
			else
			{
				MessageBox(inHwnd, psFileName, L"Missing Shader File", MB_OK);
			}

			return false;
		}

		// Create the vertex shader from the buffer.
		result = inDevice->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), nullptr, &mVertexShader);
		if(FAILED(result))
		{
			return false;
		}

		// Create the pixel shader from the buffer.
		result = inDevice->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), nullptr, &mPixelShader);
		if(FAILED(result))
		{
			return false;
		}

		// Create the vertex input layout description.
		// This setup needs to match the VertexType stucture in the ModelClass and in the shader.
		polygonLayout[0].SemanticName = "POSITION";
		polygonLayout[0].SemanticIndex = 0;
		polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
		polygonLayout[0].InputSlot = 0;
		polygonLayout[0].AlignedByteOffset = 0;
		polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		polygonLayout[0].InstanceDataStepRate = 0;

		polygonLayout[1].SemanticName = "TEXCOORD";
		polygonLayout[1].SemanticIndex = 0;
		polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
		polygonLayout[1].InputSlot = 0;
		polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
		polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		polygonLayout[1].InstanceDataStepRate = 0;

		polygonLayout[2].SemanticName = "NORMAL";
		polygonLayout[2].SemanticIndex = 0;
		polygonLayout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
		polygonLayout[2].InputSlot = 0;
		polygonLayout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
		polygonLayout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		polygonLayout[2].InstanceDataStepRate = 0;

		// Get a count of the elements in the layout.
		numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

		// Create the vertex input layout.
		result = inDevice->CreateInputLayout(polygonLayout, numElements, vertexShaderBuffer->GetBufferPointer(), 
			vertexShaderBuffer->GetBufferSize(), &mInputLayout);
		if(FAILED(result))
		{
			LOG << "ERROR: Failed creating Input Layout in LitShader";
			return false;
		}

		// Release the vertex shader buffer and pixel shader buffer since they are no longer needed.
		vertexShaderBuffer->Release();
		vertexShaderBuffer = 0;

		pixelShaderBuffer->Release();
		pixelShaderBuffer = 0;

		// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
		matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
		matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		matrixBufferDesc.MiscFlags = 0;
		matrixBufferDesc.StructureByteStride = 0;

		// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		result = inDevice->CreateBuffer(&matrixBufferDesc, nullptr, &mMatrixBuffer);
		if(FAILED(result))
		{
			return false;
		}
		D3D11_SAMPLER_DESC samplerDesc;

		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.MipLODBias = 0.0f;
		samplerDesc.MaxAnisotropy = 1;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		samplerDesc.BorderColor[0] = 0;
		samplerDesc.BorderColor[1] = 0;
		samplerDesc.BorderColor[2] = 0;
		samplerDesc.BorderColor[3] = 0;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		// Create the texture sampler state.
		HRESULT sampleres = mDevice->CreateSamplerState(&samplerDesc, &m_sampleState);
		if (FAILED(sampleres))
		{
			return false;
		}

		//Here we setup the light constant buffer description which will handle the diffuse light color and light direction.Pay attention to the size of the constant buffers, if they are not multiples of 16 you need to pad extra space on to the end of them or the CreateBuffer function will fail.In this case the constant buffer is 28 bytes with 4 bytes padding to make it 32.

		// Setup the description of the light dynamic constant buffer that is in the pixel shader.
		// Note that ByteWidth always needs to be a multiple of 16 if using D3D11_BIND_CONSTANT_BUFFER or CreateBuffer will fail.
		lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		lightBufferDesc.ByteWidth = sizeof(LightBufferType);
		lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		lightBufferDesc.MiscFlags = 0;
		lightBufferDesc.StructureByteStride = 0;

		// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
		result = inDevice->CreateBuffer(&lightBufferDesc, NULL, &mLightBuffer);
		if (FAILED(result))
		{
			LOG << "Failed creating light buffer";
			return false;
		}



		return true;
	}





	void LitShader::Shutdown()
	{
		ZShader::Shutdown();
		ReleaseCOM(mLightBuffer);
	}

	bool LitShader::Render(ID3D11DeviceContext* deviceContext, int indexCount, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix)
	{
		bool result = true;


		// Set the shader parameters that it will use for rendering.
		//HERE NOW
		//result = SetShaderParameters(deviceContext, worldMatrix, viewMatrix, projectionMatrix, light);
		if(!result)
		{
			return false;
		}

		// Now render the prepared buffers with the shader.
		RenderShader(deviceContext, indexCount);

		return true;
	}



	bool LitShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix
		, DirectX::XMMATRIX projectionMatrix, DirectX::XMFLOAT3 lightDirection, DirectX::XMFLOAT4 diffuseColor)
	{
		ZShader::SetShaderParameters(deviceContext, worldMatrix, viewMatrix, projectionMatrix);

		D3D11_MAPPED_SUBRESOURCE mappedResource;
		LightBufferType* dataPtr2;
		
		int result = deviceContext->Map(mLightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		if (FAILED(result))
		{
			return false;
		}

		// Get a pointer to the data in the constant buffer.
		dataPtr2 = (LightBufferType*)mappedResource.pData;

		// Copy the lighting variables into the constant buffer.
		dataPtr2->diffuseColor = diffuseColor;
		dataPtr2->lightDirection = lightDirection;
		dataPtr2->padding = 0.0f;

		// Unlock the constant buffer.
		deviceContext->Unmap(mLightBuffer, 0);

		// Set the position of the light constant buffer in the pixel shader.
		int bufferNumber = 0;

		// Finally set the light constant buffer in the pixel shader with the updated values.
		deviceContext->PSSetConstantBuffers(bufferNumber, 1, &mLightBuffer);

		deviceContext->PSSetShaderResources(0, 2, (textureArray.data()));
		return true;
	}

	bool LitShader::SetTexture(ETextureTypes& textureType, ZTexture* texture)
	{
		//brute force the texture set, don't care if an old texture was already there
		//textureMap.insert_or_assign(textureType, texture->_srv);
		textureArray[(int)textureType] = texture->_srv;

		//textures.push_back(inTextures[0]->_srv);

		return true;

	}

	void LitShader::RenderShader(ID3D11DeviceContext* deviceContext, int indexCount)
	{
		// Set the vertex input layout.
		deviceContext->IASetInputLayout(mInputLayout);

		// Set the vertex and pixel shaders that will be used to render this triangle.
		deviceContext->VSSetShader(mVertexShader, nullptr, 0);
		deviceContext->PSSetShader(mPixelShader, nullptr, 0);

		// Set the sampler state in the pixel shader.
		deviceContext->PSSetSamplers(0, 1, &m_sampleState);

		// Render the triangle.
		deviceContext->DrawIndexed(indexCount, 0, 0);

		return;
	}

}