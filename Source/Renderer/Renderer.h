#pragma once
#include <windows.h>
#include <d3d11.h>
#include "RendererEnums.h"
#include <memory>

#include "ZShader.h"
#include "ZCamera.h"
#include <Renderer/Tests/TriangleModel.h>
#include "Utilities/Settings/ZenSettings.h"
#include "Renderer/Tests/BoxModel.h"
#include "Shaders/DiffuseShader.h"
#include "ZMaterial.h"
#include "Scenes/Scene.h"
#include "D3DManager.h"
#define ZEN_API __declspec(dllexport)

namespace Zen {

class Renderer {
public:
	int renderTest = 5;

	Renderer(D3DManager& inD3DManager);
	~Renderer();

	void SetRenderTest(int inTest);

	bool Initialize(HWND inHwnd);

	void CalculateFrameStats();
	bool ClearRenderTargetView(ColorAlias clearColor);

	bool ClearDepthStencilView(UINT inClearFlags, float inDepth, UINT8 inStencil);
	int Tick();

	HWND hwnd;
	ID3D11Device*					mDevice;
	ID3D11DeviceContext*			mDeviceContext;
	//class D3DManager;
	D3DManager&				mD3DManager;
	void SetScene(std::shared_ptr<Scene>& inScene) {mScene = inScene;}

	float GetAspectRatio();
	XMMATRIX& GetViewMatrix() {return viewMatrix;}
	XMMATRIX& GetProjectionMatrix() {return projectionMatrix;}

	void DrawScene();
	std::shared_ptr< Scene>         mScene;
	ID3D11RenderTargetView* mRenderTargetView;
	ID3D11Texture2D* backBuffer;

	ID3D11Texture2D* mDepthStencilBuffer;
	ID3D11DepthStencilView* mDepthStencilView;

#ifndef HIGHER_FEATURE_LEVEL
	IDXGISwapChain*			mSwapChain;
#else
	IDXGISwapChain1*			mSwapChain;
#endif
	   	 
	std::unique_ptr<ZCamera> mCamera;
	//std::unique_ptr<class IMesh> mModel;
	//std::unique_ptr<class Model> mModelClass;
	//std::unique_ptr<TriangleModel> mModel;
	//std::unique_ptr<ZShader> mShader;

private:
	XMMATRIX worldMatrix, viewMatrix, projectionMatrix;


};

}
