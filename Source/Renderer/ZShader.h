#pragma once
#include <DirectXMath.h>
#include <d3d11.h>
#include <vector>
#include "ZTexture.h"


#include "RendererEnums.h"

namespace Zen {

class ZShader {
protected:
	

	ID3D11VertexShader* mVertexShader;
	ID3D11PixelShader* mPixelShader;
	ID3D11InputLayout* mInputLayout;
	ID3D11Buffer* mMatrixBuffer;

	struct MatrixBufferType
	{
		DirectX::XMMATRIX world;
		DirectX::XMMATRIX view;
		DirectX::XMMATRIX projection;
	};

	

public:
	ZShader();
	virtual ~ZShader();

	virtual bool InitializeShader(ID3D11Device* inDevice, HWND inHwnd, const WCHAR* vsFileName, const WCHAR* psFileName) = 0;
	void OutputShaderErrorMessage(ID3D10Blob* errorMessage, HWND hwnd, const WCHAR* shaderFilename);

	virtual void Shutdown();
	virtual bool Render(ID3D11DeviceContext* deviceContext, int indexCount, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix) = 0;
	//virtual bool SetTextures(std::vector<ZTexture*> textures) { return true; };
	virtual bool SetTexture(ETextureTypes& textureType, ZTexture* texture) { return true; };

protected:	

	virtual bool SetShaderParameters(ID3D11DeviceContext* deviceContext, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix);
	
	virtual void RenderShader(ID3D11DeviceContext* deviceContext, int indexCount);
};

}
