﻿#pragma once
#include <DirectXMath.h>
#include <Engine/IComponent.h>

namespace Zen {

class Light : public IComponent {

public:
	Light();	
	~Light();
	
	void SetDiffuseColor(float r, float g, float b, float a);
	void SetDirection(float x, float y, float z);

	DirectX::XMFLOAT4 GetDiffuseColor();
	DirectX::XMFLOAT3 GetDirection();

private:
	DirectX::XMFLOAT4 mDiffuseColor;
	DirectX::XMFLOAT3 mDirection;
	
};
}
