#pragma once
#include <d3d11.h>
#include "../ZMaterial.h"
#include "Engine/Transform.h"
#include <Engine/GameObject.h>
#include <map>
#include "Scenes/Scene.h"
#include <string>
#include <Engine/IComponent.h>


namespace DirectX {
struct XMMATRIX;
}

namespace Zen {

class IMesh : public IComponent {

public:
	IMesh(GameObjectPtr inOwningGO);
	IMesh(const IMesh&);
	virtual ~IMesh();

	//Creates buffers then returns, don't need to override
	virtual bool Initialize(ID3D11Device* device);
	virtual void Shutdown();
	virtual void Render();
	virtual void Tick() {};

	virtual int GetIndexCount();
	std::shared_ptr<ZMaterial> GetMaterial() {return mMaterial;}

	//inline int GetMaterialIndex() { return mMaterialIndex;}
	Transform* transform;

	std::string MeshName;

	void SetMeshName(const std::string& inMeshName);
	inline size_t GetHash() {return mMeshHash;}

protected:
	virtual void ShutdownBuffers();
	virtual bool InitializeBuffers(ID3D11Device* device) = 0;	
	virtual void RenderBuffers(ID3D11DeviceContext* deviceContext) = 0;

	//ZMaterial* GetMaterialFromIndex(int inIndex);	
	//std::unordered_map<int, std::shared_ptr<ZMaterial> > Materials;
	

protected:
	ID3D11Buffer *mVertexBuffer, *mIndexBuffer;
	GameObjectPtr mGameObject;
	int mVertexCount, mIndexCount;
	//int mMaterialIndex;
	size_t mMeshHash;
	std::shared_ptr<ZMaterial> mMaterial;

	friend class ZModelImporter;
	friend class Renderer;
};

}
