#include "IMesh.h"
#include <DirectXMath.h>
#include "Renderer/d3dUtil.h"
#include "Scenes/Scene.h"
#include "Engine/Engine.h"

namespace Zen {



	IMesh::IMesh(GameObjectPtr inOwningGO)
	{
		mGameObject = inOwningGO;
		transform = &(mGameObject->transform);
		mVertexBuffer = nullptr;
		mIndexBuffer =  nullptr;

		bShouldTick = false;
		bShouldRender = true;
		mComponentType = ComponentTypes::MESH;
	}

	IMesh::IMesh(const IMesh&)
	{

	}

	IMesh::~IMesh()
	{
		Shutdown();
	}

	bool IMesh::Initialize(ID3D11Device* device)
	{
		auto result = InitializeBuffers(device);
		return result;
	}

	void IMesh::Shutdown()
	{
		// Shutdown the vertex and index buffers.
		ShutdownBuffers();
	}

	//ZMaterial* IMesh::GetMaterialFromIndex(int inIndex)
	//{
	//	return Materials.at(inIndex).get();
	//}

	void IMesh::Render()
	{
		// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
		auto dc = ENGINE->GetDeviceContext();
		RenderBuffers(dc);

		mMaterial->Render(dc, GetIndexCount(), mGameObject->GetWorldTransformMatrix(), ENGINE->GetViewMatrix(), ENGINE->GetProjectionMatrix());
				
	}

	int IMesh::GetIndexCount()
	{
		return mIndexCount;
	}


	void IMesh::SetMeshName(const std::string& inMeshName)
	{
		//MeshName = inAiMesh->mName.C_Str();
		MeshName = inMeshName;
		//calc hash
		mMeshHash = StrUtil::GetHash(inMeshName);
	}

	void IMesh::ShutdownBuffers()
	{
		// Release the index buffer.
		ReleaseCOM(mIndexBuffer);
		ReleaseCOM(mVertexBuffer);
	}

	
}
