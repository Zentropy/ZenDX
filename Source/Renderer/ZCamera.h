#pragma once
#include <DirectXMath.h>
#include "Engine/Transform.h"

namespace Zen {

class ZCamera
{
public:
	ZCamera();
	ZCamera(const ZCamera&);
	~ZCamera();

	void Initialize();
	void SetPosition(float inX, float inY, float inZ);
	void SetRotation(float inX, float inY, float inZ);

	void SetLookAt(float inX, float inY, float inZ);

	DirectX::XMFLOAT3 GetPosition();
	DirectX::XMFLOAT3 GetRotation();

	vec3 GetForward();

	void Update();

	void Render();
	void PutViewMatrix(DirectX::XMMATRIX& inViewMatrix) { inViewMatrix = mViewMatrix;}


private:
	//float mPositionX, mPositionY, mPositionZ;
	//float mRotationX, mRotationY, mRotationZ;
	DirectX::XMMATRIX mViewMatrix;
	DirectX::XMFLOAT3 mLookAt;
	float CameraSpeed, MouseLookSpeed;
	bool mbInvertMouseVertical;
	Transform transform;
};
}
