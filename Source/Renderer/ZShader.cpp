#include "ZShader.h"
#include <string>
#include "Utilities/utils.h"
#include "d3dx11Effect.h"
#include <d3dcompiler.h>
#include "Utilities/ZLog.h"
#include "d3dUtil.h"

namespace Zen {

	ZShader::ZShader()
	{

	}

	ZShader::~ZShader()
	{
		Shutdown();
	}

	

	void ZShader::OutputShaderErrorMessage(ID3D10Blob* errorMessage, HWND hwnd, const WCHAR* shaderFilename)
	{
		char* compileErrors;
		unsigned long bufferSize, i;

		// Get a pointer to the error message text buffer.
		compileErrors = (char*)(errorMessage->GetBufferPointer());

		// Get the length of the message.
		bufferSize = static_cast<unsigned long>(errorMessage->GetBufferSize());

		

		// Write out the error message.
		for(i=0; i<bufferSize; i++)
		{
			LOG << compileErrors[i];
		}

	

		// Release the error message.
		errorMessage->Release();
		errorMessage = 0;

		// Pop a message up on the screen to notify the user to check the text file for compile errors.
		MessageBox(hwnd, L"Error compiling shader.  See Log for message.", shaderFilename, MB_OK);

		return;
	}

	void ZShader::Shutdown()
	{
		// Release the matrix constant buffer.
		ReleaseCOM(mMatrixBuffer);
		ReleaseCOM(mInputLayout);
		ReleaseCOM(mPixelShader);
		ReleaseCOM(mVertexShader);
		

		return;
	}

	

	bool ZShader::SetShaderParameters(ID3D11DeviceContext* deviceContext, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix)
	{
		HRESULT result;
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		MatrixBufferType* dataPtr;
		unsigned int bufferNumber;


		// Transpose the matrices to prepare them for the shader.
		worldMatrix = XMMatrixTranspose(worldMatrix);
		viewMatrix = XMMatrixTranspose(viewMatrix);
		projectionMatrix = XMMatrixTranspose(projectionMatrix);

		// Lock the constant buffer so it can be written to.
		result = deviceContext->Map(mMatrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		if(FAILED(result))
		{
			return false;
		}

		// Get a pointer to the data in the constant buffer.
		dataPtr = (MatrixBufferType*)mappedResource.pData;

		// Copy the matrices into the constant buffer.
		dataPtr->world = worldMatrix;
		dataPtr->view = viewMatrix;
		dataPtr->projection = projectionMatrix;

		// Unlock the constant buffer.
		deviceContext->Unmap(mMatrixBuffer, 0);

		// Set the position of the constant buffer in the vertex shader.
		bufferNumber = 0;

		// Finanly set the constant buffer in the vertex shader with the updated values.
		deviceContext->VSSetConstantBuffers(bufferNumber, 1, &mMatrixBuffer);

		return true;
	}

	void ZShader::RenderShader(ID3D11DeviceContext* deviceContext, int indexCount)
	{
		// Set the vertex input layout.
		deviceContext->IASetInputLayout(mInputLayout);

		// Set the vertex and pixel shaders that will be used to render this triangle.
		deviceContext->VSSetShader(mVertexShader, nullptr, 0);
		deviceContext->PSSetShader(mPixelShader, nullptr, 0);

		// Render the triangle.
		deviceContext->DrawIndexed(indexCount, 0, 0);

		return;
	}

}