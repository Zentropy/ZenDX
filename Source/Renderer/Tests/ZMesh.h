﻿#pragma once

#include <DirectXMath.h>
#include <memory>

#include "Renderer/Objects/IMesh.h"
#include "Renderer/ZMaterial.h"
#include "Engine/Transform.h"

struct aiMesh;


namespace Zen {

class ZMesh : public IMesh {

private:
	struct VertexType
	{
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT2 texture;
		DirectX::XMFLOAT3 normal;
		VertexType(DirectX::XMFLOAT3 inPos, DirectX::XMFLOAT2 inTex, DirectX::XMFLOAT3 inNormal) 
			: position(inPos), texture(inTex), normal(inNormal)
		{};
		VertexType() {};
	};

public:
	ZMesh(GameObjectPtr inOwningGO, const aiMesh* inAiScene);	
	virtual ~ZMesh() = default;

	void Tick() override;
	bool InitializeBuffers(ID3D11Device* device) override;		
	void RenderBuffers(ID3D11DeviceContext* deviceContext) override;
	//std::unique_ptr<struct ZTexture> texture;
	ID3D11Device* mDevice;
	//std::unique_ptr<ZMaterial> mMaterial;
	
	friend class Renderer;

};
}
