#pragma once
//#include <d3d11.h>
//#include <DirectXMath.h>
//#include "Renderer/Objects/ZMesh.h"
//
//namespace Zen {
//
//	class BoxModel : public ZMesh
//	{
//	private:
//		struct VertexType
//		{
//			DirectX::XMFLOAT3 position;
//			DirectX::XMFLOAT4 color;
//			VertexType(DirectX::XMFLOAT3 inPos, DirectX::XMFLOAT4 inColor) 
//				: position(inPos), color(inColor)
//			{};
//			VertexType() {};
//		};
//
//	public:
//		BoxModel();
//		BoxModel(const BoxModel&);
//		virtual ~BoxModel();
//
//		//Creates buffers then returns, don't need to override
//		//bool Initialize(ID3D11Device* device);
//		//void Shutdown();
//		//void Render(ID3D11DeviceContext* deviceContext);
//
//		//int GetIndexCount();
//
//	protected:
//		//void ShutdownBuffers();
//		bool InitializeBuffers(ID3D11Device* device) override;		
//		void RenderBuffers(ID3D11DeviceContext* deviceContext,  ZMaterial* material, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix) override;
//
//	private:
//		//ID3D11Buffer *mVertexBuffer, *mIndexBuffer;
//		//int mVertexCount, mIndexCount;
//	};
//
//}