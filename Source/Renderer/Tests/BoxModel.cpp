
#include "BoxModel.h"

//#include <Renderer/d3dApp.h>
//#include "Utilities/ZLog.h"
//
//namespace Zen {
//
//	using namespace DirectX;
//
//	BoxModel::BoxModel()
//	{
//		mVertexBuffer = nullptr;
//		mIndexBuffer =  nullptr;
//	}
//
//
//	BoxModel::BoxModel(const BoxModel& other)
//	{
//	}
//
//
//	BoxModel::~BoxModel()
//	{
//		//Shutdown();
//	}
//
//	/*
//	bool BoxModel::Initialize(ID3D11Device* device)
//	{
//		bool result;
//
//		// Initialize the vertex and index buffers.
//		result = InitializeBuffers(device);
//		if(!result)
//		{
//			return false;
//		}
//
//		return true;
//	}
//
//
//	void BoxModel::Shutdown()
//	{
//		// Shutdown the vertex and index buffers.
//		ShutdownBuffers();
//
//		return;
//	}
//
//
//	void BoxModel::Render(ID3D11DeviceContext* deviceContext)
//	{
//		// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
//		RenderBuffers(deviceContext);
//
//		return;
//	}
//
//
//	int BoxModel::GetIndexCount()
//	{
//		return mIndexCount;
//	}
//	*/
//
//	bool BoxModel::InitializeBuffers(ID3D11Device* device)
//	{
//		VertexType* vertices;
//		unsigned int* indices;
//		D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
//		D3D11_SUBRESOURCE_DATA vertexData, indexData;
//		HRESULT result;
//
//
//		// Set the number of vertices in the vertex array.
//		mVertexCount = 8;
//
//		
//
//
//
//		// Create the vertex array.
//		vertices = new VertexType[mVertexCount]{
//			{ XMFLOAT3(-5.0f, -5.0f, -5.0f), XMFLOAT4((const float*)&Colors::White )},
//		{ XMFLOAT3(-5.0f, +5.0f, -5.0f),     XMFLOAT4((const float*)&Colors::Black )},
//		{ XMFLOAT3(+5.0f, +5.0f, -5.0f),     XMFLOAT4((const float*)&Colors::Red )},
//		{ XMFLOAT3(+5.0f, -5.0f, -5.0f),     XMFLOAT4((const float*)&Colors::Green )},
//		{ XMFLOAT3(-5.0f, -5.0f, +5.0f),     XMFLOAT4((const float*)&Colors::Blue )},
//		{ XMFLOAT3(-5.0f, +5.0f, +5.0f),     XMFLOAT4((const float*)&Colors::Yellow )},
//		{ XMFLOAT3(+5.0f, +5.0f, +5.0f),     XMFLOAT4((const float*)&Colors::Cyan )},
//		{ XMFLOAT3(+5.0f, -5.0f, +5.0f),     XMFLOAT4((const float*)&Colors::Magenta )}
//		};
//
//		//vertices = new VertexType[mVertexCount];
//		//vertices = new VertexType[5];
//		//
//		//vertices[0].position = XMFLOAT3(-1.0f, -1.0f, -1.0f);
//		//vertices[0].color = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f );
//		//vertices[1].position  =  XMFLOAT3(-1.0f, +1.0f, -1.0f); 
//		//vertices[1].color =     XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f );
//
//
//
//		if(!vertices)
//		{
//			return false;
//		}
//
//		// Set the number of indices in the index array.
//		mIndexCount = 36;
//
//		// Create the index array.
//		indices = new unsigned int[mIndexCount]  {
//			// front face
//			0, 1, 2,
//				0, 2, 3,
//				// back face
//				4, 6, 5,
//				4, 7, 6,
//				// left face
//				4, 5, 1,
//				4, 1, 0,
//				// right face
//				3, 2, 6,
//				3, 6, 7,
//				// top face
//				1, 5, 6,
//				1, 6, 2,
//				// bottom face
//				4, 0, 3,
//				4, 3, 7
//		};
//		if(!indices)
//		{
//			return false;
//		}
//		LOG << "Size is:" << sizeof(indices) * mIndexCount;
//
//		// Load the vertex array with data.
//		//vertices[0].position = DirectX::XMFLOAT3(-1.0f, -1.0f, 0.0f);  // Bottom left.
//		//vertices[0].color = DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
//		//
//		//vertices[1].position = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);  // Top middle.
//		//vertices[1].color = DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
//		//
//		//vertices[2].position = DirectX::XMFLOAT3(1.0f, -1.0f, 0.0f);  // Bottom right.
//		//vertices[2].color = DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);
//		//
//		//// Load the index array with data.
//		//indices[0] = 0;  // Bottom left.
//		//indices[1] = 1;  // Top middle.
//		//indices[2] = 2;  // Bottom right.
//
//						 // Set up the description of the static vertex buffer.
//		vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
//		vertexBufferDesc.ByteWidth = sizeof(VertexType) * mVertexCount;
//		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//		vertexBufferDesc.CPUAccessFlags = 0;
//		vertexBufferDesc.MiscFlags = 0;
//		vertexBufferDesc.StructureByteStride = 0;
//
//		// Give the subresource structure a pointer to the vertex data.
//		vertexData.pSysMem = vertices;
//		vertexData.SysMemPitch = 0;
//		vertexData.SysMemSlicePitch = 0;
//
//		// Now create the vertex buffer.
//		result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &mVertexBuffer);
//		if(FAILED(result))
//		{
//			return false;
//		}
//
//		// Set up the description of the static index buffer.
//		indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
//		indexBufferDesc.ByteWidth = sizeof(unsigned int) * mIndexCount;
//		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
//		indexBufferDesc.CPUAccessFlags = 0;
//		indexBufferDesc.MiscFlags = 0;
//		indexBufferDesc.StructureByteStride = 0;
//
//		// Give the subresource structure a pointer to the index data.
//		indexData.pSysMem = indices;
//		indexData.SysMemPitch = 0;
//		indexData.SysMemSlicePitch = 0;
//
//		// Create the index buffer.
//		result = device->CreateBuffer(&indexBufferDesc, &indexData, &mIndexBuffer);
//		if(FAILED(result))
//		{
//			return false;
//		}
//		
//		// Release the arrays now that the vertex and index buffers have been created and loaded.
//		delete [] vertices;
//		vertices = nullptr;
//
//		delete [] indices;
//		indices = nullptr;
//
//		return true;
//	}
//
//
//	//void BoxModel::ShutdownBuffers()
//	//{
//	//	// Release the index buffer.
//	//	ReleaseCOM(mIndexBuffer);
//	//	ReleaseCOM(mVertexBuffer);
//	//
//	//}
//
//
//	void BoxModel::RenderBuffers(ID3D11DeviceContext* deviceContext,  ZMaterial* material, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix)
//	{
//		unsigned int stride;
//		unsigned int offset;
//
//
//		// Set vertex buffer stride and offset.
//		stride = sizeof(VertexType); 
//		offset = 0;
//
//		// Set the vertex buffer to active in the input assembler so it can be rendered.
//		deviceContext->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);
//
//		// Set the index buffer to active in the input assembler so it can be rendered.
//		deviceContext->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
//
//		// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
//		deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//		int last = GetLastError();
//
//		return;
//	}
//
//}
