﻿#include "ZMesh.h"
#include "Engine/Engine.h"
#include "AssetPipeline/ZModelImporter.h"
//#include <ZTexture.h>
#include <memory>
#include "Input/EngineInput.h"
#include "Engine/GameObject.h"
#include <boost/log/sources/record_ostream.hpp>

namespace Zen
{
ZMesh::ZMesh(GameObjectPtr inOwningGO, const aiMesh* inAiMesh): IMesh(inOwningGO)
{
	mDevice = ENGINE->GetDevice();
	//mMaterial = std::make_unique<ZMaterial>(L"C:/Workspace/CPP/ZenDX/Data/Models/TestBox/cube_diffuse.jpg", ShaderTypes::Diffuse, mDevice, ENGINE->GetHwnd(), L"texture.vs", L"texture.ps");


	SetMeshName(inAiMesh->mName.C_Str()); //also sets hash
	//mMaterialIndex = inAiMesh->mMaterialIndex;

	VertexType* vertices;
	std::vector<unsigned int> indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;

	mVertexCount = inAiMesh->mNumVertices;


	unsigned int numFaces = inAiMesh->mNumFaces;
	mIndexCount = numFaces * 3; //triangulated faces

	// Create the vertex array.
	vertices = new VertexType[mVertexCount];

	float scale = 1.f;

	for (int i = 0; i < mVertexCount; ++i)
	{
		vertices[i].position = DirectX::XMFLOAT3(inAiMesh->mVertices[i].x * scale, inAiMesh->mVertices[i].y * scale,
		                                         inAiMesh->mVertices[i].z * scale);
		vertices[i].texture = DirectX::XMFLOAT2(inAiMesh->mTextureCoords[0][i].x, inAiMesh->mTextureCoords[0][i].y);
		vertices[i].normal = DirectX::XMFLOAT3(inAiMesh->mNormals[0].x, inAiMesh->mNormals[0].y, inAiMesh->mNormals[0].z);
	}

	LOG << "Loaded vertices";

	indices = std::vector<unsigned int>();


	for (unsigned int i = 0; i < inAiMesh->mNumFaces; ++i)
	{
		auto& currentFace = inAiMesh->mFaces[i];
		indices.push_back(currentFace.mIndices[0]);
		indices.push_back(currentFace.mIndices[1]);
		indices.push_back(currentFace.mIndices[2]);
	}

	LOG << "Loaded indices";

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * mVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	result = mDevice->CreateBuffer(&vertexBufferDesc, &vertexData, &mVertexBuffer);
	if (FAILED(result))
	{
		LOG << "Failed creating vertex buffer";
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned int) * mIndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = &indices[0];
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = mDevice->CreateBuffer(&indexBufferDesc, &indexData, &mIndexBuffer);
	if (FAILED(result))
	{
		LOG << "Failed creating index buffer";
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete [] vertices;
	vertices = nullptr;

	indices.clear();
}

void ZMesh::Tick()
{
	if (ENGINE->Input->IsKeyDown(KEY_I))
	{
		LOG << "Key down I";
		mGameObject->transform.IncrementPositionY(0.1f);
	}
	if (ENGINE->Input->IsKeyDown(KEY_K))
	{
		LOG << "Key down K";
		mGameObject->transform.IncrementPositionY(-0.1f);
	}
	if (ENGINE->Input->IsKeyDown(KEY_L))
	{
		LOG << "Key down L";
		mGameObject->transform.RotateAroundLocalXAxisDegrees(0.1f);
	}
	if (ENGINE->Input->IsKeyDown(KEY_J))
	{
		LOG << "Key down J";
		static float uniscale = 1.0f;
		uniscale += 0.1f;
		mGameObject->transform.SetScaleUniform(uniscale);
	}
}

bool ZMesh::InitializeBuffers(ID3D11Device* device)
{
	return true;
}

void ZMesh::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;


	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &mVertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	//int last = GetLastError();

	//mMaterial->Render(deviceContext, GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix);
	//material->Render(deviceContext, GetIndexCount(), transform.WorldTransformationMatrix(), viewMatrix, projectionMatrix);

	return;
}
}
