﻿#include "ZMaterial.h"
#include "ZTexture.h"
#include "ZShader.h"
#include "Shaders/ColorShader.h"
#include "Shaders/DiffuseShader.h"
#include "Engine/Engine.h"
#include "Shaders/LitShader.h"

namespace Zen {
using namespace DirectX;
		
	ZMaterial::ZMaterial(EShaderTypes shaderType, ID3D11Device* inDevice, HWND inHwnd, const WCHAR* vsFileName, const WCHAR* psFileName)
	{
		switch (shaderType)
		{
		case EShaderTypes::COLOR:
			shader = std::make_unique<ColorShader>();
			break;
		case EShaderTypes::DIFFUSE:
			shader = std::make_unique<DiffuseShader>();
			break;
		case EShaderTypes::LIT:
			shader = std::make_unique<LitShader>();

		}
		
		shader->InitializeShader(inDevice, inHwnd, vsFileName, psFileName);
	}

	ZMaterial::~ZMaterial() {
	
	}

	void ZMaterial::SetNewTexture(const std::wstring& texturePath, ETextureTypes textureType /*= DIFFUSE*/) {

		auto dev = ENGINE->GetDevice();

		auto scene = ENGINE->GetCurrentScene();
		auto hash = StrUtil::GetHash(texturePath);
		diffuseTex = scene->TryGetTexture(hash);

		if (nullptr == diffuseTex) // texture not already in pool 
		{

			diffuseTex = std::make_shared<ZTexture>(texturePath.c_str(), dev);
			//add to pool in scene
			scene->AddTexture(hash, diffuseTex);
		}
		////TODO: kinda gross, should fix this ljater
		//std::vector<ZTexture*> texs;
		//texs.push_back(diffuseTex.get());
		shader->SetTexture(textureType, diffuseTex.get());
	}

	void ZMaterial::Render(ID3D11DeviceContext* deviceContext, int indexCount, 
		const DirectX::XMMATRIX& worldMatrix, const DirectX::XMMATRIX& viewMatrix, const DirectX::XMMATRIX& projectionMatrix)
	{
		//, ID3D11ShaderResourceView* texture
		shader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix);
	}
	
	

	ZMaterialLit::ZMaterialLit(EShaderTypes shaderType, ID3D11Device* inDevice, HWND inHwnd, const WCHAR* vsFileName, const WCHAR* psFileName)
		: ZMaterial(shaderType, inDevice, inHwnd, vsFileName, psFileName)
	{

	}

	void ZMaterialLit::SetNewTexture(const std::wstring& diffusePath, ETextureTypes textureType /*= DIFFUSE*/)
	{

	}

}
