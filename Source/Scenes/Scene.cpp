﻿#include "Scene.h"
#include <Renderer/ZTexture.h>
#include <utility>
#include "Engine/GameObject.h"
#include "Renderer/Objects/IMesh.h"
#include <AssetPipeline/ZModelImporter.h>
#include "GUI/imgui/imgui.h"
#include <sstream>
#include "Utilities/ZLog.h"
#include <queue>
#include "Engine/Editor/SceneEditor.h"
#include "Engine/Engine.h"


namespace Zen
{
	Scene::Scene()
	{
		TexturePool.clear();
		MeshPool.clear();
		RootGameObject = std::make_shared<GameObject>();
		RootGameObject->mName = "Root Node";
		editor = std::make_unique<SceneEditor>();
		SelectedGO = nullptr;
	}

	Scene::~Scene()
	{
	}

	void Scene::Tick()
	{
		//don't render imgui editor if it's not open, saves CPU
		if (ENGINE->isEditorOpen) {
			editor->Render(this);
		}	

		for (int i = 0; i < GameObjectCollection.size(); ++i)
		{
			if (GameObjectCollection[i]->bShouldTick)
				GameObjectCollection[i]->Tick();
		}
	}

	void Scene::Render()
	{
		for (int i = 0; i < GameObjectCollection.size(); ++i)
		{
			if (GameObjectCollection[i]->bShouldRender)
				GameObjectCollection[i]->Render();
		}
	
	}
	

	GameObjectPtr Scene::CreateMesh(const std::string& inMeshPath, const std::string& inObjectName)
	{
		ZModelImporter imp;
		auto ptr = imp.ImportFBX(inMeshPath);
		static int i = 1;
		if (inObjectName == "")
		{
			ptr->mName = "GameObject" + std::to_string(i);
			i++;
		} else
		{			
			ptr->mName = inObjectName;
		}

		return std::move(ptr);
	}

	Zen::GameObjectPtr Scene::FindGameObjectByName(const std::string& inName)
	{
		for (auto& go : GameObjectCollection)
		{
			if (go->mName == inName)
			{
				return go;
			}
		}
		return nullptr;
	}

	void Scene::AttachGameObjectToParent(GameObjectPtr inObject, GameObjectPtr inParent)
	{
		//#TODO: Check this to make sure these relationships don't already exist
		if (inObject->mParent)
		{
			//REMOVE the relationship here
			inObject->mParent->RemoveChild(inObject.get());
			inObject->mParent = nullptr;
		}
		inParent->AddChild(inObject);
		inObject->SetParent(inParent);
	}

	std::shared_ptr<IMesh> Scene::TryGetMesh(size_t inHash)
	{
		std::unordered_map<size_t, std::shared_ptr<IMesh>>::const_iterator iter = MeshPool.find(inHash);

		if (iter != MeshPool.end())
		{
			// iter is item pair in the map. The value will be accessible as `iter->second`.
			return iter->second;
		}
		return nullptr;
	}

	std::shared_ptr<ZTexture> Scene::TryGetTexture(size_t inHash)
	{
		std::unordered_map<size_t, std::shared_ptr<ZTexture>>::const_iterator iter = TexturePool.find(inHash);

		if (iter != TexturePool.end())
		{
			// iter is item pair in the map. The value will be accessible as `iter->second`.
			return iter->second;
		}
		return nullptr;
	}

	void Scene::AddTexture(size_t hash, std::shared_ptr<ZTexture> inTexture)
	{
		TexturePool.try_emplace(hash, inTexture);
	}

	

	void Scene::AddMesh(std::shared_ptr<IMesh> inMesh)
	{
		MeshPool.try_emplace(inMesh->GetHash(), inMesh);
	}

	void Scene::AddLight(std::shared_ptr<Light> inLight)
	{
		this->LightsInScene.push_back(inLight);
	}

	Zen::GameObjectPtr Scene::CreateGameObject(GameObjectPtr inParentObject /*= nullptr*/)
	{
		//set to root
		if (nullptr == inParentObject)
		{
			inParentObject = RootGameObject;
			
		}
		auto go = std::make_shared<GameObject>(inParentObject);
		inParentObject->AddChild(go);
		GameObjectCollection.emplace_back(go);
		//add 
		
		
		return GameObjectCollection.back();
	}
}
