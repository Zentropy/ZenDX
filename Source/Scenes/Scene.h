﻿#pragma once
#include <unordered_map>
#include <memory>
#include <d3d11.h>


namespace DirectX {
struct XMMATRIX;
}

namespace Zen {

struct ZTexture;
class IMesh;
class GameObject;
using GameObjectPtr = std::shared_ptr<GameObject>;
class SceneEditor;
class Light;
class Scene {
public:
	Scene();	
	~Scene();

	void Tick();

	//void UpdateSceneUI();

	GameObjectPtr CreateMesh(const std::string& inMeshPath, const std::string& inObjectName = "");

	//#TODO: Get rid of this eventually, so gross
	GameObjectPtr FindGameObjectByName(const std::string& inName);
	void AttachGameObjectToParent(GameObjectPtr inObject, GameObjectPtr inParent);
	
	std::shared_ptr<IMesh> TryGetMesh(size_t inHash);
	std::shared_ptr<ZTexture> TryGetTexture(size_t inHash);
	void AddTexture(size_t inHash, std::shared_ptr<ZTexture> inTexture);
	void Render();
	
	void AddMesh(std::shared_ptr<IMesh> inMesh);
	void AddLight(std::shared_ptr<Light> inLight);
	GameObjectPtr CreateGameObject(GameObjectPtr inParentObject = nullptr);

	std::unordered_map<size_t, std::shared_ptr<ZTexture>> TexturePool;
	std::unordered_map<size_t, std::shared_ptr<IMesh>> MeshPool;
	std::vector<GameObjectPtr> GameObjectCollection;
	std::vector<std::shared_ptr<Light>> LightsInScene;
	GameObjectPtr RootGameObject;
protected:
	//int RenderSceneNode(GameObject* inObject);
	std::unique_ptr<SceneEditor> editor;

	//int node_clicked = -1;

	GameObject* SelectedGO;
	friend class SceneEditor;
	
};
}
