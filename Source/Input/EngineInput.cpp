#include "EngineInput.h"

#include <algorithm>
#include "Utilities/ZLog.h"
//#include <winuser.rh>

#ifndef NOMINMAX
	#define NOMINMAX
#endif
//#include <windows.h>

namespace Zen {
	   	 

#define xLOG
	//const std::unordered_map<const char*, Keys> EngineInput::sKeyMap = []() {
	//	// keyboard mapping for windows keycodes. 
	//	// #define LOG to see each keycode when you press on output window
	//	// to be used for this->IsKeyDown("F4")
	//	std::unordered_map<const char*, Keys> m;
	//	m["F1"] = 112;	m["F2"] = 113;	m["F3"] = 114;	m["F4"] = 115;
	//	m["F5"] = 116;	m["F6"] = 117;	m["F7"] = 118;	m["F8"] = 119;
	//	m["F9"] = 120;	m["F10"] = 121;	m["F11"] = 122;	m["F12"] = 123;
	//
	//	m["0"] = 48;		m["1"] = 49;	m["2"] = 50;	m["3"] = 51;
	//	m["4"] = 52;		m["5"] = 53;	m["6"] = 54;	m["7"] = 55;
	//	m["8"] = 56;		m["9"] = 57;
	//
	//	m["C"] = 0x43;		m["c"] = /*67*/0x43;	m["N"] = 78;	m["n"] = 78;
	//	m["R"] = 82;		m["r"] = 82;	m["T"] = 'T';	m["t"] = 'T';
	//
	//	m["\\"] = 220;		m[";"] = 186;
	//	m["'"] = 222;
	//	m["Shift"] = 16;	m["shift"] = 16;
	//	m["Enter"] = 13;	m["enter"] = 13;
	//	m["Backspace"] = 8; m["backspace"] = 8;
	//	m["Escape"] = 0x1B; m["escape"] = 0x1B; m["ESC"] = 0x1B; m["esc"] = 0x1B;
	//	m["PageUp"] = 33;	m["PageDown"] = 34;
	//
	//	m["Numpad7"] = 103;		m["Numpad8"] = 104;			m["Numpad9"] = 105;
	//	m["Numpad4"] = 100;		m["Numpad5"] = 101;			m["Numpad6"] = 102;
	//	m["Numpad1"] = 97 ;		m["Numpad2"] = 98 ;			m["Numpad3"] = 99;
	//	m["Numpad+"] = VK_ADD;	m["Numpad-"] = VK_SUBTRACT;	
	//	m["+"]		 = VK_ADD;	m["-"]		 = VK_SUBTRACT;	
	//	return m;
	//}();


	EngineInput::EngineInput()
		:
		mMouseScroll(0),
		mbIgnoreInput(false)
	{
		memset(mMouseDelta, 0, 2 * sizeof(long));
		memset(mMousePos  , 0, 2 * sizeof(long));
	}

	EngineInput::EngineInput(const EngineInput &)
	{
	}


	EngineInput::~EngineInput()
	{
	}

	void EngineInput::Initialize()
	{
		memset(mKeys,	   false, sizeof(bool) * KEY_COUNT);
		memset(mPrevKeys, false, sizeof(bool) * KEY_COUNT);
	}

	//mainly to prevent mouse delta from persisting in frames where no movement occurs
	void EngineInput::PumpFrame() {
		mMouseDelta[0] = static_cast<long>(0);
		mMouseDelta[1] = static_cast<long>(0);
	}

	void EngineInput::KeyDown(Keys key)
	{
		mKeys[key] = true;

#if defined(_DEBUG) //&& defined(LOG)		
		LOG << "KeyDown: " << key;
#endif
	}


	void EngineInput::KeyUp(Keys key)
	{
#if defined(_DEBUG) && defined(LOG)
		LOG << "Key Released: " << key;
#endif
		mKeys[key] = false;
	}

	void EngineInput::KeyDown(WPARAM key)
	{
		mKeys[key] = true;

#if defined(_DEBUG) //&& defined(LOG)		
		LOG << "KeyDown: " << key;
#endif
	}


	void EngineInput::KeyUp(WPARAM key)
	{
#if defined(_DEBUG) && defined(LOG)
		LOG << "Key Released: " << key;
#endif
		mKeys[key] = false;
	}

	void EngineInput::ButtonDown(Keys btn)
	{
#if defined(_DEBUG) && defined(LOG)
		LOG << "Mouse button down: " << btn;
#endif

		mButtons[btn] = true;
	}

	void EngineInput::ButtonUp(Keys btn)
	{
#if defined(_DEBUG) && defined(LOG)
		LOG << "Mouse button up: " << btn;
#endif

		mButtons[btn] = false;
	}

	void EngineInput::UpdateMousePos(long x, long y, short scroll)
	{
#ifdef ENABLE_RAW_INPUT
		mMouseDelta[0] = x;
		mMouseDelta[1] = y;

		// unused for now
		mMousePos[0] = 0;
		mMousePos[1] = 0;
#else
		mMouseDelta[0] = std::max(-1, std::min(x - mMousePos[0], 1));
		mMouseDelta[1] = std::max(-1, std::min(y - mMousePos[1], 1));

		mMousePos[0] = x;
		mMousePos[1] = y;
#endif

#if defined(_DEBUG) && defined(LOG)
		//Log::Info("Mouse Delta: (%d, %d)\tMouse Position: (%d, %d)\tMouse Scroll: (%d)", 
		//	m_mouseDelta[0], m_mouseDelta[1],
		//	m_mousePos[0], m_mousePos[1],
		//	(int)scroll);
		//LOG << "Mouse Delta: (" << mMouseDelta[0] << ", " << mMouseDelta[1] << ")" 
		//	<< " \tMouse Position(Unused For Now): (" << mMousePos[0] << ", " << mMousePos[1] << ")"
		//	<< " \tMouse Scroll: (" << (int)scroll << ")";
#endif
		mMouseScroll = scroll;
	}

	bool EngineInput::IsScrollUp() const
	{
		return mMouseScroll > 0 && !mbIgnoreInput;
	}

	bool EngineInput::IsScrollDown() const
	{
		return mMouseScroll < 0 && !mbIgnoreInput;
	}

	bool EngineInput::IsKeyDown(Keys key) const
	{
		return mKeys[key] && !mbIgnoreInput;
	}

	

	bool EngineInput::IsMouseDown(Keys btn) const
	{
		return mButtons[btn] && !mbIgnoreInput;
	}

	bool EngineInput::IsKeyTriggered(Keys key) const
	{
		return !mPrevKeys[key] && mKeys[key] && !mbIgnoreInput;
	}



	int EngineInput::MouseDeltaX() const
	{
		return !mbIgnoreInput ? mMouseDelta[0] : 0;
	}

	int EngineInput::MouseDeltaY() const
	{
		return !mbIgnoreInput ? mMouseDelta[1] : 0;
	}

	// called at the end of the frame
	void EngineInput::PostUpdate()
	{
		memcpy(mPrevKeys, mKeys, sizeof(bool) * KEY_COUNT);
		mMouseDelta[0] = mMouseDelta[1] = 0;
		mMouseScroll = 0;
	}

	const long * EngineInput::GetDelta() const
	{
		return mMouseDelta;
	}


}