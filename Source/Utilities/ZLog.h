#pragma once

#include <memory>

#include <boost/log/sources/logger.hpp>

#include <boost/log/sources/record_ostream.hpp>

//#include <3rdParty/spdlog/include/spdlog/logger.h>

namespace Zen {

	//enum severity_level
	//{
	//	DEBUG,
	//	INFO,
	//	WARNING,
	//	ERROR,
	//	CRITICAL
	//};

	class ZLog
	{
	public:
		ZLog();
		
		boost::log::sources::logger log;
		//boost::log::sources::severity_logger_mt<severity_level> log;
		//log.

		static std::unique_ptr<ZLog> Logger;
	};
}

#define LOG BOOST_LOG(Zen::ZLog::Logger->log)