//	VQEngine | DirectX11 Renderer
//	Copyright(C) 2018  - Volkan Ilbeyli
//
//	This program is free software : you can redistribute it and / or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program.If not, see <http://www.gnu.org/licenses/>.
//
//	Contact: volkanilbeyli@gmail.com

#include "Utilities/PerfTimer.h"

#ifdef FRANK_LUNA_TIMER
#include <Windows.h>

#include <string>

PerfTimer::PerfTimer()
	:
	mSecPerCount(0),
	mDeltaTime(-1),
	mBaseTime(0),
	mPausedTime(0),
	mPrevTime(0),
	mCurrTime(0),
	mStopped(false)
{
	__int64 countsPerSec;	// use this data type for querying - otherwise stack corruption!
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
	mSecPerCount = 1.0 / (double)(countsPerSec);
}


PerfTimer::~PerfTimer()
{}


float PerfTimer::DeltaTime() const
{
	return static_cast<float>(mDeltaTime);
}

// todo: implement
float PerfTimer::GetPausedTime() const { assert(false); }
float PerfTimer::GetStopDuration() const {assert(false); }

void PerfTimer::Reset()
{
	__int64 currTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);

	mBaseTime = currTime;
	mPrevTime = currTime;
	mStopTime = 0;
	mStopped = false;
}

// accumulates paused time & starts the timer
void PerfTimer::Start()
{
	__int64 startTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&startTime);

	if (mStopped)
	{
		mPausedTime = (startTime - mStopTime);
		mPrevTime = startTime;
		mStopTime = 0;
		mStopped = false;
	}
}

// stops the timer
void PerfTimer::Stop()
{
	if (!mStopped)
	{
		__int64 currTime;
		QueryPerformanceCounter((LARGE_INTEGER*)&currTime);

		mStopTime = currTime;
		mStopped = true;
	}
}

// returns time elapsed since Reset(), not counting paused time in between
float PerfTimer::TotalTime() const
{
	long timeLength = 0;
	if (mStopped)
	{
		//					TIME
		// Base	  Stop		Start	 Stop	Curr
		//--*-------*----------*------*------|
		//			<---------->
		//			   Paused
		timeLength = (mStopTime - mPausedTime) - mBaseTime;
	}
	else
	{
		//					TIME
		// Base			Stop	  Start			Curr
		//--*------------*----------*------------|
		//				 <---------->
		//					Paused
		timeLength = (mCurrTime - mPausedTime) - mBaseTime;
	}

	return static_cast<float>(timeLength * mSecPerCount);
}

double PerfTimer::CurrentTime()
{
	__int64 currTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
	mCurrTime = currTime;
	return mCurrTime * mSecPerCount;
}

float PerfTimer::Tick()
{
	if (mStopped)
	{
		mDeltaTime = 0.0;
		return;
	}

	__int64 currTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
	mCurrTime = currTime;
	mDeltaTime = (mCurrTime - mPrevTime) * mSecPerCount;

	mPrevTime = mCurrTime;
	mDeltaTime = mDeltaTime < 0.0 ? 0.0 : mDeltaTime;		// mDeltaTime => 0
}
#else


PerfTimer::PerfTimer()
	:
	bIsStopped(true)
{
	Reset();
}

Vtime_t GetNow() { return std::chrono::system_clock::now();}

float PerfTimer::TotalTime() const
{
	duration_t totalTime = duration_t::zero();
	// Base	  Stop		Start	 Stop	   Curr
	//--*-------*----------*------*---------|
	//			<---------->
	//			   Paused
	if (bIsStopped)	totalTime = (stopTime - baseTime) - pausedTime;

	// Base			Stop	  Start			Curr
	//--*------------*----------*------------|
	//				 <---------->
	//					Paused
	else totalTime = (currTime - baseTime) - pausedTime;

	return totalTime.count();
}


float PerfTimer::DeltaTime() const
{
	return dt.count();
}

void PerfTimer::Reset()
{
	baseTime = prevTime = GetNow();
	bIsStopped = true;
	dt = duration_t::zero();
}

void PerfTimer::Start()
{
	if (bIsStopped)
	{
		pausedTime = startTime - stopTime;
		prevTime = GetNow();
		bIsStopped = false;
	}
	Tick();
}

void PerfTimer::Stop()
{
	Tick();
	if (!bIsStopped)
	{
		stopTime = GetNow();
		bIsStopped = true;
	}
}

float PerfTimer::Tick()
{
	if (bIsStopped)
	{
		dt = duration_t::zero();
		return dt.count();
	}

	currTime = GetNow();
	dt = currTime - prevTime;

	prevTime = currTime;
	dt = dt.count() < 0.0f ? dt.zero() : dt;	// make sure dt >= 0 (is this necessary?)

	return dt.count();
}

float PerfTimer::GetPausedTime() const
{
	return pausedTime.count();
}
float PerfTimer::GetStopDuration() const
{
	duration_t stopDuration = GetNow() - stopTime;
	return stopDuration.count();
}

#endif;
