#include "Utilities/ZLog.h"


//#include <3rdParty/spdlog/include/spdlog/sinks/stdout_color_sinks.h>
//#include <3rdParty/spdlog/include/spdlog/sinks/msvc_sink.h>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sinks/debug_output_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <iomanip>
#include "assimp/LogStream.hpp"
#include "assimp/DefaultLogger.hpp"
#include "assimp/Logger.hpp"
#include "../ZAssimpLogStream.h"



namespace logging = boost::log;
namespace sinks = boost::log::sinks;
namespace expr = boost::log::expressions;
typedef sinks::synchronous_sink< sinks::debug_output_backend > sink_t;

namespace Zen {
	std::unique_ptr<ZLog> ZLog::Logger = std::make_unique<ZLog>();

		
	ZLog::ZLog()
	{
		boost::shared_ptr< logging::core > core = logging::core::get();
		// Create the sink. The backend requires synchronization in the frontend.
		boost::shared_ptr< sink_t > sink(new sink_t());
		sink->set_filter(boost::log::expressions::is_debugger_present());

		sink->set_formatter
		(
			expr::stream << expr::smessage << std::endl
		);

		boost::log::register_simple_formatter_factory< boost::log::trivial::severity_level, char >("Severity");

		core->add_sink(sink);


		//assimp setup
		// Select the kinds of messages you want to receive on this log stream
		
		const unsigned int severity = Assimp::Logger::ErrorSeverity::Debugging|Assimp::Logger::ErrorSeverity::Info|Assimp::Logger::ErrorSeverity::Err|Assimp::Logger::ErrorSeverity::Warn;
		// Attaching it to the default logger
		Assimp::DefaultLogger::get()->attachStream( new ZAssimpLogStream(), severity );
	}
}
