//	VQEngine | DirectX11 Renderer
//	Copyright(C) 2018 - Volkan Ilbeyli
//
//	This program is free software : you can redistribute it and / or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.quat.wee the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program.If not, see <http://www.gnu.org/licenses/>.
//
//	Contact: volkanilbeyli@gmail.com

#include "Utilities/vectormath.h"

#include "Utilities/utils.h"

vec3 vec3::Rand()
{
	vec3 v = vec3(RandF(0, 1), RandF(0, 1), RandF(0, 1));
	return v.normalized();
}
const std::string vec3::print() const
{
	std::string s = "(";
	s += std::to_string(_v.x) + ", " + std::to_string(_v.y) + ", " + std::to_string(_v.z) + ")";
	return s;
}
const XMVECTOR vec3::Zero = XMVectorZero();
const XMVECTOR vec3::Up = XMVectorSet(+0.0f, +1.0f, +0.0f, +0.0f);
const XMVECTOR vec3::Down = XMVectorSet(+0.0f, -1.0f, +0.0f, +0.0f);
const XMVECTOR vec3::Left = XMVectorSet(-1.0f, +0.0f, +0.0f, +0.0f);
const XMVECTOR vec3::Right = XMVectorSet(+1.0f, +0.0f, +0.0f, +0.0f);
const XMVECTOR vec3::Forward = XMVectorSet(+0.0f, +0.0f, +1.0f, +0.0f);
const XMVECTOR vec3::Back = XMVectorSet(+0.0f, +0.0f, -1.0f, +0.0f);

const vec3 vec3::ZeroF3 = vec3();
const vec3 vec3::UpF3 = vec3(+0.0f, +1.0f, +0.0f);
const vec3 vec3::DownF3 = vec3(+0.0f, -1.0f, +0.0f);
const vec3 vec3::LeftF3 = vec3(-1.0f, +0.0f, +0.0f);
const vec3 vec3::RightF3 = vec3(+1.0f, +0.0f, +0.0f);
const vec3 vec3::ForwardF3 = vec3(+0.0f, +0.0f, +1.0f);
const vec3 vec3::BackF3 = vec3(+0.0f, +0.0f, -1.0f);

const vec3 vec3::XAxis = vec3(1.0f, 0.0f, 0.0f);
const vec3 vec3::YAxis = vec3(0.0f, 1.0f, 0.0f);
const vec3 vec3::ZAxis = vec3(0.0f, 0.0f, 1.0f);

const XMVECTOR vec2::Zero = XMVectorZero();
const XMVECTOR vec2::Up = XMVectorSet(+0.0f, +1.0f, +0.0f, +0.0f);
const XMVECTOR vec2::Down = XMVectorSet(+0.0f, -1.0f, +0.0f, +0.0f);
const XMVECTOR vec2::Left = XMVectorSet(-1.0f, +0.0f, +0.0f, +0.0f);
const XMVECTOR vec2::Right = XMVectorSet(+1.0f, +0.0f, +0.0f, +0.0f);

const vec2 vec2::ZeroF2 = vec2();
const vec2 vec2::UpF2 = vec2(+0.0f, +1.0f);
const vec2 vec2::DownF2 = vec2(+0.0f, -1.0f);
const vec2 vec2::LeftF2 = vec2(-1.0f, +0.0f);
const vec2 vec2::RightF2 = vec2(+1.0f, +0.0f);


vec3::vec3() : _v(XMFLOAT3(0.0f, 0.0f, 0.0f)) {}
vec3::vec3(const vec3& v_in) : _v(v_in._v) {}
vec3::vec3(const XMFLOAT3& f3) : _v(f3) {}
vec3::vec3(const XMVECTOR& v_in) { XMStoreFloat3(&_v, v_in); }
vec3::vec3(float x, float y, float z) : _v(x, y, z) {}
vec3::vec3(float x) : _v(x, x, x) {}

vec3::vec3(const Quaternion& inQuat)
	: _v(inQuat.x, inQuat.y, inQuat.z)
{}

vec2::vec2() : _v(XMFLOAT2(0.0f, 0.0f)) {}
vec2::vec2(const vec3& v3) : _v(v3.x(), v3.y()) {}
vec2::vec2(const vec2& v_in) : _v(v_in._v) {}
vec2::vec2(float x, float y) : _v(x, y) {}
vec2::vec2(int x, int y) : _v(static_cast<float>(x), static_cast<float>(y)) {}
vec2::vec2(float f) : _v(f, f) {}
vec2::vec2(const XMFLOAT2& f2) : _v(f2) {}
vec2::vec2(const XMFLOAT3& f3) : _v(f3.x, f3.y) {}
vec2::vec2(const XMVECTOR& v_in) { XMStoreFloat2(&_v, v_in); }

vec3::operator XMVECTOR() const { return XMLoadFloat3(&_v); }
vec3::operator XMFLOAT3() const { return _v; }
bool vec3::operator==(const vec3 &v) const { return v._v.x == _v.x && v._v.y == _v.y && v._v.z == _v.z; }
vec2::operator XMVECTOR() const { return XMLoadFloat2(&_v); }
bool vec2::operator==(const vec2 &v) const { return v._v.x == _v.x && v._v.y == _v.y; }
vec2::operator XMFLOAT2() const { return _v; }

float& vec3::x() { return _v.x; }
float& vec3::y() { return _v.y; }
float& vec3::z() { return _v.z; }
float& vec3::x() const { return const_cast<float&>(_v.x); }
float& vec3::y() const { return const_cast<float&>(_v.y); }
float& vec3::z() const { return const_cast<float&>(_v.z); }

float& vec2::x() { return _v.x; }
float& vec2::y() { return _v.y; }
float& vec2::x() const { return const_cast<float&>(_v.x); }
float& vec2::y() const { return const_cast<float&>(_v.y); }

void vec3::normalize()
{
	XMVECTOR v = XMLoadFloat3(&_v);
	_v = vec3(XMVector3Normalize(v));
}

const vec3 vec3::normalized() const
{
	XMVECTOR v = XMLoadFloat3(&_v);
	return vec3(XMVector3Normalize(v));
}

void vec2::normalize()
{
	XMVECTOR v = XMLoadFloat2(&_v);
	_v = vec2(XMVector2Normalize(v));
}

const vec2 vec2::normalized() const
{
	XMVECTOR v = XMLoadFloat2(&_v);
	return vec2(XMVector2Normalize(v));
}




//-------------

#include <cmath>
#include <algorithm>	// min, max

ZQuaternion::ZQuaternion(float s, const vec3& v)
{
	quat.w = s;
	quat.x = v.x();
	quat.y =  v.y();
	quat.z =  v.z();
}

ZQuaternion::ZQuaternion() 
{
	quat = Quaternion::Identity;
}

// Pitch:	X
// Yaw:		Y
// Roll:	Z
//Quaternion::Quaternion(float pitch, float yaw, float roll)
//{
//	// source: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
//	float t0 = std::cosf(yaw * 0.5f);
//	float t1 = std::sinf(yaw * 0.5f);
//	float t2 = std::cosf(roll * 0.5f);
//	float t3 = std::sinf(roll * 0.5f);
//	float t4 = std::cosf(pitch * 0.5f);
//	float t5 = std::sinf(pitch * 0.5f);
//
//	float w = t0 * t2 * t4 + t1 * t3 * t5;
//	float x = t0 * t3 * t4 - t1 * t2 * t5;
//	float y = t0 * t2 * t5 + t1 * t3 * t4;
//	float z = t1 * t2 * t4 - t0 * t3 * t5;
//
//	S = w;
//	//V = vec3(y, x, z);
//	V = vec3(y, z, x);	// i need an explanation why this works and not others:( 
//							// read: euler vs yawpitchroll
//	//V = vec3(z, x, y);
//}

//Quaternion::Quaternion(const vec3& pitchYawRoll)
//	:
//	Quaternion(pitchYawRoll.x(), pitchYawRoll.y(), pitchYawRoll.z())
//{}

ZQuaternion::ZQuaternion(const XMMATRIX& rotMatrix)
{
	//const XMMATRIX & m = rotMatrix;
	//S = 0.5f * sqrt(m.r[0].m128_f32[0] + m.r[1].m128_f32[1] + m.r[2].m128_f32[2] + 1);
	//V.x = (m.r[2].m128_f32[1] - m.r[1].m128_f32[2]) / (4.0f * S);
	//V.y = (m.r[0].m128_f32[2] - m.r[2].m128_f32[0]) / (4.0f * S);
	//V.z = (m.r[1].m128_f32[0] - m.r[0].m128_f32[1]) / (4.0f * S);

	// Therefore, I decided to use decompose function to get the XM quaternion, which doesn't work
	// with my system as well. I have to rotate it 180degrees on Y to get the correct rotation
	XMVECTOR scl = XMVectorZero();
	XMVECTOR quat = XMVectorZero();
	XMVECTOR trans = XMVectorZero();
	XMMatrixDecompose(&scl, &quat, &trans, XMMatrixTranspose(rotMatrix));
	//XMMatrixDecompose(&scl, &quat, &trans, rotMatrix);

	// hack zone
	//quat.m128_f32[2] *= -1.0f;

	//*this = Quaternion(quat.m128_f32[3], quat); //*Quaternion(0.0f, XM_PI, 0.0f);
	*this = ZQuaternion(quat.m128_f32[3], quat).Conjugate(); //*Quaternion(0.0f, XM_PI, 0.0f);
	int a = 5;
}

void ZQuaternion::SetQuatFromVec3AndScalar(Quaternion& inQuat, const XMVECTOR &  inVec3, float scalar)
{
	inQuat.w = scalar;
	inQuat.x =  inVec3.m128_f32[0];
	inQuat.y =  inVec3.m128_f32[1];
	inQuat.z =  inVec3.m128_f32[2];
}

ZQuaternion::ZQuaternion(float s, const XMVECTOR & v)
	
{
	quat.w = s;
	quat.x = v.m128_f32[0];
	quat.y =  v.m128_f32[1];
	quat.z =  v.m128_f32[2];
}

ZQuaternion ZQuaternion::Identity()
{
	return ZQuaternion(1, vec3(0.0f, 0.0f, 0.0f));
}


ZQuaternion ZQuaternion::FromAxisAngle(const XMVECTOR& axis, const float angle)
{
	//const float half_angle = angle / 2;
	ZQuaternion Q;
	//Q.quat.w = cosf(half_angle);
	//Q.V = axis * sinf(half_angle);
	//Q.quat.w = cosf(half_angle);
	Q.quat = Quaternion::CreateFromAxisAngle(axis, angle);
	return Q;
}



ZQuaternion ZQuaternion::Lerp(const ZQuaternion & from, const ZQuaternion & to, float t)
{
	return  from * (1.0f - t) + to * t;
}

ZQuaternion ZQuaternion::Slerp(const ZQuaternion & from, const ZQuaternion & to, float t)
{
	double alpha = std::acos(from.Dot(to));
	if (alpha < 0.00001) return from;
	double sina = std::sin(alpha);
	double sinta = std::sin(t * alpha);
	ZQuaternion interpolated = from * static_cast<float>(std::sin(alpha - t * alpha) / sina) +
		to * static_cast<float>(sinta / sina);
	interpolated.Normalize();
	return interpolated;
}

ZQuaternion ZQuaternion::FromEulerDeg(float x, float y, float z)
{
	ZQuaternion q;
	q.quat = Quaternion::CreateFromYawPitchRoll(y , x, z);
	// Abbreviations for the various angular functions
	//double cy = cos(y * 0.5);
	//double sy = sin(y * 0.5);
	//double cr = cos(z * 0.5);
	//double sr = sin(z * 0.5);
	//double cp = cos(x * 0.5);
	//double sp = sin(x * 0.5);
	//
	//q.quat.w = cy * cr * cp + sy * sr * sp;
	//q.quat.x = cy * sr * cp - sy * cr * sp;
	//q.quat.y = cy * cr * sp + sy * sr * cp;
	//q.quat.z = sy * cr * cp - cy * sr * sp;
	return q;
}

vec3 ZQuaternion::ToEulerRad(const ZQuaternion& Q)
{
	// source: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
	double ysqr = Q.quat.y * Q.quat.y;
	double t0 = -2.0f * (ysqr + Q.quat.z * Q.quat.z) + 1.0f;
	double t1 = +2.0f * (Q.quat.x * Q.quat.y - Q.quat.w * Q.quat.z);
	double t2 = -2.0f * (Q.quat.x * Q.quat.z + Q.quat.w * Q.quat.y);
	double t3 = +2.0f * (Q.quat.y * Q.quat.z - Q.quat.w * Q.quat.x);
	double t4 = -2.0f * (Q.quat.x * Q.quat.x + ysqr) + 1.0f;

	t2 = t2 > 1.0f ? 1.0f : t2;
	t2 = t2 < -1.0f ? -1.0f : t2;

	float pitch = static_cast<float>(std::asin(t2));
	float roll = static_cast<float>(std::atan2(t3, t4));
	float yaw = static_cast<float>(std::atan2(t1, t0));
	//negate for LH coord sys
	return vec3(-roll, -pitch, -yaw);	// ??? (probably due to wiki convention)
}

vec3 ZQuaternion::ToEulerDeg(const ZQuaternion& Q)
{
	vec3 eul = ZQuaternion::ToEulerRad(Q);
	eul.x() *= RAD2DEG;
	eul.y() *= RAD2DEG;
	eul.z() *= RAD2DEG;
	return eul;
}


ZQuaternion ZQuaternion::operator+(const ZQuaternion & q) const
{
	ZQuaternion result;
	result.quat = quat + q.quat;
	//XMVECTOR V1 = XMVectorSet(quat.x, quat.y, quat.z, 0);
	//XMVECTOR V2 = XMVectorSet(q.quat.x, q.quat.y, q.quat.z, 0);
	//
	//SetQuatFromVec3AndScalar(result.quat, V1 + V2, quat.w + q.quat.w );

	//result.quat.w = quat.w + q.quat.w;
	//auto res = V1 + V2;
	//result.quat.x = res.;

	return result;
}

ZQuaternion ZQuaternion::operator*(const ZQuaternion & q) const
{
	ZQuaternion result;
	result.quat = quat * q.quat;
	//XMVECTOR V1 = XMVectorSet(quat.x, quat.y, quat.z, 0);
	//XMVECTOR V2 = XMVectorSet(q.quat.x, q.quat.y, q.quat.z, 0);
	//
	//SetQuatFromVec3AndScalar(result.quat, V1 + V2, quat.w + q.quat.w );

	//result.quat.w = quat.w + q.quat.w;
	//auto res = V1 + V2;
	//result.quat.x = res.;

	return result;
	//ZQuaternion result;
	//XMVECTOR V1 = XMVectorSet(quat.x, quat.y, quat.z, 0);
	//XMVECTOR V2 = XMVectorSet(q.quat.x, q.quat.y, q.quat.z, 0);
	//
	//// s1s2 - v1.v2 
	//result.quat.w = quat.w * q.quat.w - XMVector3Dot(V1, V2).m128_f32[0];
	//// s1v2 + s2v1 + v1xv2
	//auto v = this->quat.w * V2 + q.quat.w * V1 + XMVector3Cross(V1, V2);
	//
	//result.V = 
	//return result;
}

ZQuaternion ZQuaternion::operator*(float c) const
{
	ZQuaternion result;
	result.quat.w = c * quat.w;
	
	result.quat.x = quat.x*c;
	result.quat.y = quat.y*c;
	result.quat.z = quat.z*c;
	return result;
}


bool ZQuaternion::operator==(const ZQuaternion& q) const
{
	double epsilons[4] = { 99999.0, 99999.0, 99999.0, 99999.0 };
	epsilons[0] = static_cast<double>(q.quat.x) - static_cast<double>(this->quat.x);
	epsilons[1] = static_cast<double>(q.quat.y) - static_cast<double>(this->quat.y);
	epsilons[2] = static_cast<double>(q.quat.z) - static_cast<double>(this->quat.z);
	epsilons[3] = static_cast<double>(q.quat.w) - static_cast<double>(this->quat.w);
	bool same_x = std::abs(epsilons[0]) < 0.000001;
	bool same_y = std::abs(epsilons[1]) < 0.000001;
	bool same_z = std::abs(epsilons[2]) < 0.000001;
	bool same_w = std::abs(epsilons[3]) < 0.000001;
	return same_x && same_y && same_z && same_w;
}

// other operations
float ZQuaternion::Dot(const ZQuaternion & q) const
{
	return quat.Dot(q.quat);

	//XMVECTOR V1 = XMVectorSet(quat.x, quat.y, quat.z, 0);
	//XMVECTOR V2 = XMVectorSet(q.quat.x, q.quat.y, q.quat.z, 0);
	//return std::max(-1.0f, std::min(quat.w*q.quat.w + XMVector3Dot(V1, V2).m128_f32[0], 1.0f));
}

float ZQuaternion::Len() const
{
	return sqrt(quat.w*quat.w + quat.x*quat.x + quat.y*quat.y + quat.z*quat.z);
}

ZQuaternion ZQuaternion::Inverse() const
{
	ZQuaternion result;
	float f = 1.0f / (quat.w*quat.w + quat.x*quat.x + quat.y*quat.y + quat.z*quat.z);
	result.quat.w = f * quat.w;
	
	result.quat.x = -quat.x*f;
	result.quat.y = -quat.y*f;
	result.quat.z = -quat.z*f;
	return result;
}

ZQuaternion ZQuaternion::Conjugate() const
{
	ZQuaternion result;
	result.quat.w = quat.w;
	
	result.quat.x = -quat.x;
	result.quat.y = -quat.y;
	result.quat.z = -quat.z;
	return result;
}

XMMATRIX ZQuaternion::Matrix() const
{
	XMMATRIX m = XMMatrixIdentity();
	float y2 = quat.y * quat.y;
	float z2 = quat.z * quat.z;
	float x2 = quat.x * quat.x;
	float xy = quat.x * quat.y;
	float sz = quat.w * quat.z;
	float xz = quat.x * quat.z;
	float sy = quat.w * quat.y;
	float yz = quat.y * quat.z;
	float sx = quat.w * quat.x;

	// -Z X -Y
	// LHS
	XMVECTOR r0 = XMVectorSet(1.0f - 2.0f * (y2 + z2), 2 * (xy - sz), 2 * (xz + sy), 0);
	XMVECTOR r1 = XMVectorSet(2 * (xy + sz), 1.0f - 2.0f * (x2 + z2), 2 * (yz - sx), 0);
	XMVECTOR r2 = XMVectorSet(2 * (xz - sy), 2 * (yz + sx), 1.0f - 2.0f * (x2 + y2), 0);
	XMVECTOR r3 = XMVectorSet(0, 0, 0, 1);

	//XMVECTOR r0 = XMVectorSet(2 * (xy - sz), -2.0f * (xz + sy), -(1.0f - 2.0f * (y2 + z2)), 0);
	//XMVECTOR r1 = XMVectorSet(-(1.0f - 2.0f * (x2 + z2)), 2.0f * (yz - sx), -2.0f * (xy + sz), 0);
	//XMVECTOR r2 = XMVectorSet(-2 * (yz + sx), -(1.0f - 2.0f * (x2 + y2)), 2.0f * (xz - sy), 0);
	//XMVECTOR r3 = XMVectorSet(0, 0, 0, 1);

	// RHS
	//XMVECTOR r0 = XMVectorSet(1.0f - 2.0f * (y2 + z2), 2 * (xy + sz)		  , 2 * (xz - sy)		   , 0);
	//XMVECTOR r1 = XMVectorSet(2 * (xy - sz)			 , 1.0f - 2.0f * (x2 + z2), 2 * (yz + sx)		   , 0);
	//XMVECTOR r2 = XMVectorSet(2 * (xz + sy)			 , 2 * (yz - sx)		  , 1.0f - 2.0f * (x2 + y2), 0);
	//XMVECTOR r3 = XMVectorSet(0						 , 0					  , 0					   , 1);

	m.r[0] = r0;
	m.r[1] = r1;
	m.r[2] = r2;
	m.r[3] = r3;
	return XMMatrixTranspose(m);
}

ZQuaternion& ZQuaternion::Normalize()
{
	float len = Len();
	if (len > 0.00001)
	{
		quat.w = quat.w / len;
		quat.x = quat.x / len;
		quat.y = quat.y / len;
		quat.z = quat.z / len;
	}
	return *this;
}

vec3 ZQuaternion::TransformVector(const vec3 & v) const
{
	ZQuaternion pure(0.0f, v);
	ZQuaternion rotated = *this * pure * this->Conjugate();
	return vec3(rotated.quat);
}
