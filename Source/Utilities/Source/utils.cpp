//	VQEngine | DirectX11 Renderer
//	Copyright(C) 2018  - Volkan Ilbeyli
//
//	This program is free software : you can redistribute it and / or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program.If not, see <http://www.gnu.org/licenses/>.
//
//	Contact: volkanilbeyli@gmail.com

#include "Utilities/utils.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <random>

#include <ctime>
#include <unordered_map>

#include "shlobj.h"		// SHGetKnownFolderPath()

#ifdef _DEBUG
#include <cassert>
#endif _DEBUG

#include <winnt.h>
#include "gtest/gtest.h"


// 2011**L::C++11 | 201402L::C++14 | 201703L::C++17
#if _MSVC_LANG >= 201703L	// CPP17
#include <experimental/filesystem>

namespace filesys = std::experimental::filesystem;
#else
#include <fstream>
#endif

#include <algorithm>
#include <shtypes.h>
#include <winerror.h>
using std::string;

namespace StrUtil
{
	using std::vector;
	using std::wstring;
	using std::cout;
	using std::endl;

	std::wstring StringToWide(const std::string& inString)
	{
		return std::wstring(inString.begin(), inString.end());
	}

	std::string WideToString(const std::wstring& inString)
	{
		return std::string(inString.begin(), inString.end());
	}

	vector<wstring> _splitw(const char* s, char c)
	{
		vector<wstring> result;
		do
		{
			const char* begin = s;

			if (*begin == c || *begin == '\0') 
				continue;	// skip delimiter character

			while (*s != c && *s)	
				s++;	// iterate until delimiter is found

			result.push_back(wstring(begin, s));

		} while (*s++);
		return result;
	}

	vector<wstring> _splitw(const wchar_t* s, char c)
	{
		vector<wstring> result;
		do
		{
			const wchar_t* begin = s;

			if (*begin == c || *begin == '\0') 
				continue;	// skip delimiter character

			while (*s != c && *s)	
				s++;	// iterate until delimiter is found

			result.push_back(wstring(begin, s));

		} while (*s++);
		return result;
	}

	vector<wstring> _splitw(const wstring& str, char c)
	{
		return _splitw(str.c_str(), c);
	}

	std::vector<std::wstring> _splitw(const std::wstring & s, const std::vector<char>& delimiters)
	{
		vector<wstring> result;
		const wchar_t* ps = s.c_str();
		auto& IsDelimiter = [&delimiters](const wchar_t c)
		{
			return std::find(delimiters.begin(), delimiters.end(), c) != delimiters.end();
		};

		do
		{
			const wchar_t* begin = ps;

			if (IsDelimiter(*begin) || (*begin == '\0')) 
				continue;	// skip delimiter characters

			while (!IsDelimiter(*ps) && *ps) 
				ps++;	// iterate until delimiter is found or wstring has ended

			result.push_back(wstring(begin, ps));

		} while (*ps++);
		return result;
	}

	//NON WIDE STRINGS
vector<std::string> _split(const char* s, char c)
	{
		vector<string> result;
		do
		{
			const char* begin = s;

			if (*begin == c || *begin == '\0') 
				continue;	// skip delimiter character

			while (*s != c && *s)	
				s++;	// iterate until delimiter is found

			result.push_back(string(begin, s));

		} while (*s++);
		return result;
	}

	vector<string> _split(const wchar_t* s, char c)
	{
		vector<string> result;
		do
		{
			const wchar_t* begin = s;

			if (*begin == c || *begin == '\0') 
				continue;	// skip delimiter character

			while (*s != c && *s)	
				s++;	// iterate until delimiter is found

			result.push_back(string(begin, s));

		} while (*s++);
		return result;
	}

	vector<string> _split(const string& str, char c)
	{
		return _split(str.c_str(), c);
	}

	std::vector<std::string> _split(const std::string & s, const std::vector<char>& delimiters)
	{
		vector<string> result;
		const char* ps = s.c_str();
		auto& IsDelimiter = [&delimiters](const char c)
		{
			return std::find(delimiters.begin(), delimiters.end(), c) != delimiters.end();
		};

		do
		{
			const char* begin = ps;

			if (IsDelimiter(*begin) || (*begin == '\0')) 
				continue;	// skip delimiter characters

			while (!IsDelimiter(*ps) && *ps) 
				ps++;	// iterate until delimiter is found or wstring has ended

			result.push_back(string(begin, ps));

		} while (*ps++);
		return result;
	}

	std::wstring CommaSeparatedNumber(const std::wstring& num)
	{
		std::wstring _num = L"";
		int i = 0;
		for (auto it = num.rbegin(); it != num.rend(); ++it)
		{
			if (i % 3 == 0 && i != 0)
			{
				_num += L",";
			}
			_num += *it;
			++i;
		}
		return std::wstring(_num.rbegin(), _num.rend());
	}

	UnicodeString::UnicodeString(const std::string& strIn)
		: str(strIn)
	{
		auto stdWstr = std::wstring(str.begin(), str.end());
		wstr = stdWstr.c_str();
	}
	
	UnicodeString::UnicodeString(PWSTR strIn)
		: wstr(strIn)
	{
		wstr = std::wstring(wstr.begin(), wstr.end());
	}

	size_t GetHash(const std::string& inString)
	{
		//static std::hash<std::
		return std::hash<std::string>{}(inString);
	}

	size_t GetHash(const std::wstring& inString)
	{
		//static std::hash<std::
		return std::hash<std::wstring>{}(inString);
	}

}

//---------------------------------------------------------------------------------------------

namespace DirectoryUtil
{
	std::wstring GetSpecialFolderPath(ESpecialFolder folder)
	{
		PWSTR retPath = {};
		REFKNOWNFOLDERID folder_id = [&]()
		{
			switch (folder)
			{
			case PROGRAM_FILES:	return FOLDERID_ProgramFiles;
			case APPDATA:		return FOLDERID_RoamingAppData;
			case LOCALAPPDATA:	return FOLDERID_LocalAppData;
			case USERPROFILE:	return FOLDERID_UserProfiles;
			}
			return FOLDERID_RoamingAppData;
		}();

		HRESULT hr = SHGetKnownFolderPath(folder_id, 0, nullptr, &retPath);
		if (hr != S_OK)
		{
			// Log::Error("SHGetKnownFolderPath() returned %s.", hr == E_FAIL ? "E_FAIL" : "E_INVALIDARG");
			return L"";
		}

		return StrUtil::UnicodeString::ToASCII(retPath);
	}

	//#TODO: Cache these statically later
	std::wstring GetExeDirectoryW()
	{
		wchar_t result[ MAX_PATH];
		std::wstring strres = std::wstring(result, GetModuleFileName(nullptr, result, MAX_PATH));
		return GetFolderPath(strres);		
	}
	std::string GetExeDirectory()
	{
		wchar_t result[ MAX_PATH];
		std::wstring strres = std::wstring(result, GetModuleFileName(nullptr, result, MAX_PATH));
		return GetFolderPath(StrUtil::WideToString(strres));		
	}

	std::wstring GetDataDirectoryW()
	{
		wchar_t result[ MAX_PATH];
		std::wstring strres = std::wstring(result, GetModuleFileName(nullptr, result, MAX_PATH));
		std::wstringstream str;
		str << GetFolderPath(strres) << L"Data/";
		return str.str();	
	}

	std::string GetDataDirectory()
	{
		
		return StrUtil::WideToString(GetDataDirectoryW());
	}

	std::wstring GetModelDirectoryW()
	{
		return GetExeDirectoryW() + L"Data/Models/";
	}

	std::string GetModelDirectory()
	{
		return GetExeDirectory() + "Data/Models/";
	}

	std::string GetFolderPath(const std::string& pathToFile)
	{
		const auto tokens = StrUtil::split(pathToFile, '\\', '/');
		std::string path = "";
		std::for_each(RANGE(tokens), [&](const std::string& s) { if(s != tokens.back()) path += s + "/"; });
		return path;
	}


	std::wstring GetFolderPath(const std::wstring & pathToFile)
	{
		const auto tokens = StrUtil::split(pathToFile, '\\', '/');
		std::wstring path = L"";
		std::for_each(RANGE(tokens), [&](const std::wstring& s) { if(s != tokens.back()) path += s + L"/"; });
		return path;
	}

	bool IsImageName(const std::wstring & str)
	{
		std::vector<std::wstring> FileNameAndExtension = StrUtil::split(str, '.');
		if (FileNameAndExtension.size() < 2)
			return false;

		const std::wstring& extension = FileNameAndExtension[1];

		bool bIsImageFile = false;
		bIsImageFile = bIsImageFile || extension == L"png";
		bIsImageFile = bIsImageFile || extension == L"jpg";
		bIsImageFile = bIsImageFile || extension == L"hdr";
		return bIsImageFile;
	}

	std::wstring GetFileNameWithoutExtension(const std::wstring& path)
	{	// example: path: "Archetypes/player.txt" | return val: "player"
		std::wstring no_extension = StrUtil::split(path.c_str(), '.')[0];
		return StrUtil::split(no_extension.c_str(), '/').back();
	}


	bool FileExists(const std::wstring & pathToFile)
	{	// src: https://msdn.microsoft.com/en-us/library/b0084kay.aspx
#if _MSVC_LANG >= 201703L	// CPP17
		return filesys::exists(pathToFile);
#else
		std::ifstream infile(pathToFile);
		return infile.good();
#endif
	}

bool IsFileNewer(const std::wstring & file0, const std::wstring & file1)
	{
#if _MSVC_LANG >= 201703L // CPP17 
		return filesys::last_write_time(file0) > filesys::last_write_time(file1);
#else
		FILETIME ftCreate[2], ftAccess[2], ftWrite[2];
		HANDLE hFile0 = CreateFile(file0.data(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
		HANDLE hFile1 = CreateFile(file1.data(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
		GetFileTime(hFile0, &ftCreate[0], &ftAccess[0], &ftWrite[0]);
		GetFileTime(hFile1, &ftCreate[1], &ftAccess[1], &ftWrite[1]);
		return CompareFileTime(&ftWrite[0], &ftWrite[1]) == 1;
#endif
	}
}

//---------------------------------------------------------------------------------------------

// GLOBAL NAMESPACE
//
std::wstring GetCurrentTimeAsString()
{
	const std::time_t now = std::time(0);
	std::tm tmNow;	// current time
	localtime_s(&tmNow, &now);

	// YYYY-MM-DD_HH-MM-SS
	std::wstringstream ss;
	ss << (tmNow.tm_year + 1900) << L"_" 
		<< std::setfill(L'0') << std::setw(2) << tmNow.tm_mon + 1 << L"_" 
		<< std::setfill(L'0') << std::setw(2) << tmNow.tm_mday << L"-"
		<< std::setfill(L'0') << std::setw(2) << tmNow.tm_hour << L"_" 
		<< std::setfill(L'0') << std::setw(2) << tmNow.tm_min << L"_" 
		<< std::setfill(L'0') << std::setw(2) << tmNow.tm_sec;
	return ss.str();
}
std::wstring GetCurrentTimeAsStringWithBrackets(){ return L"[" + GetCurrentTimeAsString() + L"]"; }

float RandF(float l, float h)
{
	if (l > h)
	{	// swap params in case order is wrong
		float tmp = l;
		l = h;
		h = tmp;
	}
	thread_local std::mt19937_64 generator(std::random_device{}());
	std::uniform_real_distribution<float> distribution(l, h);
	return distribution(generator);
}

// [)
int RandI(int l, int h) 
{
	int offset = rand() % (h - l);
	return l + offset;
}
size_t RandU(size_t l, size_t h)
{
#ifdef _DEBUG
	assert(l <= h);
#endif
	int offset = rand() % (h - l);
	return l + static_cast<size_t>(offset);
}

bool Zen::IsAlmost(float first, float second)
{


	const testing::internal::FloatingPoint<float> lhs(first), rhs(second);

	return lhs.AlmostEquals(rhs);

	//static float epsilon = std::numeric_limits<float>::epsilon();
	//bool res;
	//if (std::abs)
}
