//	VQEngine | DirectX11 Renderer
//	Copyright(C) 2018  - Volkan Ilbeyli
//
//	This program is free software : you can redistribute it and / or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program.If not, see <http://www.gnu.org/licenses/>.
//
//	Contact: volkanilbeyli@gmail.com

#pragma once

#include <string>
#include <vector>
//#include <../packages/googletest.1.8.2/build/native/include/gtest/internal/gtest-internal.h>
//#include "../../packages/Microsoft.googletest.v140.windesktop.msvcstl.static.rt-dyn.1.8.0/build/native/include/gtest/internal/gtest-internal.h"



#define RANGE(c) std::begin(c), std::end(c)
#define RRANGE(c) std::rbegin(c), std::rend(c)

namespace Zen
{
bool IsAlmost(float first, float second);
}



/// STRING PROCESSING
//===============================================================================================
namespace StrUtil
{
// typedefs
typedef wchar_t WCHAR; // wc,   16-bit UNICODE character
typedef wchar_t* PWSTR;

std::wstring StringToWide(const std::string& inString);
std::string WideToString(const std::wstring& inString);

std::vector<std::wstring> _splitw(const char* s, char c = ' ');
std::vector<std::wstring> _splitw(const std::wstring& s, char c = ' ');
std::vector<std::wstring> _splitw(const std::wstring& s, const std::vector<char>& delimiters);

std::vector<std::string> _split(const char* s, char c = ' ');
std::vector<std::string> _split(const std::string& s, char c = ' ');
std::vector<std::string> _split(const std::string& s, const std::vector<char>& delimiters);

template <class... Args>
std::vector<std::wstring> split(const std::wstring& s, Args&&... args)
{
	const std::vector<char> delimiters = {args...};
	return _splitw(s, delimiters);
}

template <class... Args>
std::vector<std::string> split(const std::string& s, Args&&... args)
{
	const std::vector<char> delimiters = {args...};
	return _split(s, delimiters);
}

std::wstring CommaSeparatedNumber(const std::wstring& num);

size_t GetHash(const std::string& inString);
size_t GetHash(const std::wstring& inString);

inline void ReplaceStringInPlace(std::string& subject, const std::string& search,
                                 const std::string& replace)
{
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != std::string::npos)
	{
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
}

struct UnicodeString
{
public:
	static std::wstring ToASCII(PWSTR pwstr)
	{
		std::wstring wstr(pwstr);
		return std::wstring(wstr.begin(), wstr.end());
	}

	static std::wstring ASCIIToUnicode(const std::wstring& str) { return std::wstring(str.begin(), str.end()); }

private:
	std::string str;
	std::wstring wstr;

public:
	UnicodeString(const std::string& strIn);
	UnicodeString(PWSTR strIn);

	operator std::wstring() const { return wstr; }
	operator const char*() const { return str.c_str(); }

	const WCHAR* UnicodeString::GetUnicodePtr() const { return wstr.c_str(); }
	const char* UnicodeString::GetASCIIPtr() const { return str.c_str(); }
};
}

namespace DirectoryUtil
{
enum ESpecialFolder
{
	PROGRAM_FILES,
	APPDATA,
	LOCALAPPDATA,
	USERPROFILE,
};

std::wstring GetSpecialFolderPath(ESpecialFolder folder);
std::wstring GetFileNameWithoutExtension(const std::wstring&);
bool FileExists(const std::wstring& pathToFile);
std::wstring GetExeDirectoryW();
std::string GetExeDirectory();

std::wstring GetDataDirectoryW();
std::string  GetDataDirectory();

std::wstring GetModelDirectoryW();
std::string GetModelDirectory();

// returns the folder path given a file path
// e.g.: @pathToFile="C:\\Folder\\File.text" -> return "C:\\Folder\\"
//
std::wstring GetFolderPath(const std::wstring& pathToFile);
std::string GetFolderPath(const std::string& pathToFile);

// returns true if the given @pathToImageFile ends with .jpg, .png or .hdr
//
bool IsImageName(const std::wstring& pathToImageFile);

// returns true if file0 has been written into later than file1 has.
//
bool IsFileNewer(const std::wstring& file0, const std::wstring& file1);
}


// returns current time in format "YYYY-MM-DD_HH-MM-SS"
std::wstring GetCurrentTimeAsString();
std::wstring GetCurrentTimeAsStringWithBrackets();


float inline lerp(float low, float high, float t) { return low + (high - low) * t; }

/// RANDOM
//===============================================================================================
float RandF(float l, float h);
int RandI(int l, int h);
size_t RandU(size_t l, size_t h);
