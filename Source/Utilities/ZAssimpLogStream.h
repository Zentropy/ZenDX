#pragma once
#include "ZLog.h"
#include "assimp/LogStream.hpp"


// Example stream
class ZAssimpLogStream : public Assimp::LogStream
{
public:
	// Constructor
	ZAssimpLogStream()
	{
		// empty
	}

	// Destructor
	~ZAssimpLogStream()
	{
		// empty
	}
	// Write womethink using your own functionality
	void write(const char* message)
	{
		LOG << message;
	}
};


