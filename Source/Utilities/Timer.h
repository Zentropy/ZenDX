#pragma once
#include <memory>

namespace Zen {
class Timer
{
public:
	
	Timer();
	float TotalTime()const; // in seconds
	float DeltaTime()const; // in seconds
	void Reset(); // Call before message loop.
	void Start(); // Call when unpaused.
	void Stop(); // Call when paused.
	void Tick(); // Call every frame.
	
private:
	
	double mSecondsPerCount;
	double mDeltaTime;
	__int64 mBaseTime;
	__int64 mPausedTime;
	__int64 mStopTime;
	__int64 mPrevTime;
	__int64 mCurrTime;
	bool mStopped;

public:
	static Timer* GetTimer();
private:
	static std::unique_ptr<Timer> sInstance;

};

// Quick access to static main timer
#define TIMER Zen::Timer::GetTimer()

}