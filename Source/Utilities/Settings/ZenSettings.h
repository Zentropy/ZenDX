#pragma once
#include <memory>
#include "INIReader.h"
#include "Utilities/ZLog.h"

namespace Zen {

struct ZenSettings {
	std::unique_ptr<INIReader> reader;
	bool Initialize();

	//settings fields

	int windowWidth, windowHeight;
	float cameraSpeed, mouseLookSpeed;
	bool bUseVsync, bUseFullscreen, bUseMSAA, invertMouseVertical;

private:

	void LoadSettings();

	//singleton
public:
	static ZenSettings*	GetSettings();
private:
	static std::unique_ptr<ZenSettings>					sInstance;
};




};


#define ZSettings Zen::ZenSettings::GetSettings()