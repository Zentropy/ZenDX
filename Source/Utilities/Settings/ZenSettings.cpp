#include "ZenSettings.h"
#include "Utilities/utils.h"
#include "Utilities/ZLog.h"


namespace Zen {
bool ZenSettings::Initialize()
{
	auto path = DirectoryUtil::GetExeDirectoryW();
	path += L"Settings.ini";
	LOG << path;
	std::string pathString(path.begin(), path.end()); //god i hate unicode
	reader = std::make_unique<INIReader>(pathString);

	if (reader->ParseError() < 0) {
		LOG << "Can't load 'Settings.ini'";
		return false;
	}

	LoadSettings();
	
	return true;
}


void ZenSettings::LoadSettings()
{
	//do the actual reads into vars
	windowWidth = reader->GetInteger("Engine", "WindowWidth", -1);
	windowHeight = reader->GetInteger("Engine", "WindowHeight", -1);
	bUseVsync = reader->GetBoolean("Engine", "VSync", false);
	bUseFullscreen = reader->GetBoolean("Engine", "FullScreen", false);
	bUseMSAA = reader->GetBoolean("Engine", "UseMSAA", false);
	cameraSpeed = (float)reader->GetReal("Engine", "CameraSpeed", 1.0f);
	mouseLookSpeed = (float)reader->GetReal("Engine", "MouseLookSpeed", 1.0f);
	invertMouseVertical = reader->GetBoolean("Engine", "InvertMouseVertical", false);
	LOG << "Settings have been initialized and parsed: ";
}



std::unique_ptr<ZenSettings> ZenSettings::sInstance = nullptr;
ZenSettings* ZenSettings::GetSettings()
{
	if (sInstance == nullptr) { 
		sInstance = std::make_unique<ZenSettings>();
		sInstance->Initialize();
	}
	return sInstance.get();}


}
