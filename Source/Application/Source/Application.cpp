//	VQEngine | DirectX11 Renderer
//	Copyright(C) 2018  - Volkan Ilbeyli
//
//	This program is free software : you can redistribute it and / or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program.If not, see <http://www.gnu.org/licenses/>.
//
//	Contact: volkanilbeyli@gmail.com

#define NOMINMAX
#include "Application/Application.h"

#include "Utilities/CustomParser.h"
#include <strsafe.h>
#include <vector>
#include <new>

#ifdef _DEBUG
#include <cassert>
#endif

#include <iostream>
#include "Engine/Engine.h"
#include "Utilities/ZLog.h"
#include <Utilities/Timer.h>
#include "Utilities/Settings/ZenSettings.h"
#include "Input/EngineInput.h"
#include "GUI/imgui/imgui.h"
#include "GUI/ZimGUI.h"
#include <wrl/internal.h>
#include <winerror.h>


#define xLOG_WINDOW_EVENTS

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

namespace Zen {

	using KeyCode = unsigned int;

std::string Application::s_WorkspaceDirectory = "";

Application::Application()
	:
	mAppName(L"ZenEngine"),
	mbMouseCaptured(false),
	mbAppWantsExit(false),
	mMainWndCaption("ZenEngine")
	//m_threadPool(VQEngine::ThreadPool::sHardwareThreadCount - 2)
{
	HRESULT hr = CoInitializeEx(nullptr, COINITBASE_MULTITHREADED);
	mHInstance		= GetModuleHandle(nullptr);	// instance of this application
	//mTimer = std::make_shared<Timer>();

	

}

Application::~Application(){}


void Application::Exit()
{
	ENGINE->Exit();
	ZimGUI::Shutdown();
	ShutdownWindows();
}

bool Application::Init()
{
	// SETTINGS
	mWindowWidth = ZSettings->windowWidth;
	mWindowHeight = ZSettings->windowHeight;
	//s_WorkspaceDirectory = DirectoryUtil::GetSpecialFolderPath(DirectoryUtil::ESpecialFolder::APPDATA) + "\\VQEngine";
	//Settings::Engine& settings = const_cast<Settings::Engine&>(Engine::ReadSettingsFromFile());	// namespace doesn't make sense.
		
	// WINDOW
	//
	//InitWindow(settings.window);
	InitWindow();
	//ShowWindow(m_hwnd, SW_SHOW);
	this->CaptureMouse(true);

#ifdef ENABLE_RAW_INPUT
	// INPUT
	//
	InitRawInputDevices();
#endif

	// ENGINE
	
	std::cout << "Engine is: " << ENGINE->test;
	
	if (!ENGINE->Initialize(mHwnd))
	{
		LOG << "cannot initialize engine. Exiting..";
		return false;
	}
	else {
		//LOG->info("Engine initialized");
		ZimGUI::CreateContext(mHwnd);
	}
	
	TIMER->Reset();

	return true;
}	

void Application::Run()
{
	MSG msg = { };
	
	while (!mbAppWantsExit)
	{
		// todo: keep dragging main window
		// game engine architecture
		// http://advances.realtimerendering.com/s2016/index.html
		while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);		// Translates virtual-key messages into character messages
			DispatchMessage(&msg);		// indirectly causes Windows to invoke WndProc
		}

		TIMER->Tick();
		ENGINE->Tick();
		if (!mAppPaused) {
			//CalculateFrameStats();
			//UpdateScene(mTimer->DeltaTime());
			//DrawScene();
		} 

		//if (ENGINE->INP()->IsKeyUp("ESC"))
		//{
		//	if (m_bMouseCaptured)		  { this->CaptureMouse(false); }
		//	else if(!ENGINE->IsLoading()) 
		//	{ 
		//		m_bAppWantsExit = true;    
		//	}
		//}

		//if (msg.message == WM_QUIT && !ENGINE->IsLoading())
		//{
		//	m_bAppWantsExit = true;
		//}
		//
		//ENGINE->UpdateAndRender();
		//const_cast<Input*>(ENGINE->INP())->PostUpdate();	// update previous state after frame;
	}
}


LRESULT CALLBACK Application::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	//const Settings::Window& setting = Engine::sEngineSettings.window;
	if (this == nullptr) {
		return 0;
	}

	ImGui_ImplWin32_WndProcHandler(hwnd, umsg, wparam, lparam);

	switch (umsg)
	{
	case WM_CLOSE:		// Check if the window is being closed.
		//LOG->info("[WANT EXIT X]");
		mbAppWantsExit = true;
		PostQuitMessage(0);
		break;

	case WM_SIZE:
#ifdef LOG_WINDOW_EVENTS
		//LOG->info("[WM_SIZE]");
#endif
		//ImGui_ImplDX11_InvalidateDeviceObjects();

		break;

	case WM_ACTIVATE:	// application active/inactive
		if (LOWORD(wparam) == WA_INACTIVE)
		{
			LOG << "WA INACTIVE";
#ifdef LOG_WINDOW_EVENTS
			//LOG->info("WM_ACTIVATE::WA_INACTIVE");
#endif
			this->CaptureMouse(false);
			// paused = true
			// timer stop
			mAppPaused = true;
			TIMER->Stop();
		}
		else
		{
#ifdef LOG_WINDOW_EVENTS
			//LOG->info("WM_ACTIVATE::WA_ACTIVE");
#endif
			LOG << "WA Active";
			this->CaptureMouse(true);
			// paused = false
			// timer start
			mAppPaused = false;
			TIMER->Start();
		}
		break;

	// resize bar grab-release
	case WM_ENTERSIZEMOVE:
#ifdef LOG_WINDOW_EVENTS
		//LOG->info("WM_ENTERSIZEMOVE");
#endif
		// paused = true
		// resizing = true
		// timer.stop()
		break;

	case WM_EXITSIZEMOVE:
		LOG << "WM Exit size move";
#ifdef LOG_WINDOW_EVENTS
		//LOG->info("WM_EXITSIZEMOVE");
#endif
		// paused = false
		// resizing= false
		// timer.start()
		// onresize()
		break;

	// prevent window from becoming too small
	case WM_GETMINMAXINFO:
		((MINMAXINFO*)lparam)->ptMinTrackSize.x = 200;
		((MINMAXINFO*)lparam)->ptMinTrackSize.y = 200;
		break;

	// keyboard
	case WM_KEYDOWN:
	{
#ifdef LOG_WINDOW_EVENTS
		//LOG->info("[WM_KEYDOWN]");// :\t MouseCaptured = %s", m_bMouseCaptured ? "True" : "False");
#endif
		if ( wparam == VK_ESCAPE)
		{
			mbAppWantsExit = true;
			PostQuitMessage(0);
			//if (!m_bMouseCaptured && !ENGINE->IsLoading())
			//{
			//	m_bAppWantsExit = true;
			//	//LOG->info("[WANT EXIT ESC]");
			//}
		}
		//if editor is open, only let Tab through
		if (ENGINE->isEditorOpen) {
			 if (wparam == 9) { // tab
				 LOG << "Tab hit";
				ENGINE->Input->KeyDown((KeyCode)wparam);	
			 }
		}
		else {
			ENGINE->Input->KeyDown((KeyCode)wparam);		
		}
		break;
	}

	case WM_KEYUP:
	{
#ifdef LOG_WINDOW_EVENTS
		//LOG->info("WM_UP");
#endif
		//if editor is open, only let Tab through
		if (ENGINE->isEditorOpen) {
			 if (wparam == 9) { // tab
				 LOG << "Tab hit";
				ENGINE->Input->KeyUp((KeyCode)wparam);	
			 }
		}
		else {
			ENGINE->Input->KeyUp((KeyCode)wparam);		
		}
		break;
	}

	// mouse buttons
	case WM_MBUTTONDOWN:
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	{
		//if (m_bMouseCaptured)	
		if (ENGINE->isEditorOpen) {
			 if (wparam == 9) { // tab
				 LOG << "Tab hit";
				ENGINE->Input->KeyDown((KeyCode)wparam);	
			 }
		}
		else {
			ENGINE->Input->KeyDown((KeyCode)wparam);		
		}
		//else					this->CaptureMouse(true);
		break;
	}

	case WM_MBUTTONUP:
	case WM_RBUTTONUP:
	case WM_LBUTTONUP:
	{
		//if (m_bMouseCaptured)
		if (ENGINE->isEditorOpen) {
			 if (wparam == 9) { // tab
				 LOG << "Tab hit";
				ENGINE->Input->KeyUp((KeyCode)wparam);	
			 }
		}
		else {
			ENGINE->Input->KeyUp((KeyCode)wparam);		
		}
		break;
	}

#ifdef ENABLE_RAW_INPUT
	// raw input for mouse - see: https://msdn.microsoft.com/en-us/library/windows/desktop/ee418864.aspx
	case WM_INPUT:	
	{
		UINT rawInputSize = 48;
		LPBYTE inputBuffer[48];
		ZeroMemory(inputBuffer, rawInputSize);

		GetRawInputData(
			(HRAWINPUT)lparam, 
			RID_INPUT,
			inputBuffer, 
			&rawInputSize, 
			sizeof(RAWINPUTHEADER));

		RAWINPUT* raw = (RAWINPUT*)inputBuffer;

		if (raw->header.dwType == RIM_TYPEMOUSE && raw->data.mouse.usFlags == MOUSE_MOVE_RELATIVE)
		{
			if (mbMouseCaptured)
			{
				if (!ENGINE->isEditorOpen) {

					ENGINE->Input->UpdateMousePos(raw->data.mouse.lLastX, raw->data.mouse.lLastY, raw->data.mouse.usButtonData);
				}
				//SetCursorPos(setting.width / 2, setting.height / 2);
			}
			
#ifdef LOG
			wchar_t szTempOutput[1024];
			StringCchPrintf(szTempOutput, STRSAFE_MAX_CCH, TEXT("%u  Mouse: usFlags=%04x ulButtons=%04x usButtonFlags=%04x usButtonData=%04x ulRawButtons=%04x lLastX=%04x lLastY=%04x ulExtraInformation=%04x\r\n"),
				rawInputSize,
				raw->data.mouse.usFlags,
				raw->data.mouse.ulButtons,
				raw->data.mouse.usButtonFlags,
				raw->data.mouse.usButtonData,
				raw->data.mouse.ulRawButtons,
				raw->data.mouse.lLastX,
				raw->data.mouse.lLastY,
				raw->data.mouse.ulExtraInformation);
			//OutputDebugString(szTempOutput);
#endif
		}
		break;
	}

#else
	// client area mouse - not good for first person camera
	case WM_MOUSEMOVE:
	{
		ENGINE->Input->UpdateMousePos(GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam), scroll);
		break;
	}
#endif

	default:
	{
		return DefWindowProc(hwnd, umsg, wparam, lparam);
	}
	}

	return 0;
}

//-----------------------------------------------------------------------------
// The WndProc function is where windows sends its messages to. You'll notice 
// we tell windows the name of it when we initialize the window class with 
// wc.lpfnWndProc = WndProc in the InitializeWindows function above.
LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	switch (umessage)
	{

	case WM_DESTROY:	// Check if the window is being destroyed.
		PostQuitMessage(0);
		return 0;
		
	case WM_QUIT:		// Check if the window is being closed.
		PostQuitMessage(0);
		return 0;
	default: // All other messages pass to the message handler in the system class.
		return gp_appHandle->MessageHandler(hwnd, umessage, wparam, lparam);
	}
}



void Application::UpdateWindowDimensions(int w, int h)
{
	mWindowHeight = h;
	mWindowWidth = w;
}

bool Application::InitWindow() {
	//int width, height;
	int posX, posY;				// window position
	gp_appHandle = this;		// global handle		

	WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(nullptr), nullptr, nullptr, nullptr, nullptr, L"ImGui Example", nullptr };
	RegisterClassEx(&wc);
	mHwnd = CreateWindow(L"ImGui Example", L"Dear ImGui DirectX11 Example", WS_OVERLAPPEDWINDOW, 100, 100, mWindowWidth, mWindowHeight, nullptr, nullptr, wc.hInstance, nullptr);

	if (mHwnd == nullptr)
	{
		MessageBox(nullptr, L"CreateWindowEx() failed", L"Error", MB_OK);
		//ENGINE->Exit();
		PostQuitMessage(0);
		return false;
	}
	ShowWindow(mHwnd, SW_SHOWDEFAULT);
	UpdateWindow(mHwnd);

	return true;

	/////stuff after this is not ripped from imgui.main

	// default settings for windows class
	//WNDCLASSEX wc;
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = mHInstance;
	wc.hIcon = LoadIcon(nullptr, L"ZenDX.ico");
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = nullptr;
	wc.lpszClassName = mAppName;
	wc.cbSize = sizeof(WNDCLASSEX);
	RegisterClassEx(&wc);

	

	// get clients desktop resolution
	//width = GetSystemMetrics(SM_CXSCREEN);
	//height = GetSystemMetrics(SM_CYSCREEN);

	// set screen settings
	
	
		mWindowWidth = std::max(640, mWindowWidth);
		mWindowHeight = std::max(480, mWindowHeight);
		
	//#TODO: Set this elsewhere
		

		posX = (GetSystemMetrics(SM_CXSCREEN) - mWindowWidth) / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - mWindowHeight) / 2;
		
		posX = posY = 0;

	// create window with screen settings
	mHwnd = CreateWindowEx(
		WS_EX_APPWINDOW,			// Forces a top-level window onto the taskbar when the window is visible.
		mAppName,					// class name
		mAppName,					// Window name
		//WS_POPUP,					// Window style
		WS_OVERLAPPEDWINDOW,		//Window style with titlebar for stats
		posX, posY, mWindowWidth, mWindowHeight,	// Window position and dimensions
		nullptr, nullptr,					// parent, menu
		mHInstance, nullptr
	);

	if (mHwnd == nullptr)
	{
		MessageBox(nullptr, L"CreateWindowEx() failed", L"Error", MB_OK);
		//ENGINE->Exit();
		PostQuitMessage(0);
		return false;
	}
	ShowWindow(mHwnd, SW_SHOW);
	UpdateWindow(mHwnd);
	return true;
}




void Application::ShutdownWindows()
{
	ShowCursor(true);

	//if (Engine::sEngineSettings.window.fullscreen)
	//{
	//	ChangeDisplaySettings(nullptr, 0);
	//}

	// Remove the window.
	DestroyWindow(mHwnd);
	mHwnd = nullptr;

	// Remove the application instance.
	UnregisterClass(mAppName, mHInstance);
	mHInstance = nullptr;

	// Release the pointer to this class.
	gp_appHandle = nullptr;

	return;
}


void Application::InitRawInputDevices()
{
	// register mouse for raw input
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms645565.aspx
	RAWINPUTDEVICE Rid[1];
	Rid[0].usUsagePage = (USHORT)0x01;	// HID_USAGE_PAGE_GENERIC;
	Rid[0].usUsage = (USHORT)0x02;	// HID_USAGE_GENERIC_MOUSE;
	Rid[0].dwFlags = 0;
	Rid[0].hwndTarget = mHwnd;
	if (FALSE == (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0]))))	// Cast between semantically different integer types : a Boolean type to HRESULT.
	{
		OutputDebugString(L"Failed to register raw input device!");
	}

	// get devices and print info
	//-----------------------------------------------------
	UINT numDevices = 0;
	GetRawInputDeviceList(
		nullptr, &numDevices, sizeof(RAWINPUTDEVICELIST));
	if (numDevices == 0) return;

	std::vector<RAWINPUTDEVICELIST> deviceList(numDevices);
	GetRawInputDeviceList(
		&deviceList[0], &numDevices, sizeof(RAWINPUTDEVICELIST));

	std::vector<wchar_t> deviceNameData;
	std::wstring deviceName;
	for (UINT i = 0; i < numDevices; ++i)
	{
		const RAWINPUTDEVICELIST& device = deviceList[i];
		if (device.dwType == RIM_TYPEMOUSE)
		{
			wchar_t info[1024];
			swprintf_s(info, L"Mouse: Handle=0x%08p\n", device.hDevice);
			//OutputDebugString(info);

			UINT dataSize = 0;
			GetRawInputDeviceInfo(
				device.hDevice, RIDI_DEVICENAME, nullptr, &dataSize);
			if (dataSize)
			{
				deviceNameData.resize(dataSize);
				UINT result = GetRawInputDeviceInfo(
					device.hDevice, RIDI_DEVICENAME, &deviceNameData[0], &dataSize);
				if (result != UINT_MAX)
				{
					deviceName.assign(deviceNameData.begin(), deviceNameData.end());

					wchar_t info[1024];
					std::wstring ndeviceName(deviceName.begin(), deviceName.end());
					swprintf_s(info, L"  Name=%s\n", ndeviceName.c_str());
					//OutputDebugString(info);
				}
			}

			RID_DEVICE_INFO deviceInfo;
			deviceInfo.cbSize = sizeof deviceInfo;
			dataSize = sizeof deviceInfo;
			UINT result = GetRawInputDeviceInfo(
				device.hDevice, RIDI_DEVICEINFO, &deviceInfo, &dataSize);
			if (result != UINT_MAX)
			{
#ifdef _DEBUG
				assert(deviceInfo.dwType == RIM_TYPEMOUSE);
#endif
				wchar_t infoMouse[1024];
				swprintf_s(infoMouse,
					L"  Id=%u, Buttons=%u, SampleRate=%u, HorizontalWheel=%s\n",
					deviceInfo.mouse.dwId,
					deviceInfo.mouse.dwNumberOfButtons,
					deviceInfo.mouse.dwSampleRate,
					deviceInfo.mouse.fHasHorizontalWheel ? L"1" : L"0");
				OutputDebugString(infoMouse);
			}
		}
	}
}


void Application::CaptureMouse(bool bDoCapture)
{
	
#ifdef LOG_WINDOW_EVENTS
	//LOG->info("Capture Mouse: %d", bDoCapture ? 1 : 0);
#endif

	mbMouseCaptured = bDoCapture;
	if (bDoCapture)
	{
		RECT rcClip;	
		GetWindowRect(mHwnd, &rcClip);

		// keep clip cursor rect inside pixel area
		//constexpr int PX_OFFSET = 15;
		//constexpr int PX_WND_TITLE_OFFSET = 30;
		//rcClip.left += PX_OFFSET;
		//rcClip.right -= PX_OFFSET;
		//rcClip.top += PX_OFFSET + PX_WND_TITLE_OFFSET;
		//rcClip.bottom -= PX_OFFSET;

		//while (ShowCursor(FALSE) >= 0);
		ShowCursor(FALSE);
		ClipCursor(&rcClip);
		GetCursorPos(&mCapturePosition);
		SetForegroundWindow(mHwnd);
		SetFocus(mHwnd);
		LOG << "Setting capture to true";
		//const_cast<Input*>(ENGINE->INP())->m_bIgnoreInput = false;
	}
	else
	{
		ClipCursor(nullptr);
		SetCursorPos(mCapturePosition.x, mCapturePosition.y);
		//while (ShowCursor(TRUE) <= 0);
		ShowCursor(TRUE);
		SetForegroundWindow(nullptr);
		LOG << "setting capture to false";
		//const_cast<Input*>(ENGINE->INP())->m_bIgnoreInput = true;
	}
	//ShowCursor(TRUE);
}

}