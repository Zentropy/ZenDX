//	VQEngine | DirectX11 Renderer
//	Copyright(C) 2018  - Volkan Ilbeyli
//
//	This program is free software : you can redistribute it and / or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program.If not, see <http://www.gnu.org/licenses/>.
//
//	Contact: volkanilbeyli@gmail.com

#pragma once 

// https://stackoverflow.com/questions/11040133/what-does-defining-win32-lean-and-mean-exclude-exactly
// https://msdn.microsoft.com/en-us/library/windows/desktop/aa383745(v=vs.85).aspx
#define WIN32_LEAN_AND_MEAN		// speeds up build process

//#include "ThreadPool.h"
#define NOMINMAX
#define ZEN_API __declspec(dllexport)
#include <windows.h>
#include <string>
namespace Zen {


namespace Settings { struct Window; }

class Application
{
public: 
	static std::string s_WorkspaceDirectory;

public:
	Application();
	~Application();

	bool Init();
	void Run();
	void Exit();

	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);
	void UpdateWindowDimensions(int w, int h);

private:
	void InitRawInputDevices();
	//void InitWindow(Settings::Window& windowSettings);
	bool InitWindow();
	void ShutdownWindows();
	
	void CaptureMouse(bool bDoCapture);

private:
	LPCWSTR		mAppName;
	HINSTANCE	mHInstance;
	HWND		mHwnd;
	int			mWindowWidth, mWindowHeight;

	// state
	bool		mbMouseCaptured;
	bool		mbAppWantsExit;
	POINT		mCapturePosition;
	//std::shared_ptr<class Timer> mTimer;
	bool mAppPaused;
	std::string mMainWndCaption;

	//VQEngine::ThreadPool	m_threadPool;
};

// The WndProc function and ApplicationHandle pointer are also included here so we can redirect 
// the windows system messaging into our MessageHandler function inside the system class.

static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
static Application* gp_appHandle = nullptr;

}