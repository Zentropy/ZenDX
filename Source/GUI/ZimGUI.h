#pragma once
#include "imgui/imgui.h"
#include "imgui/imgui_impl_dx11.h"
#include "imgui/imgui_impl_win32.h"
#include "imgui/imgui_internal.h"


namespace Zen {

	struct ZimGUI {

		~ZimGUI() {
		
		}

		static void CreateContext(HWND inHwnd, bool showMouseCursor = true)
		{
			IMGUI_CHECKVERSION();
			//PUT FILE PATH TO INI HERE
			ImGui::CreateContext();
			ImGuiIO& io = ImGui::GetIO(); 
			(void)io;
			io.MouseDrawCursor = showMouseCursor;
			ImGui_ImplWin32_Init(inHwnd);

		}

		static void Shutdown()
		{
			//ImGui::Shutdown(ImGui::GetCurrentContext());
			ImGui::DestroyContext(ImGui::GetCurrentContext());
		}

		static void Draw()
		{
			ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
		}

		static void StartFrame()
		{
			// Start the Dear ImGui frame
			ImGui_ImplDX11_NewFrame();
			ImGui_ImplWin32_NewFrame();
			ImGui::NewFrame();
		}

		static void EndFrame() {
			
		}
		
		static void CreateRenderData()
		{
			

			auto io = ImGui::GetIO();
			io.DisplaySize = ImVec2(1600, 900);

			{ //using brackets to control scope makes formatting and checking where the ImGui::Render(); is easier.
				//bool show_demo_window = true;
				//bool show_another_window = true;
				//ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

				ImGui::Begin("Framerate", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar);
				ImGui::SetWindowSize(ImVec2(200, 30), ImGuiSetCond_FirstUseEver);
				ImGui::SetWindowPos(ImVec2(2, 2), ImGuiSetCond_FirstUseEver);
				ImGui::Text("%.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);        
				ImGui::End();
				ImGui::ShowTestWindow();

				//ImGui::ShowDemoWindow(&show_demo_window);
				//
				//// 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
				//{
				//	static float f = 0.0f;
				//	static int counter = 0;
				//
				//	ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.
				//
				//	ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
				//	ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
				//	ImGui::Checkbox("Another Window", &show_another_window);
				//
				//	ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f    
				//	ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color
				//
				//	if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
				//		counter++;
				//	ImGui::SameLine();
				//	ImGui::Text("counter = %d", counter);
				//
				//	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
				//	ImGui::End();
				//}
				//
				//// 3. Show another simple window.
				//if (show_another_window)
				//{
				//	ImGui::Begin("Another Window", &show_another_window);   // Pass a pointer to our bool variable (the window will have a closing button that will clear the bool when clicked)
				//	ImGui::Text("Hello from another window!");
				//	if (ImGui::Button("Close Me"))
				//		show_another_window = false;
				//	ImGui::End();
				//}
			}
			ImGui::Render();
			
		}
	};




};


