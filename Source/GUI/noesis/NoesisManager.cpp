﻿#include "NoesisManager.h"
#include <cstdint>
#include "Utilities/ZLog.h"
#include <Noesis_pch.h>
//#include "NsApp/EntryPoint.h"
//#include "NsApp/Application.h"
//#include "NsApp/ApplicationLauncher.h"
//#include "NsApp/Window.h"
#include "NsApp/LocalXamlProvider.h"
#include "NsApp/LocalFontProvider.h"
#include "NsApp/LocalTextureProvider.h"
#include "Utilities/utils.h"

using namespace Noesis;
using namespace NoesisApp;

namespace Zen
{
NoesisManager::NoesisManager()
{
}

NoesisManager::~NoesisManager()
{
}

void LogHandler(const char* filename, uint32_t line, uint32_t level, const char* channel,
                const char* message)
{
	// if (strcmp(channel, "") == 0)
	// {
	//     // [TRACE] [DEBUG] [INFO] [WARNING] [ERROR]
	//     const char* prefixes[] = { "T", "D", "I", "W", "E" };
	//     const char* prefix = level < NS_COUNTOF(prefixes) ? prefixes[level] : " ";
	//     fprintf(stderr, "[NOESIS/%s] %s\n", prefix, message);
	// }
	LOG << "Noesis log: " << message;
}

void NoesisManager::Initialize()
{
	//Noesis::GUI::Init(nullptr, LogHandler, nullptr);
	//LOG << "Initialized Noesis";
	//auto basedir = DirectoryUtil::GetDataDirectory();
	//basedir += "Noesis";
	//Noesis::GUI::SetXamlProvider(Noesis::MakePtr<LocalXamlProvider>((basedir + "/xaml").c_str()));
	//Noesis::GUI::SetTextureProvider(Noesis::MakePtr<LocalTextureProvider>("."));
	//Noesis::GUI::SetFontProvider(Noesis::MakePtr<LocalFontProvider>("."));
	//
	//Ptr<FrameworkElement> xaml = Noesis::GUI::LoadXaml<FrameworkElement>("Reflections.xaml");
	//Ptr<IView> view = Noesis::GUI::CreateView(xaml);
	//view->SetSize(800, 600);

	//Ptr<RenderDevice> device = *new D3D11RenderContext();
	//view->GetRenderer()->Init(device);
}
}
