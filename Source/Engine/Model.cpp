﻿#include "Model.h"
//#include "Engine.h"
//#include "Renderer/Tests/ZMesh.h"
//#include <stack>
//#include <queue>
//#include <assimp/matrix4x4.h>
//
//namespace Zen
//{
//
//void ReplaceStringInPlace(std::string& subject, const std::string& search,
//                          const std::string& replace) {
//    size_t pos = 0;
//    while ((pos = subject.find(search, pos)) != std::string::npos) {
//         subject.replace(pos, search.length(), replace);
//         pos += replace.length();
//    }
//}
//
//
////inModelPath = "NewBox/NewBox.fbx"
//Model::Model(ID3D11Device* inDeviced, std::wstring inModelPath)
//{
//	mDevice = inDevice;
//	//mMaterial = std::make_unique<ZMaterial>(L"C:/Workspace/CPP/ZenDX/Data/Models/TestBox/cube_diffuse.jpg", ShaderTypes::Diffuse, mDevice, ENGINE->GetHwnd(), L"texture.vs", L"texture.ps");
//
//	//texture = std::make_unique<ZTexture>(L"C:/Workspace/CPP/ZenDX/Data/Textures/awesomeface.png", device);
//	auto imp = ENGINE->GetAssetImporter();
//	//change this to GetExePath and  symlink my data over to build exe folder, then remove NewBox.fbx and concat aiString path onto the end of it to load texture
//	std::wstring modelpath = DirectoryUtil::GetExeDirectoryW() + L"Data/Models/" + inModelPath;
//	std::string str(modelpath.begin(), modelpath.end());
//	imp->ImportFBX(str);
//	//imp->ImportFBX("C:/Workspace/CPP/ZenDX/Data/Models/nanosuit/nanosuit.obj");
//	auto& data = imp->scene;
//	auto gameScene = ENGINE->GetCurrentScene();
//	LOG << "Imported fbx and texture";
//
//	//one node per game object for now
//	aiNode* currentNode = data->mRootNode;
//	std::queue<aiNode*> nodeStack;
//	auto rootObject = gameScene->CreateGameObject(inDevice);
//	
//	//for (int i = 0; i < NumMeshes; ++i)
//	while (nullptr != currentNode)
//	{
//		//get node's children and put em onto the stack
//		for (unsigned int i = 0; i < currentNode->mNumChildren; ++i)
//		{
//			nodeStack.push(currentNode->mChildren[i]);
//		}
//
//		//load each mesh from this node, if no meshes LOG but keep going for now
//		for (unsigned int i = 0; i < currentNode->mNumMeshes; ++i)
//		{
//			//currentnode->mMeshes holds an index pointing to the scene mesh
//			aiMesh* mesh = data->mMeshes[currentNode->mMeshes[i]];
//
//
//			int NumMeshes = data->mNumMeshes;
//
//			int texIndex = 0;
//			aiString path; // filename
//			//path after this line runs ends up being "textures\\sponza_thorn_diff.png"
//			aiReturn texFound = data->mMaterials[mesh->mMaterialIndex]->GetTexture(
//			aiTextureType_DIFFUSE, texIndex, &path);
//
//			aiString name;
//			auto res = data->mMaterials[mesh->mMaterialIndex]->Get(AI_MATKEY_NAME, name);
//
//
//			if(texFound == aiReturn_FAILURE)
//			{
//				LOG << "Failed loading texture";
//			} else
//			{
//				
//			}
//
//			//while (texFound == AI_SUCCESS) {
//			//    //fill map with textures, OpenGL image ids set to 0
//			//   // textureIdMap[path.data] = 0; 
//			//    // more textures?
//			//    texIndex++;
//			//    texFound = data->mMaterials[0]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
//			//}
//			// Tex path is: C:/Workspace/CPP/ZenDX/Build/ZenApplication/x64/Debug/Data/Models/NewBoxtextures\awesomeface.png
//
//			std::string texpath = str.substr(0, str.find_last_of("\\/"));
//			texpath += "/";
//			texpath += path.C_Str();
//			
//			std::replace(texpath.begin(), texpath.end(), '\\', '/');
//			//trade out all TGAs for PNGs forcibly b/c tga loading doesn't fucking work 
//			ReplaceStringInPlace(texpath, "tga", "png");
//			std::wstring texpathwide(texpath.begin(), texpath.end());
//			//std::replace(texpathwide.begin(), texpathwide.end(), 'tga', 'png');
//			LOG << "Tex path is: " << texpathwide;
//
//			Materials.emplace(std::make_pair(mesh->mMaterialIndex, std::make_unique<ZMaterial>(
//			                                 texpathwide, ShaderTypes::Diffuse, mDevice,
//			                                 ENGINE->GetHwnd(), L"texture.vs",
//			                                 L"texture.ps")));
//
//			
//			aiMatrix4x4t tform = currentNode->mTransformation;
//			aiVector3t<float> scaling;
//			aiVector3t<float> position;
//			aiVector3t<float> rotationAxis;
//			float rotationDegree;
//			tform.Decompose(scaling, rotationAxis, rotationDegree, position);
//			//float scaleTest = 0.0100f; //3ds at least exports the positions 100x too big
//			float scaleTest = 1.0f;
//			ZMesh* newMesh =new ZMesh(mDevice, mesh);
//			newMesh->transform.SetPosition(position.x/scaleTest, position.y/scaleTest, position.z/scaleTest);
//			if (rotationDegree != 0.0f)
//				newMesh->transform.RotateAroundAxisDegrees(vec3(rotationAxis.x, rotationAxis.y, rotationAxis.z), rotationDegree);
//			//newMesh->transform.SetScale(vec3(scaling.x, scaling.y, scaling.z));
//			Meshes.emplace_back(newMesh);
//			LOG << "Loaded missile mesh";
//		}
//
//		//move to next node from stack
//		if (!nodeStack.empty())
//		{
//			currentNode = nodeStack.front();
//			nodeStack.pop();
//		}
//		else
//		{
//			//stop traversing node structure
//			currentNode = nullptr;
//		}
//	}
//	LOG << "Done processing nodes";
//}
//
//Model::~Model()
//{
//}
//
//
//void Model::Render(ID3D11DeviceContext* deviceContext, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix,
//                   DirectX::XMMATRIX projectionMatrix)
//{
//	//for (int i = 0; i < Meshes.size(); ++i)
//	//{
//	//	//render each mesh
//	//	Meshes[i]->Render(deviceContext);
//	//	//moving this to IMesh
//	//	//for each, pull out material index then render material retrieved after it
//	//	//ZMaterial* currentMat = GetMaterialFromIndex(Meshes[i]->GetMaterialIndex());
//	//	////this is what actually uses the world matrix that i need to hierarchy-ify
//	//	//currentMat->Render(deviceContext, Meshes[i]->GetIndexCount(), Meshes[i]->transform.WorldTransformationMatrix(),
//	//	//                   viewMatrix, projectionMatrix);
//	//}
//}

//ZMaterial* Model::GetMaterialFromIndex(int inIndex)
//{
//	return Materials.at(inIndex).get();
//}
//}
