#pragma once
#include "Transform.h"
#include "Engine/Model.h"
#include <vector>
#include <memory>
#include <map>
#include "IComponent.h"

namespace Zen {

class IMesh;
class IComponent;

class GameObject {

	using GameObjectPtr = std::shared_ptr<GameObject>;
public:
	GameObject( GameObjectPtr inParentObject = nullptr, const std::string& inName = "NewGo");	
	~GameObject();

	void SetMesh(std::shared_ptr<IMesh> inMesh);

	void AddComponent(std::shared_ptr<IComponent> inComponent);
	inline void SetParent(GameObjectPtr parent) { mParent = parent; };
	inline void SetName(const std::string& inName) { mName = inName;}
	inline void AddChild(GameObjectPtr child) {mChildren.push_back(child.get());};
	void RemoveChild(GameObject* inChild);
	void SetPosition(float x, float y, float z);
	inline void ResetPosition() {transform.ResetPosition();}
	inline void ResetRotation() {transform.ResetRotation();}
	inline void ResetScale() {transform.ResetScale();}
	inline void SetScaleUniform(float inScale) {transform.SetScaleUniform(inScale);}
	inline void SetScale(const vec3& inScale) {transform.SetScale(inScale);}
	inline void SetScale(float x, float y, float z) {transform.SetScale(x, y, z);}

	//#TODO: implement this
	void GetBoundingBox(){};

	//TRANSFORM METHODS
	void Translate(const vec3& inTranslationVector);
	void Translate(float x, float y, float z);
	
	inline GameObjectPtr GetParent() {return mParent;}

	template<typename T>
	T* GetComponent(ComponentTypes inComponentType) {
		return static_cast<T*>(mComponentMap[inComponentType].get());
	}


	void Render();
	void Tick();
	
	Transform transform;
	//std::shared_ptr<IMesh> mMesh;
	//ID3D11Device* mDevice;
	GameObjectPtr mParent;
	std::vector<GameObject*> mChildren;
	std::string mName;
	XMMATRIX GetWorldTransformMatrix();
	std::map<ComponentTypes, std::shared_ptr<IComponent>> mComponentMap;

	bool bShouldTick = false;
	bool bShouldRender = false;
};
}
