#include "Engine/Engine.h"
#include "Input/EngineInput.h"
#include "AssetPipeline/ZModelImporter.h"
#include <assimp/scene.h>
#include "GUI/ZimGUI.h"
#include <Renderer/D3DManager.h>
#include <GUI/noesis/NoesisManager.h>
#include <Keyboard.h>


namespace Zen {

	Engine::Engine()
	{
		
		mScene = std::make_shared<Scene>();
		mHwnd = nullptr;
	}	

	bool Engine::Initialize(HWND inHwnd)
	{
		mHwnd = inHwnd;
		importer = std::make_unique<ZModelImporter>();
		
		Input = std::make_unique<EngineInput>();
		Input->Initialize();
		//mScene = std::make_shared<FirstScene>();

		mD3DManager = std::make_unique<D3DManager>();
		

		mRenderer = std::make_unique<Renderer>(*(mD3DManager.get()));
		//mRenderer->SetScene(mScene);
		mRenderer->Initialize(inHwnd);
		mDevice = mRenderer->mDevice;
		mDeviceContext = mRenderer->mDeviceContext;
		//auto aiscene = importer->ImportFBX("C:/Workspace/CPP/ZenDX/Data/Models/free3D/WhiteMissile1.dae");
		//scene->Init(renderer.get());

		mNoesisMgr = std::make_unique<NoesisManager>();
		mNoesisMgr->Initialize();

		return true;
	}

	void Engine::Exit()
	{
		sInstance = nullptr;
	}

	RECT& Engine::GetClientRect()
	{
		
		::GetClientRect(mHwnd, &clientRect);
		return clientRect;
	}

	int Engine::Tick()
	{
		if (isEditorOpen) {

			ZimGUI::StartFrame();

		}
		mScene->Tick();
		mRenderer->Tick();

		//trigger editor
		if (Input->IsKeyTriggered(Keys::KEY_TAB)) {
			LOG << "Flipping editor";
			isEditorOpen = !isEditorOpen;
		}

		Input->PumpFrame();  //clears out mouse delta, MUST be last
		Input->PostUpdate();
		return 0;
	}

	std::unique_ptr<Engine> Engine::sInstance = nullptr;
	Engine * Engine::GetEngine()
	{
		//make_unique blows up here
		if (sInstance == nullptr) { sInstance = std::unique_ptr<Engine>(new Engine()); }
		return sInstance.get();
	}
}
