#pragma once
#include <queue>
#include "GUI/imgui/imguifilesystem/imguifilesystem.h"
#include <memory>

namespace Zen {
class Scene;
class GameObject;
struct DirectoryNode;


class SceneEditor {

public:
	SceneEditor();

	~SceneEditor() {}

	void Render(Scene* scene);
	

	std::unique_ptr<DirectoryNode> RefreshAssetNode(std::string inDirectoryQueue);
	void RefreshAssetBrowser();
	int RenderAssetBrowserNode(DirectoryNode* inDirectoryNode);
private:
	Scene* currentScene;
	std::unique_ptr<DirectoryNode> mRootDirectoryNode;
	void RenderAssetBrowser(Scene* scene);
	void RenderObjectWindow(Scene* scene);
	void RenderHierarchyWindow(Scene* scene);
	int RenderSceneHierarchyNode(GameObject* inObject);
	void UpdateSceneHierarchyUI();
	void UpdateSceneObjectUI();
	
};

struct AssetDescription
{
	std::string name;
	std::string path;
	AssetDescription(const std::string& inName, const std::string& inPath);
	AssetDescription();;
	~AssetDescription();
};

struct DirectoryNode
{
	AssetDescription description;
	std::vector<AssetDescription> files;
	std::vector<std::shared_ptr<DirectoryNode>> subdirectories;
	DirectoryNode();;
	~DirectoryNode();;
};
}
