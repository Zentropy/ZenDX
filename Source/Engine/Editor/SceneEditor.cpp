﻿#include "SceneEditor.h"
#include "GUI/imgui/imgui.h"
//#include "GUI/imgui/imguifilesystem/imguifilesystem.h"
#include "Engine/GameObject.h"
#include "Engine/Engine.h"
#include <sstream>
#include <Utilities/ZLog.h>
#include <windows.h>
#include "GUI/imgui/imguifilesystem/imguifilesystem.h"
#include <queue>
#include <cstdlib>
#include "Renderer/Objects/IMesh.h"

using namespace ImGuiFs;

namespace Zen
{
SceneEditor::SceneEditor() : currentScene(nullptr)
{
	mRootDirectoryNode = std::make_unique<DirectoryNode>();
	RefreshAssetBrowser();
}

void SceneEditor::Render(Scene* scene)
{
	RenderHierarchyWindow(scene);
	RenderObjectWindow(scene);
	RenderAssetBrowser(scene);
}

std::unique_ptr<DirectoryNode> SceneEditor::RefreshAssetNode(std::string inDirectory)
{
	//recurse on each subdir and add to subdirectories
	std::unique_ptr<DirectoryNode> selfNode = std::make_unique<DirectoryNode>();
	selfNode->description.path = inDirectory;
	char pathname[256];
	ImGuiFs::PathGetParentDirectoryName(inDirectory.c_str(), pathname);
	selfNode->description.name = pathname;

	PathStringVector psv; // = new PathStringVector();
	ImGuiFs::DirectoryGetDirectories(inDirectory.c_str(), psv);

	for (int i = 0; i < psv.size(); ++i)
	{
		//std::string subdir = psv[i];
		selfNode->subdirectories.push_back(RefreshAssetNode(psv[i]));
	}
	FilenameStringVector fsv; //= new FilenameStringVector();
	//add children to files
	ImGuiFs::DirectoryGetFiles(inDirectory.c_str(), psv, &fsv);
	for (int i = 0; i < psv.size(); ++i)
	{
		//std::string subfile = (*psv)[i];

		selfNode->files.emplace_back(AssetDescription(fsv[i], psv[i]));
	}

	//delete psv;
	//delete fsv;	

	return selfNode;
}

void SceneEditor::RefreshAssetBrowser()
{
	mRootDirectoryNode = RefreshAssetNode(DirectoryUtil::GetDataDirectory());
	LOG << "Done refreshing";

	//std::queue<PathString> directoryQueue;
	//directoryQueue.
}

int SceneEditor::RenderAssetBrowserNode(DirectoryNode* inDirectoryNode)
{
	static int i = 0;
	int leafFlag = 0;
	if (inDirectoryNode->subdirectories.size() == 0)
	{
		leafFlag = 1;
	}


	ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow
	| ImGuiTreeNodeFlags_OpenOnDoubleClick
	//| ImGuiTreeNodeFlags_DefaultOpen
	| leafFlag
	//| i
	;
	//render leaf node

	std::string nm = inDirectoryNode->description.name;
	size_t x = (size_t)inDirectoryNode;
	//ImGui::PushID(x);
	std::string label = std::to_string(x);
	//if (ImGui::TreeNodeEx("Directory##", node_flags, inDirectoryNode->description.name.c_str()))
	if (ImGui::TreeNodeEx(label.c_str(), node_flags, inDirectoryNode->description.name.c_str()))
	{
		i++;
		for (auto obj : inDirectoryNode->subdirectories)
		{
			RenderAssetBrowserNode(obj.get());
		}


		for (auto file : inDirectoryNode->files)
		{
			ImGui::Selectable(file.name.c_str());
			if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
			{
				LOG << "Dragging: " << inDirectoryNode->description.name;
				char* filepath = const_cast<char*>(file.path.c_str());
				ImGui::SetDragDropPayload("DraggedFilePath", filepath, sizeof(filepath) * file.path.size());
				ImGui::Text(file.name.c_str());
				ImGui::EndDragDropSource();
			}
			//ImGui::Text(file.name.c_str());			
		}


		//complete this node
		ImGui::TreePop();
	}

	//ImGui::PopID();


	return 0;
}


void SceneEditor::RenderAssetBrowser(Scene* scene)
{
	ImGui::SetNextWindowSize(ImVec2(200, 300), ImGuiCond_FirstUseEver);
	ImGui::SetNextWindowPos(ImVec2(10.0f, 50.f), ImGuiCond_FirstUseEver);
	ImGui::Begin("Asset Browser", nullptr, 0);

	{
		//ImGui::PushStyleVar(ImGuiStyleVar_IndentSpacing, ImGui::GetFontSize() * 1);
		if (ImGui::TreeNode("AssetBrowserRoot"))
		{
			int Res = RenderAssetBrowserNode(mRootDirectoryNode.get());
			ImGui::TreePop();
		}

		//GameObject* currentGO = currentScene->RootGameObject.get();

		//ImGui::PopStyleVar();
	}
	ImGui::End();
}

void SceneEditor::RenderObjectWindow(Scene* scene)
{
	currentScene = scene;
	//UpdateSceneHierarchyUI();
	UpdateSceneObjectUI();
}

void SceneEditor::UpdateSceneObjectUI()
{
	ImGui::SetNextWindowSize(ImVec2(200, 400), ImGuiCond_FirstUseEver);
	ImGui::SetNextWindowPos(ImVec2(550.0f, 10.f), ImGuiCond_FirstUseEver);
	ImGui::Begin("Object Editor", nullptr, 0);

	{
		//if (ImGui::TreeNode("RootNode"))
		//{		
		if (currentScene->SelectedGO != nullptr)
			ImGui::Text(currentScene->SelectedGO->mName.c_str());
		else
		{
			ImGui::End();
			return; // bail early, nothing selected
		}

		auto inObject = currentScene->SelectedGO;

		char buf[2000];

		auto tex = inObject->GetComponent<IMesh>(ComponentTypes::MESH)->GetMaterial()->diffuseTex;
		
		strcpy(buf, tex->_name.c_str());
		
		ImGui::InputText("TextureDropTest", buf, sizeof(buf));

		if (ImGui::BeginDragDropTarget())
		{
			LOG << "IN TARGET";
			if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("DraggedFilePath"))
            {
				char* pp = static_cast<char*>(payload->Data);
				//std::string pd(*pp);
				LOG << "Found Payload " << pp;
				std::string path(pp);
				currentScene->SelectedGO->GetComponent<IMesh>(ComponentTypes::MESH)->GetMaterial()->SetNewTexture(StrUtil::StringToWide(path));
			}
		}



		{
			//Position
			ImGui::Button("Pos");
			if (ImGui::IsItemClicked())
			{
				LOG << "Clicked button";
				//inObject->SetPosition(0, 0, 0);
				inObject->ResetPosition();
			}
			ImGui::SameLine(60);
			ImGuiInputTextFlags inputFlags = ImGuiInputTextFlags_CharsDecimal
			| ImGuiInputTextFlags_EnterReturnsTrue
			| ImGuiInputTextFlags_AutoSelectAll;


			float matrixTranslation[3]{
				inObject->transform.xPos(), inObject->transform.yPos(), inObject->transform.zPos()
			};
			//third parameter is decimal precision
			if (ImGui::InputFloat3("", matrixTranslation, 3, inputFlags))
			{
				LOG << "Setting Position";
				inObject->SetPosition(matrixTranslation[0], matrixTranslation[1], matrixTranslation[2]);
			}
		}

		{
			//Rotation
			ImGui::Button("Rot");
			if (ImGui::IsItemClicked())
			{
				LOG << "Clicked button";
				//inObject->SetPosition(0, 0, 0);
				inObject->ResetRotation();
			}
			ImGui::SameLine(60);
			ImGuiInputTextFlags inputFlags = ImGuiInputTextFlags_CharsDecimal
			| ImGuiInputTextFlags_EnterReturnsTrue
			| ImGuiInputTextFlags_AutoSelectAll;

			auto rotEuler = ZQuaternion::ToEulerDeg(inObject->transform._rotation);
			float matrixTranslation[3]{
				rotEuler.x(), rotEuler.y(), rotEuler.z()
			};
			if (ImGui::InputFloat3("##Rotation", matrixTranslation, 3, inputFlags))
			{
				LOG << "Setting rotation to: " << matrixTranslation[0];
				inObject->transform.SetRotationFromEuler(matrixTranslation[0], matrixTranslation[1],
				                                         matrixTranslation[2]);
			}
		}

		{
			//Rotation
			ImGui::Button("Scl");
			if (ImGui::IsItemClicked())
			{
				LOG << "Clicked button";
				//inObject->SetPosition(0, 0, 0);
				inObject->ResetScale();
			}
			ImGui::SameLine(60);
			ImGuiInputTextFlags inputFlags = ImGuiInputTextFlags_CharsDecimal
			| ImGuiInputTextFlags_EnterReturnsTrue
			| ImGuiInputTextFlags_AutoSelectAll;


			float matrixTranslation[3]{
				inObject->transform._scale.x(), inObject->transform._scale.y(), inObject->transform._scale.z()
			};
			if (ImGui::InputFloat3("##Scale", matrixTranslation, 3, inputFlags))
			{
				LOG << "Setting rotation to: " << matrixTranslation[0];
				inObject->transform.SetScale(matrixTranslation[0], matrixTranslation[1], matrixTranslation[2]);
			}
		}

		//delete [] buf;
	}
	ImGui::End();
}


void SceneEditor::RenderHierarchyWindow(Scene* scene)
{
	currentScene = scene;
	UpdateSceneHierarchyUI();
}

void SceneEditor::UpdateSceneHierarchyUI()
{
	//ImGui::Begin("TestWindow", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar);
	ImGui::SetNextWindowSize(ImVec2(300, 400), ImGuiCond_FirstUseEver);
	ImGui::SetNextWindowPos(ImVec2(1.0f, 50.f), ImGuiCond_FirstUseEver);
	ImGui::Begin("Scene Graph", nullptr, 0);

	{
		ImGui::PushStyleVar(ImGuiStyleVar_IndentSpacing, ImGui::GetFontSize() * 1);

		GameObject* currentGO = currentScene->RootGameObject.get();
		int Res = RenderSceneHierarchyNode(currentGO);


		//if (currentScene->SelectedGO == nullptr)
		//{
		//	currentScene->SelectedGO = currentScene->RootGameObject.get();
		//}


		ImGui::PopStyleVar();
	}


	ImGui::End();
}

int SceneEditor::RenderSceneHierarchyNode(GameObject* inObject)
{
	//int node_clicked = -1;
	std::stringstream TopLevelNode;

	TopLevelNode << inObject->mName.c_str();

	auto* SelectedGO = currentScene->SelectedGO;
	static int i = 0;
	if (inObject == SelectedGO)
	{
		i = ImGuiTreeNodeFlags_Selected;
	}
	else
	{
		i = 0;
	}
	int leafFlag = 0;
	if (inObject->mChildren.size() == 0)
	{
		leafFlag = ImGuiTreeNodeFlags_Leaf;
	}


	ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow
	| ImGuiTreeNodeFlags_OpenOnDoubleClick
	| ImGuiTreeNodeFlags_DefaultOpen
	| leafFlag
	| i;
	//render leaf node


	if (ImGui::TreeNodeEx((void*)(intptr_t)i, node_flags, TopLevelNode.str().c_str()))
	{
		if (nullptr == SelectedGO)
		{
			SelectedGO = currentScene->RootGameObject.get();
		}

		//static int selection_mask = (1 << 2);
		// Dumb representation of what may be user-side selection state. You may carry selection state inside or outside your objects in whatever format you see fit.
		//int node_clicked = -1;
		// Temporary storage of what node we have clicked to process selection at the end of the loop. May be a pointer to your own node type, etc.
		//ImGui::PushStyleVar(ImGuiStyleVar_IndentSpacing, ImGui::GetFontSize() * 3);
		// Disable the default open on single-click behavior and pass in Selected flag according to our selection state.


		//std::string nodeName = inObject->mName + "
		// Node
		std::stringstream posStr;
		posStr << inObject->transform.xPos() << " , " << inObject->transform.yPos() << " , " << inObject
		                                                                                        ->transform.
		                                                                                        zPos();
		std::string nodeLabel = "NodeLabel(EX) - Pos: " + posStr.str();
		//bool node_open = ImGui::TreeNodeEx((void*)(intptr_t)i, node_flags, nodeLabel.c_str());

		i++;

		if (ImGui::IsItemClicked())
		{
			//node_clicked = i;
			LOG << "Item Clicked";
			currentScene->SelectedGO = inObject;
		}

		for (GameObject* obj : inObject->mChildren)
		{
			RenderSceneHierarchyNode(obj);
		}


		//complete this node
		ImGui::TreePop();
	}
	return 0;
}

AssetDescription::AssetDescription()
{
}

AssetDescription::AssetDescription(const std::string& inName, const std::string& inPath) : name(inName), path(inPath)
{
}

AssetDescription::~AssetDescription()
{
}

DirectoryNode::DirectoryNode()
{
}

DirectoryNode::~DirectoryNode()
{
}
}
