#pragma once
#include <windows.h>
#include <memory>
#include <Renderer/Renderer.h>

#include <AssetPipeline/ZModelImporter.h>
#include "Scenes/Scene.h"

namespace Zen {
	class D3DManager;
	class NoesisManager;
	class Engine {
	public:

		virtual ~Engine() = default;
		int test = 5;

		Renderer& GetRenderer() { return *mRenderer; }

		bool Initialize(HWND hwnd);
		void Exit();

		RECT& GetClientRect();
		//bool Load(VQEngine::ThreadPool* pThreadPool);
		//void UpdateAndRender();

		int Tick();

		std::unique_ptr<class EngineInput> Input;

		ZModelImporter* GetAssetImporter()
		{
			return importer.get();
		}

		ID3D11Device* GetDevice() { return mDevice; }
		void SetDevice(ID3D11Device* inDevice) {mDevice = inDevice;}
		ID3D11DeviceContext* GetDeviceContext() { return mDeviceContext; }
		HWND& GetHwnd() { return mHwnd; }
		XMMATRIX& GetViewMatrix() {return mRenderer->GetViewMatrix();}
		XMMATRIX& GetProjectionMatrix() {return mRenderer->GetProjectionMatrix();}

		std::shared_ptr<Scene> GetCurrentScene() {return mScene;}
		bool isEditorOpen = false;

	private:

		std::unique_ptr<Renderer> mRenderer;
		std::unique_ptr<ZModelImporter> importer;
		std::shared_ptr<Scene> mScene;
		std::unique_ptr<D3DManager> mD3DManager;
		std::unique_ptr<NoesisManager> mNoesisMgr;

		ID3D11Device*				mDevice;			// shared ptr
		ID3D11DeviceContext*		mDeviceContext;

		HWND mHwnd;

		//Singleton
		Engine();
	public:
		static Engine*	GetEngine();
	private:
		static std::unique_ptr<Engine>					sInstance;
		RECT clientRect;
	};

#define ENGINE Zen::Engine::GetEngine()
}
