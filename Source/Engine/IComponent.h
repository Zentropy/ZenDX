﻿#pragma once

namespace Zen {

	enum class ComponentTypes {
		NONE,
		MESH,
		LIGHT
	};

class IComponent {
public:
	virtual void Tick(){};
	virtual void Render() {};	
	ComponentTypes mComponentType;
	bool bShouldTick = false;
	bool bShouldRender = false;
};

}