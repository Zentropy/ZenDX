//	VQEngine | DirectX11 Renderer
//	Copyright(C) 2018  - Volkan Ilbeyli
//
//	This program is free software : you can redistribute it and / or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with this program.If not, see <http://www.gnu.org/licenses/>.
//
//	Contact: volkanilbeyli@gmail.com

#pragma once
#include "Utilities/vectormath.h"

struct Transform
{
public:

	//----------------------------------------------------------------------------------------------------------------
	// CONSTRUCTOR / DESTRUCTOR
	//----------------------------------------------------------------------------------------------------------------
	Transform(  const vec3&			position = vec3(0.0f, 0.0f, 0.0f),
				const ZQuaternion&	rotation = ZQuaternion::Identity(),
				const vec3&			scale    = vec3(1.0f, 1.0f, 1.0f));
	~Transform();
	Transform& operator=(const Transform&);

	inline void Initialize(float x, float y, float z, ZQuaternion quat) { _position = vec3(x, y, z); _originalPosition = _position; _rotation = _originalRotation =  quat;}
	inline void Initialize(const vec3& pos, ZQuaternion quat)		   { _position = pos; _originalPosition = _position; _rotation = _originalRotation =  quat;}

	//----------------------------------------------------------------------------------------------------------------
	// GETTERS & SETTERS
	//----------------------------------------------------------------------------------------------------------------
	inline void SetXRotationDeg(float xDeg)            { _rotation = ZQuaternion::FromAxisAngle(vec3::Right  , xDeg * DEG2RAD); }
	inline void AddXRotationDeg(float xDeg)            { _rotation = ZQuaternion::FromAxisAngle(vec3::Right  , xDeg * DEG2RAD) * _rotation; }
	inline void SetYRotationDeg(float yDeg)            { _rotation = ZQuaternion::FromAxisAngle(vec3::Up     , yDeg * DEG2RAD); }
	inline void AddYRotationDeg(float xDeg)            { _rotation = ZQuaternion::FromAxisAngle(vec3::Up  , xDeg * DEG2RAD) * _rotation; }
	inline void SetZRotationDeg(float zDeg)            { _rotation = ZQuaternion::FromAxisAngle(vec3::Forward, zDeg * DEG2RAD); }
	inline void AddZRotationDeg(float xDeg)            { _rotation = ZQuaternion::FromAxisAngle(vec3::Forward  , xDeg * DEG2RAD) * _rotation; }
	inline void SetScale(float x, float y, float z)    { _scale	= vec3(x, y, z); }
	inline void SetScale(const vec3& scl)              { _scale	= scl; }
	inline void SetScaleUniform(float s)		       { _scale	= vec3(s, s, s); }
	inline void SetPosition(float x, float y, float z) { _position = vec3(x, y, z); }
	inline void SetPosition(const vec3& pos)		   { _position = pos; }
	inline void SetPositionX(float x)					{ _position._v.x = x; }
	inline void SetPositionY(float y)					{ _position._v.y = y; }
	inline void SetPositionZ(float z)					{ _position._v.z = z; }
	inline void IncrementPositionX(float x)					{ _position._v.x += x; }
	inline void IncrementPositionY(float y)					{ _position._v.y += y; }
	inline void IncrementPositionZ(float z)					{ _position._v.z += z; }
	inline void ResetPosition() {_position = _originalPosition;}
	inline void ResetRotation() {_rotation = _originalRotation;}
	inline void ResetScale() {_position = _originalPosition;}

	inline float xPos() { return _position._v.x;}
	inline float yPos() { return _position._v.y;}
	inline float zPos() { return _position._v.z;}

	inline float xRot() { return _rotation.quat.x;}
	inline float yRot() { return  _rotation.quat.y;}
	inline float zRot() { return  _rotation.quat.z;}

	inline XMVECTOR GetUp()
	{
		return XMVector3TransformCoord(vec3::Up,		_rotation.Matrix());
	}

	inline XMVECTOR GetDown()
	{
		return XMVector3TransformCoord(vec3::Down,		_rotation.Matrix());
	}

	inline XMVECTOR GetLeft()
	{
		return XMVector3TransformCoord(vec3::Left,		_rotation.Matrix());
	}

	inline XMVECTOR GetRight()
	{
		return XMVector3TransformCoord(vec3::Right,		_rotation.Matrix());
	}

	inline XMVECTOR GetForward()
	{
		return XMVector3TransformCoord(vec3::Forward,		_rotation.Matrix());
	}

	inline XMVECTOR GetBack()
	{
		return XMVector3TransformCoord(vec3::Back,		_rotation.Matrix());
	}

	inline void SetRotation(const ZQuaternion& inRotation)  { _rotation = inRotation; }
	void SetRotationFromEuler(float x, float y, float z);
	vec3 GetEulerRotation();

	//----------------------------------------------------------------------------------------------------------------
	// TRANSFORMATIONS
	//----------------------------------------------------------------------------------------------------------------
	void Translate(const vec3& translation);
	void Translate(float x, float y, float z);
	
	
	void RotateAroundPointAndAxis(const vec3& axis, float angle, const vec3& point);
	inline void RotateAroundAxisRadians(const vec3& axis, float angle) { RotateInWorldSpace(ZQuaternion::FromAxisAngle(axis, angle)); }
	inline void RotateAroundAxisDegrees(const vec3& axis, float angle) { RotateInWorldSpace(ZQuaternion::FromAxisAngle(axis, angle * DEG2RAD)); }

	inline void RotateAroundLocalXAxisDegrees(float angle)	{ RotateInLocalSpace(ZQuaternion::FromAxisAngle(vec3::XAxis, std::forward<float>(angle * DEG2RAD))); }
	inline void RotateAroundLocalYAxisDegrees(float angle)	{ RotateInLocalSpace(ZQuaternion::FromAxisAngle(vec3::YAxis, std::forward<float>(angle * DEG2RAD))); }
	inline void RotateAroundLocalZAxisDegrees(float angle)	{ RotateInLocalSpace(ZQuaternion::FromAxisAngle(vec3::ZAxis, std::forward<float>(angle * DEG2RAD))); }
	inline void RotateAroundGlobalXAxisDegrees(float angle)	{ RotateAroundAxisDegrees(vec3::XAxis, std::forward<float>(angle)); }
	inline void RotateAroundGlobalYAxisDegrees(float angle)	{ RotateAroundAxisDegrees(vec3::YAxis, std::forward<float>(angle)); }
	inline void RotateAroundGlobalZAxisDegrees(float angle)	{ RotateAroundAxisDegrees(vec3::ZAxis, std::forward<float>(angle)); }

	inline void RotateInWorldSpace(const ZQuaternion& q)	{ _rotation = q * _rotation;	}
	inline void RotateInLocalSpace(const ZQuaternion& q)	{ _rotation = _rotation * q;	}


	XMMATRIX WorldTransformationMatrix() const;
	XMMATRIX WorldTransformationMatrix_NoScale() const;
	static XMMATRIX NormalMatrix(const XMMATRIX& world);
	XMMATRIX RotationMatrix() const;

	//----------------------------------------------------------------------------------------------------------------
	// DATA
	//----------------------------------------------------------------------------------------------------------------
	vec3				_position;
	ZQuaternion			_rotation;
	vec3				_scale;
	vec3			_originalPosition;
	ZQuaternion	_originalRotation;

	friend class ZModelImporter;
};

