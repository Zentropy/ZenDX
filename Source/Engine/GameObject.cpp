﻿#include "GameObject.h"
#include "Renderer/Objects/IMesh.h"
#include <algorithm>



namespace Zen
{
GameObject::GameObject(GameObjectPtr inParentObject /* = nullptr */, const std::string& inName)
	: mParent(inParentObject), mName(inName)
{	
	//mModelClass = std::make_unique<Model>(mDevice, inModelPath);
	bShouldRender = true;
	bShouldTick = true;
}

GameObject::~GameObject()
{
}

void GameObject::Render()
{	
	for(auto it = mComponentMap.begin(); it != mComponentMap.end(); ++it) {
		if (it->second->bShouldRender) {
			it->second->Render();
		}
	}	
}

void GameObject::Tick()
{	
	for(auto it = mComponentMap.begin(); it != mComponentMap.end(); ++it) {
		if (it->second->bShouldTick) {
			it->second->Tick();
		}
	}	
}

void GameObject::SetMesh(std::shared_ptr<IMesh> inMesh )
{
	mComponentMap[ComponentTypes::MESH] = inMesh;	
}



void GameObject::AddComponent(std::shared_ptr<IComponent> inComponent )
{
	mComponentMap.insert_or_assign(inComponent->mComponentType, inComponent);
	//mComponentMap.push_back(inComponent);
}

void GameObject::RemoveChild(GameObject* inChild)
{
	//mChildren.push_back(child.get());
	auto val = std::remove(mChildren.begin(), mChildren.end(), inChild);
	mChildren.erase(val, mChildren.end());
}

void GameObject::SetPosition(float x, float y, float z)
{
	transform.SetPosition(x, y, z);
}

void GameObject::Translate(float x, float y, float z)
{
	transform.SetPosition(transform.xPos() + x, transform.yPos() + y, transform.zPos() + z);
}

void GameObject::Translate(const vec3& inTranslationVector)
{
	Translate(inTranslationVector.x(), inTranslationVector.y(), inTranslationVector.z());
}



DirectX::XMMATRIX GameObject::GetWorldTransformMatrix()
{
	if (mParent)
	{
		XMMATRIX parentWorld = mParent->GetWorldTransformMatrix();
		XMMATRIX combined = parentWorld * transform.WorldTransformationMatrix(); //or swapped order depending on pre or post mult
		return combined;
	}
	return transform.WorldTransformationMatrix();
}

}
